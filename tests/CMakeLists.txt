add_custom_target(tests)
add_custom_target(neuroh5_gtests)
add_custom_target(neuroh5_gtest)
add_dependencies(tests neuroh5_gtests)

set(EXECUTABLE_OUTPUT_PATH ${TEST_OUTPUT_PATH})

if (GTEST_FOUND AND HDF5_FOUND)
    include_directories(${HDF5_INCLUDE_DIR})

    set(TEST_LIBRARIES "${HDF5_LIBRARIES}")
    if (HDF5_IS_PARALLEL)
        set(TEST_LIBRARIES "${TEST_LIBRARIES};${MPI_LIBRARIES}")
    endif()
    set(TEST_LIBRARIES "${TEST_LIBRARIES};${PThreadLib}")
    
    neuroh5_add_gtest(stl_io test_stl_io.cc)
    target_link_libraries(stl_io ${TEST_LIBRARIES})

    neuroh5_add_gtest(enumtype_io test_enumtype_io.cc)
    target_link_libraries(enumtype_io ${TEST_LIBRARIES})

endif()


#---------- pyunit tests --------------
if (BUILD_PYTHON_BINDINGS) 
#    neuroh5_add_pyunit(test_read_cell_attr.py)
#    neuroh5_add_pyunit(test_append_cell_attr.py)
endif()
