
#define GOOGLE_STRIP_LOG 1

#include <gtest/gtest.h>
#include <h5cpp/all>

#include "gtest_hdf5.hh"

class EnumTypeTest : public HDF5Test< int >{};


TEST_F(EnumTypeTest, enum_write)
{

  h5::dt_t<int> ty = h5::enum_create<int>();

  h5::enum_insert(ty, "GC", 0);
  h5::enum_insert(ty, "BC", 1);

  h5::commit<int>( this->fd, "/H5Types/Population labels", ty);
}


TEST_F(EnumTypeTest, enum_read)
{
  h5::dt_t<int> ty = h5::type_open<int>( this->fd, "/H5Types/Population labels");

  size_t nmembers = h5::get_nmembers( ty );
  ASSERT_EQ(nmembers, 2);
  
  ASSERT_EQ (h5::enum_nameof(ty, 0), "GC");
  ASSERT_EQ (h5::enum_nameof(ty, 1), "BC");
  
}


/*----------- BEGIN TEST RUNNER ---------------*/
HDF5_TEST_RUNNER( int argc, char**  argv );
/*----------------- END -----------------------*/

