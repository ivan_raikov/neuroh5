
#define GOOGLE_STRIP_LOG 1

#include <gtest/gtest.h>
#include <h5cpp/all>

#include "gtest_hdf5.hh"

template <typename T> class STLTest : public HDF5Test<T>{};
typedef ::testing::Types<H5CPP_TEST_STL_VECTOR_TYPES> StlTypes;

// instantiate for listed types
TYPED_TEST_CASE(STLTest, StlTypes);

TYPED_TEST(STLTest, stl_write)
{
  std::vector<TypeParam> vec(10); // = h5::utils::get_test_data<TypeParam>(10);
  std::iota( std::begin(vec), std::end(vec), 1);

  const h5::count_t& count = h5::count{2};
  const h5::stride_t& stride = h5::stride_t(h5::stride{5});
  const h5::block_t& block = h5::block_t(h5::block{5});
  const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

  const h5::dxpl_t& dxpl = h5::default_dxpl;
  const h5::dapl_t& dapl = h5::default_dapl;
  const h5::dcpl_t& dcpl = h5::chunk{4} | h5::alloc_time(h5::alloc_time_early);

  h5::write(this->fd, this->name, *(std::begin(vec)),
            count, stride, block, max_dims,
            dcpl, dxpl, dapl);
}

TYPED_TEST(STLTest, stl_append)
{
  std::vector<TypeParam> vec1(10); // = h5::utils::get_test_data<TypeParam>(10);
  std::iota( std::begin(vec1), std::end(vec1), 1);

  std::vector<TypeParam> vec2(10); // = h5::utils::get_test_data<TypeParam>(10);
  std::iota( std::begin(vec2), std::end(vec2), 10);
  
  const h5::count_t& count = h5::count{2};
  const h5::stride_t& stride = h5::stride_t(h5::stride{5});
  const h5::block_t& block = h5::block_t(h5::block{5});
  const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

  const h5::dxpl_t& dxpl = h5::default_dxpl;
  const h5::dapl_t& dapl = h5::default_dapl;
  const h5::dcpl_t& dcpl = h5::chunk{4} | h5::alloc_time(h5::alloc_time_early);
  
  h5::append(this->fd, this->name, *(std::begin(vec1)),
             count, stride, block, max_dims,
             dxpl, dapl, dcpl);

  h5::append(this->fd, this->name, *(std::begin(vec2)),
             count, stride, block, max_dims,
             dxpl, dapl, dcpl);
}

TYPED_TEST(STLTest, stl_read)
{
  std::vector<TypeParam> vec(10); // = h5::utils::get_test_data<TypeParam>(10);
  std::iota( std::begin(vec), std::end(vec), 1);

  const h5::count_t& count = h5::count{2};
  const h5::stride_t& stride = h5::stride_t(h5::stride{5});
  const h5::block_t& block = h5::block_t(h5::block{5});
  const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

  const h5::dxpl_t& dxpl = h5::default_dxpl;
  const h5::dapl_t& dapl = h5::default_dapl;
  const h5::dcpl_t& dcpl = h5::chunk{4} | h5::alloc_time(h5::alloc_time_early);

  h5::write(this->fd, this->name, *(std::begin(vec)),
            count, stride, block, max_dims,
            dcpl, dxpl, dapl);


  std::vector<TypeParam> out_vec(10);   
  h5::read(this->fd, this->name, *(std::begin(out_vec)),
           count, block, stride);

  ASSERT_EQ(vec, out_vec);
}


/*----------- BEGIN TEST RUNNER ---------------*/
HDF5_TEST_RUNNER( int argc, char**  argv );
/*----------------- END -----------------------*/

