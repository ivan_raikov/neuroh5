#ifndef NEUROH5_SEARCH_ATTR_ITER
#define NEUROH5_SEARCH_ATTR_ITER

#include <vector>

#include "h5cpp/all"

namespace neuroh5
{
  namespace lowio
  {

    struct attr_iter_od
    {
      unsigned int level;         /* Recursion level.  0=root */
      struct attr_iter_od  *prev;    /* Pointer to previous opdata */
      haddr_t         addr;          /* Group address */
      vector<string, hid_t> *attr_info; /* attribute description */
    };
    
    /* This function recursively searches the linked list of
       attr_iter_od structures for one whose address matches
       target_addr.  Returns 1 if a match is found, and 0
       otherwise. */
    int search_attr_iter (struct attr_iter_od *od, haddr_t target_addr)
    {
      if (od->addr == target_addr)
        return 1;       /* Addresses match */
      else if (od->level <= 0)
        return 0;       /* Root group reached with no matches */
      else
        return group_check (od->prev, target_addr);
      /* Recursively examine the next node */
    }
  }    

#endif
