#ifndef NEUROH5_COPY_H5TYPES_HH
#define NEUROH5_COPY_H5TYPES_HH

#include <vector>
#include "h5cpp/all"

namespace neuroh5
{

  namespace lowio
  {
    /*****************************************************************************
     * Copies H5Types definitions for from src file to dst file
     *****************************************************************************/
    void copy_h5types
    (
     const h5::fd_t& src,
     const h5::fd_t& dst
     )
    {
      const h5::ocpl_t& default_ocpl{ H5Pcreate(H5P_OBJECT_COPY) };
      const h5::ocpl_t& ocpl = merge_comitted_dtype;
      
      H5Padd_merge_committed_dtype_path(ocpl, h5types_path_join(POPULATIONS).c_str());
      H5Ocopy(src, H5_TYPES.c_str(), dst, H5_TYPES.c_str(), ocpl, H5P_DEFAULT);
    }

  }
}

#endif
