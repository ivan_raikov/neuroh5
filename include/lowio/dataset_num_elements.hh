#ifndef DATASET_NUM_ELEMENTS_HH
#define DATASET_NUM_ELEMENTS_HH

#include "h5cpp/all"
#include <string>

namespace neuroh5
{
  namespace lowio
  {

    template <class... args_t >
    size_t dataset_num_elements(const h5::ds_t& ds)
      
    {

      h5::sp_t sp{H5Dget_space( static_cast<::hid_t>(ds) )};
      hsize_t num_elements = H5Sget_simple_extent_npoints( sp );
      
      return num_elements;
      
    }


    template <class... args_t >
    size_t dataset_num_elements( const h5::fd_t& fd,
                                 const string& dataset_path,
                                 args_t&&... args)
      
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);

      const h5::ds_t& ds = h5::open( fd, dataset_path, dapl);
      
      return dataset_num_elements(ds);
    }


    template <class... args_t >
    size_t dataset_num_elements( const string& file_path, 
                                 const string& dataset_path,
                                 args_t&&... args)
      
    {
      auto args_tuple = std::forward_as_tuple(args...);

      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDONLY, h5::default_fapl );
      
      return dataset_num_elements(fd, dataset_path, args...);
    }

    
  }
}


#endif
