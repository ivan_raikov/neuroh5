#ifndef CREATE_FILE_TOPLEVEL_HH
#define CREATE_FILE_TOPLEVEL_HH

#include "h5cpp/all"

#include <vector>

namespace neuroh5
{
  namespace lowio
  {
    /*****************************************************************************
     * Creates a file with the specified top-level groups for storing NeuroH5 structures.
     *****************************************************************************/

    template <class... args_t >
    void create_file_toplevel (const h5::fd_t& fd, 
                               const std::vector<std::string>& groups)
    {

      /* Creates the specified groups.  */
      for (size_t i=0; i<groups.size(); i++)
        {
          gr_t grp = h5::create(fd, groups[i]);
        }
    }

    template <class... args_t >
    void create_file_toplevel( const std::string& file_path,
                               const std::vector<std::string>& groups,
                               args_t&&... args)
    {

      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);
      
      const h5::fapl_t& fapl = comm == MPI_COMM_NULL ? default_fapl :
        fapl_mpio{comm, MPI_INFO_NULL};

      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR, fapl );
      
      return create_file_toplevel( fd, groups );
    }

    
  }
}

#endif
