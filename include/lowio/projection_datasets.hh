// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file projection_datasets.hh
///
///  Functions for reading edge information in DBS (Destination Block Sparse)
///  format.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#ifndef NEUROH5_PROJECTION_DATASETS_HH
#define NEUROH5_PROJECTION_DATASETS_HH

#include "neuroh5_types.hh"
#include "h5cpp/all"

using namespace std;

namespace neuroh5
{
  namespace lowio
  {
    
    /**************************************************************************
     * Read the basic DBS graph structure
     *************************************************************************/

    template <class args_t...>
    void read_projection_datasets_blocks
    (
     const h5::fd_t&            fd,
     const std::string&         src_pop_name,
     const std::string&         dst_pop_name,
     const NODE_IDX_T&          dst_start,
     const NODE_IDX_T&          src_start,
     DST_BLK_PTR_T&             block_base,
     DST_PTR_T&                 edge_base,
     vector<DST_BLK_PTR_T>&     dst_blk_ptr,
     vector<NODE_IDX_T>&        dst_idx,
     vector<DST_PTR_T>&         dst_ptr,
     vector<NODE_IDX_T>&        src_idx,
     size_t&                    total_num_edges,
     size_t&                    total_read_blocks,
     size_t&                    local_read_blocks,
     args_t&&... args)

    {

      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<h5::offset_t>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<h5::count_t>(args_tuple, default_count);

      MPI_Comm comm = h5::get_mpi_comm(fd);
      
      unsigned int rank, size;
      throw_assert(MPI_Comm_size(comm, (int*)&size) == MPI_SUCCESS,
                   "read_projection_datasets: unable to determine MPI communicator size");
      throw_assert(MPI_Comm_rank(comm, (int*)&rank) == MPI_SUCCESS,
                   "read_projection_datasets: unable to determine MPI communicator rank");

      // determine number of blocks in projection
      hsize_t num_blocks = lowio::dataset_num_elements
         (fd, lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                         lowio::EDGES, lowio::DST_BLK_PTR));
      if (num_blocks > 0)
        num_blocks--;
      
      // determine number of edges in projection
      total_num_edges = lowio::dataset_num_elements
        (fd, lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::SRC_IDX));

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t& dxpl = get<const h5::dxpl_t&>(args_tuple, default_dxpl);

      vector< pair<hsize_t,hsize_t> > bins;
      hsize_t read_blocks = 0;

      if (numitems > 0)
        {
          if (offset < num_blocks)
            {
              read_blocks = min((hsize_t)numitems*size, num_blocks-offset);
            }
        }
      else
        { 
          read_blocks = num_blocks;
        }

      total_read_blocks = read_blocks;
      
      if (read_blocks > 0)
        {
          // determine which blocks of block_ptr are read by which rank
          mpi::rank_ranges(read_blocks, size, bins);

          // determine start and stop block for the current rank
          hsize_t start, stop;
          
          start = bins[rank].first + offset;
          stop  = start + bins[rank].second;
          block_base = start;
          
          hsize_t block;
          if (stop > start)
            block = stop - start + 1;
          else
            block = 0;
          if (block > 0)
            {
              local_read_blocks = block-1;
            }
          else
            {
              local_read_blocks = 0;
            }
          

          DST_BLK_PTR_T block_rebase = 0;

          // read destination block pointers

          // allocate buffer and memory dataspace
          if (block > 0)
            {
              dst_blk_ptr.resize(block, 0);
            }
          
          h5::read<DST_BLK_PTR_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_BLK_PTR),
             dst_blk_ptr,
             h5::count{block},
             h5::offset{start},
             dxpl);
          
          // rebase the block_ptr array to local offsets
          // REBASE is going to be the start offset for the hyperslab
          
      
          if (block > 0)
            {
              block_rebase = dst_blk_ptr[0];
          
              for (size_t i = 0; i < dst_blk_ptr.size(); ++i)
                {
                  dst_blk_ptr[i] -= block_rebase;
                }
            }
          else
            {
              block_rebase = 0;
            }

          // read destination block indices
          hsize_t dst_idx_block=0;
          
          if (dst_blk_ptr.size() > 0)
            dst_idx_block = block-1;
          else
            dst_idx_block = 0;
          if (dst_idx_block > 0)
            {
              dst_idx.resize(dst_idx_block, 0);
            }
          
          
          h5::read<NODE_IDX_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_BLK_IDX),
             dst_idx,
             h5::count{dst_idx_block},
             h5::offset{start},
             dxpl);

          // read destination pointers
          hsize_t dst_ptr_block=0, dst_ptr_start=0;
          if (dst_blk_ptr.size() > 0)
            {
              dst_ptr_start = (hsize_t)block_rebase;
              dst_ptr_block = (hsize_t)(dst_blk_ptr.back() - dst_blk_ptr.front());
              if  (stop < num_blocks)
                {
                  dst_ptr_block ++;
                }
            }
          

          if (dst_ptr_block > 0)
            {
              dst_ptr.resize(dst_ptr_block, 0);
            }
          
          h5::read<DST_PTR_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_PTR),
             dst_ptr,
             h5::count{dst_ptr_block},
             h5::offset{dst_ptr_start},
             dxpl);
          
          DST_PTR_T dst_rebase = 0;
          
          hsize_t src_idx_block=0, src_idx_start=0;
          if (dst_ptr_block > 0)
            {
              dst_rebase = dst_ptr[0];
              edge_base = dst_rebase;
              for (size_t i = 0; i < dst_ptr.size(); ++i)
                {
                  dst_ptr[i] -= dst_rebase;
                }
              
              // read source indices
              src_idx_start = dst_rebase;
              src_idx_block = (hsize_t)(dst_ptr.back() - dst_ptr.front());

              // allocate buffer
              if (src_idx_block > 0)
                {
                  src_idx.resize(src_idx_block, 0);
                }
            }

          h5::read<NODE_IDX_T>
            (
             fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::SRC_IDX),
             src_idx,
             h5::count{src_idx_block},
             h5::offset{src_idx_start},
             dxpl);
          
        }

      throw_assert(MPI_Comm_free(&comm) == MPI_SUCCESS,
                   "read_projection_datasets: unable to free MPI communicator");
    }

    
    template <class args_t...>
    void read_projection_datasets_blocks
    (
     const std::string&         file_path,
     const std::string&         src_pop_name,
     const std::string&         dst_pop_name,
     const NODE_IDX_T&          dst_start,
     const NODE_IDX_T&          src_start,
     DST_BLK_PTR_T&             block_base,
     DST_PTR_T&                 edge_base,
     vector<DST_BLK_PTR_T>&     dst_blk_ptr,
     vector<NODE_IDX_T>&        dst_idx,
     vector<DST_PTR_T>&         dst_ptr,
     vector<NODE_IDX_T>&        src_idx,
     size_t&                    total_num_edges,
     size_t&                    total_read_blocks,
     size_t&                    local_read_blocks,
     args_t&&... args)
  
    {
      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);
    
      const h5::fapl_t& fapl = comm == MPI_COMM_NULL ? default_fapl :
        fapl_mpio{comm, MPI_INFO_NULL};
    
      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDONLY, fapl );
    
      read_projection_datasets(fd, src_pop_name, dst_pop_name, dst_start, src_start,
                               block_base, edge_base, dst_blk_ptr, dst_idx, dst_ptr, src_idx,
                               total_num_edges, total_read_blocks, local_read_blocks
                               args...);
    }
    

    template <class args_t...>
    void read_projection_dataset_selection
    (
     const h5::fd_t&            fd,
     const std::string&         file_name,
     const std::string&         src_pop_name,
     const std::string&         dst_pop_name,
     const NODE_IDX_T&          src_start,
     const NODE_IDX_T&          dst_start,
     const std::vector<NODE_IDX_T>&  selection,
     DST_PTR_T&                 edge_base,
     vector<NODE_IDX_T>&        selection_dst_idx,
     vector<DST_PTR_T>&         selection_dst_ptr,
     vector<NODE_IDX_T>&        src_idx,
     size_t&                    total_num_edges,
     args_t&&... args)
    {

      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<h5::offset_t>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<h5::count_t>(args_tuple, default_count);

      MPI_Comm comm = h5::get_mpi_comm(fd);
      
      unsigned int rank, size;
      throw_assert(MPI_Comm_size(comm, (int*)&size) == MPI_SUCCESS,
                   "unable to determine MPI communicator size");
      throw_assert(MPI_Comm_rank(comm, (int*)&rank) == MPI_SUCCESS,
                   "unable to determine MPI communicator rank");

      // determine number of blocks in projection
      hsize_t num_blocks = h5::dataset_num_elements
        (fd, lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_BLK_PTR));
      if (num_blocks > 0)
        num_blocks--;
      
      // determine number of edges in projection
      total_num_edges = h5::dataset_num_elements
        (fd, lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::SRC_IDX));

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t& dxpl = get<const h5::dxpl_t&>(args_tuple, default_dxpl);

      vector< pair<hsize_t,hsize_t> > bins;
      hsize_t read_blocks = num_blocks;
      
      if (read_blocks > 0)
        {
          // determine which blocks of block_ptr are read by which rank
          mpi::rank_ranges(read_blocks, size, bins);

          // determine start and stop block for the current rank
          hsize_t start, stop;
          
          start = bins[rank].first;
          stop  = start + bins[rank].second;
          
          hsize_t block;
          if (stop > start)
            block = stop - start + 1;
          else
            block = 0;

          DST_BLK_PTR_T block_rebase = 0;

          // read destination block pointers

          vector<DST_BLK_PTR_T> dst_blk_ptr(block, 0);
          
          h5::read<DST_BLK_PTR_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_BLK_PTR),
             dst_blk_ptr,
             h5::count{block},
             h5::offset{start},
             dxpl);
          
          // rebase the block_ptr array to local offsets
          // REBASE is going to be the start offset for the hyperslab
          
      
          if (block > 0)
            {
              block_rebase = dst_blk_ptr[0];
          
              for (size_t i = 0; i < dst_blk_ptr.size(); ++i)
                {
                  dst_blk_ptr[i] -= block_rebase;
                }
            }
          else
            {
              block_rebase = 0;
            }

          // read destination block indices
          hsize_t dst_blk_idx_block=0;
          vector<NODE_IDX_T> dst_blk_idx;

          
          if (dst_blk_ptr.size() > 0)
            dst_blk_idx_block = block-1;
          else
            dst_blk_idx_block = 0;
          if (dst_blk_idx_block > 0)
            {
              dst_blk_idx.resize(dst_blk_idx_block, 0);
            }
          
          
          h5::read<NODE_IDX_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_BLK_IDX),
             dst_blk_idx,
             h5::offset{start},
             h5::count{dst_blk_idx_block});

          
          // read destination pointers
          hsize_t dst_ptr_block=0, dst_ptr_start=0;
          if (dst_blk_ptr.size() > 0)
            {
              dst_ptr_start = (hsize_t)block_rebase;
              dst_ptr_block = (hsize_t)(dst_blk_ptr.back() - dst_blk_ptr.front());
              if  (stop < num_blocks)
                {
                  dst_ptr_block ++;
                }
            }
          
          vector<DST_PTR_T> dst_ptr;
          if (dst_ptr_block > 0)
            {
              dst_ptr.resize(dst_ptr_block, 0);
            }
          
          h5::read<DST_PTR_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::DST_PTR),
             dst_ptr,
             h5::offset{dst_ptr_start},
             h5::count{dst_ptr_block},
             dxpl);
          
          DST_PTR_T dst_rebase = 0;
          
          // Create source index ranges based on selection_dst_ptr
          vector< pair<size_t,size_t> >& src_idx_ranges;

          src_idx_ranges.clear();
          ATTR_PTR_T selection_dst_ptr_pos = 0;
          if (dst_ptr_block > 0)
            {
              dst_rebase = dst_ptr[0];
              edge_base = dst_rebase;
              for (size_t i = 0; i < dst_ptr.size(); ++i)
                {
                  dst_ptr[i] -= dst_rebase;
                }

              vector<NODE_IDX_T> dst_idx;
              // Create node index in order to determine which edges to read
              for (size_t i=0; i < dst_blk_idx.size(); i++)
                {
                  NODE_IDX_T dst_base = dst_blk_idx[i];
                  DST_BLK_PTR_T sz = dst_blk_ptr[i+1] - dst_blk_ptr[i];
                  if (i == (dst_blk_idx.size()-1))
                    {
                      sz--;
                    }
                  for (size_t j=0; j<sz; j++)
                    {
                      dst_idx.push_back(dst_base + j);
                    }
                }
              
              for (const NODE_IDX_T& s : selection) 
                {
                  if (s >= dst_start)
                    {
                      NODE_IDX_T n = s-dst_start; 
                      auto it = std::find(dst_idx.begin(), dst_idx.end(), n);
                      
                      if (it != dst_idx.end())
                        {
                          selection_dst_idx.push_back(s);
                          
                          ptrdiff_t pos = it - dst_idx.begin();

                          hsize_t src_idx_start=dst_ptr[pos];
                          hsize_t src_idx_block=dst_ptr[pos+1]-src_idx_start;

                          src_idx_ranges.push_back(make_pair(src_idx_start+dst_rebase, src_idx_block));
                          selection_dst_ptr.push_back(selection_dst_ptr_pos);
                          selection_dst_ptr_pos += src_idx_block;
                        }
                    }
                }
              selection_dst_ptr.push_back(selection_dst_ptr_pos);
            }

          if (src_idx_ranges.size() > 0)
            {
              // allocate buffer and memory dataspace
              src_idx.resize(selection_dst_ptr_pos, 0);
            }

          h5::read_selection<NODE_IDX_T>
            (fd,
             lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                        lowio::EDGES, lowio::SRC_IDX),
             src_idx,
             src_idx_ranges,
             dxpl);
      
        }
    }


    template <class T, class args_t ...>
    void read_projection_datasets
    (
     const h5::fd_t& fd,
     const std::string&         src_pop_name,
     const std::string&         dst_pop_name,
     const NODE_IDX_T&          dst_start,
     const NODE_IDX_T&          src_start,
     DST_BLK_PTR_T&             block_base,
     DST_PTR_T&                 edge_base,
     vector<DST_BLK_PTR_T>&     dst_blk_ptr,
     vector<NODE_IDX_T>&        dst_idx,
     vector<DST_PTR_T>&         dst_ptr,
     vector<NODE_IDX_T>&        src_idx,
     size_t&                    total_num_edges,
     size_t&                    total_read_blocks,
     size_t&                    local_read_blocks,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const std::vector<NODE_IDX_T> default_selection;
      const std::vector<NODE_IDX_T>& selection = get<const std::vector<NODE_IDX_T>&>(args_tuple, default_selection);

      const collective_t collective = get<collective_t>(args_tuple, true);

      const h5::dxpl_t dxpl = collective ? h5::mpio_collective_opt : h5::default_dxpl;

      
      if ( has_type<const std::vector<NODE_IDX_T>&, args_t...>::value )
        {
          read_projection_datasets_selection(fd, src_pop_name, dst_pop_name,
                                             dst_start, src_start, 
                                             selection, edge_base,
                                             dst_idx, dst_ptr, src_idx,
                                             total_num_edges, dxpl, args...);
        }
      else
        {
          read_projection_datasets_blocks(fd, src_pop_name, dst_pop_name,
                                          dst_start, src_start, 
                                          block_base, edge_base,
                                          dst_blk_ptr, dst_idx, dst_ptr, src_idx,
                                          total_num_edges, total_read_blocks, local_read_blocks,
                                          dxpl, args...);
        }
    }


  }

}

#endif
    
