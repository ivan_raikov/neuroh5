#ifndef NUM_PROJECTION_BLOCKS_HH
#define NUM_PROJECTION_BLOCKS_HH

#include <string>


#include "h5cpp/all"


namespace neuroh5
{
  namespace hdf5
  {

    template <>
    size_t num_projection_blocks
    (
     const h5::fd_t& fd,
     const std::string& src_pop_name,
     const std::string& dst_pop_name
     )
    {
      size_t num_blocks = hdf5::dataset_num_elements (fd, lowio::edge_attribute_path(src_pop_name,
                                                                                     dst_pop_name,
                                                                                     lowio::EDGES,
                                                                                     lowio::DST_BLK_PTR)) - 1;
      return num_blocks;
    }
    
    template <>
    size_t num_projection_blocks
    (
     const std::string& file_path,
     const std::string& src_pop_name,
     const std::string& dst_pop_name
     )
    {
      size_t num_blocks = hdf5::dataset_num_elements (file_path,
                                                      lowio::edge_attribute_path(src_pop_name,
                                                                                 dst_pop_name,
                                                                                 lowio::EDGES,
                                                                                 lowio::DST_BLK_PTR)) - 1;
      return num_blocks;
    }
    
  }
}

#endif
