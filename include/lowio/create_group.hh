#ifndef CREATE_GROUP_HH
#define CREATE_GROUP_HH

#include "h5cpp/all"

#include <vector>

namespace neuroh5
{

  namespace lowio
  {
    /*****************************************************************************
     * Creates a dataset group
     *****************************************************************************/

    template <class... args_t >
    void create_group (const h5::fd_t& fd, 
                       const string&   path,
                       args_t&&... args)
    {
      if (!(H5Lexists_path (fd, path)))
        {
          gr_t group = h5::create(fd, path, args...);
        }
    
      return status;
    }
    
    template <class... args_t >
    void create_group (const string& file_path,
                       const string& group_path,
                       args_t&&... args)
    {
      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR, h5::default_fapl );
    
      create_group(fd, group_path, args...);
    }
    
  }

}

#endif

