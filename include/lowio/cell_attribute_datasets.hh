// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file cell_attribute_datasets.hh
///
///  Low-level routines for reading and writing cell attributes.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================
#ifndef CELL_ATTRIBUTE_DATASETS
#define CELL_ATTRIBUTE_DATASETS


#include <vector>

#include "h5cpp/all"

#include "neuroh5_types.hh"
#include "rank_range.hh"
#include "dataset_num_elements.hh"
#include "attr_kind_datatype.hh"
#include "search_attr_iter.hh"
#include "throw_assert.hh"


namespace neuroh5
{
  namespace lowio
  {
    
    
    herr_t get_cell_attributes_cb
    (
     hid_t             loc,
     const char*       name,
     const H5L_info_t* info,
     void*             op_data
     )
    {
      struct *od = (struct attr_iter_od*)op_data;

      H5O_info_t infobuf;
      H5CPP_CHECK_NZ((H5Oget_info_by_name (loc, name, &infobuf, H5P_DEFAULT)),
                     std::runtime_error, "error in H5Oget_info_by_name");

      switch (infobuf.type)
        {
        case H5O_TYPE_GROUP:
          {
            string value_path = string(name) + "/" + ATTR_VAL;

            /* If the group contains dataset ATTR_VAL then it is considered an attribute group */
            if (H5Lexists(loc, value_path.c_str(), H5P_DEFAULT) > 0)
              {
                hid_t dset, dtype;
                H5CPP_CHECK_NZ((dset = H5Dopen2(loc, value_path.c_str(), H5P_DEFAULT)),
                               std::runtime_error, "cell_attributes_cb: unable to open data set");
                H5CPP_CHECK_NZ((ftype = H5Dget_type(dset)),
                               std::runtime_error, "cell_attributes_cb: unable to get data set type");
                od->attr_info->push_back(make_pair(string(name), ftype));
                H5CPP_CHECK_NZ((H5Dclose(dset)),
                               std::runtime_error, "get_cell_attributes_cb: unable to close data set");

              }
            else
              { /* Otherwise, recurse into group */
                 
                /*
                 * Check group address against linked list of operator
                 * data structures to make sure there are no loops.  
                 */
                throw_assert ( search_attr_iter (attr_iter_od, infobuf.addr),
                               "get_cell_attributes_cb: loop detected" );
                 
                /*
                 * Initialize new operator data structure and
                 * begin recursive iteration on the discovered
                 * group.  The new opdata structure is given a
                 * pointer to the current one.
                 */
                struct attr_iter_od next_od;
                nextod.level = od->level + 1;
                nextod.prev  = od;
                nextod.addr  = infobuf.addr;
                nextod.attr_info = od->attr_info; 
                H5CPP_CHECK_NZ((H5Literate_by_name (loc, name, H5_INDEX_NAME,
                                                    H5_ITER_NATIVE, NULL, op_func, (void *) &next_od,
                                                    H5P_DEFAULT)),
                               std::runtime_error,  "get_cell_attributes_cb: error in H5Literate_by_name" );
              }
          }
          break;
        default:
          break;
        }
      
      return 0;
    }

    void get_cell_attributes
    (
     const h5::fd_t& fd,
     const std::string& name_space,
     const std::string& pop_name,
     vector< pair<string, AttrKind> >& out_attributes
     )
    {
      string path = cell_attribute_prefix(name_space, pop_name);

      if (H5Lexists_path (fd, path))
        {
          const h5::gr_t& grp = h5::open(fd, path);

          hsize_t idx = 0;
          vector< pair<string, hid_t> > attr_info;
          struct attr_iter_od od;
          od->attr_info = &attr_info;
          
          H5CPP_CHECK_NZ((H5Literate(static_cast<hid_t>(grp), H5_INDEX_NAME, H5_ITER_NATIVE,
                                     &idx, &get_cell_attributes_cb, (void*)&od )),
                         std::runtime_error,  "get_cell_attributes: error in H5Literate" );
          
          for (size_t i = 0; i < attr_info.size(); ++i)
            {
              string name = attr_info[i].first;
              hid_t ftype = attr_info[i].second;
              attr_kind = h5type_attr_kind(ftype);
              H5CPP_CHECK_NZ((H5Tclose(ftype)), std::runtime_error,  h5::error::msg::type_close);
              out_attributes.push_back(make_pair(string, attr_kind));
            }
        }

      return ierr;
    }

    
    void size_cell_attributes
    (
     const h5::fd_t fd&            loc,
     const std::string&        path,
     const CellPtr&            ptr_type,
     size_t&                   ptr_size,
     size_t&                   index_size,
     size_t&                   value_size
     )
    {
      string ptr_name;
      switch (ptr_type.type)
        {
        case PtrOwner:
          {
            std::string ptr_name;
            if (ptr_type.shared_ptr_name.has_value())
              {
                ptr_name = ptr_type.shared_ptr_name.value();
              }
            else
              {
                ptr_name = ATTR_PTR;
              }
            ptr_size = dataset_num_elements(loc, path + "/" + ptr_name);
          }
          break;
        case PtrShared:
          {
            std::string ptr_name;
            throw_assert (ptr_type.shared_ptr_name.has_value(),
                          "size_cell_attributes: shared attribute pointer has no value");
            ptr_name = ptr_type.shared_ptr_name.value();
            ptr_size = dataset_num_elements(loc, ptr_name);
          }
          break;
        case PtrNone:
          break;
        }
      index_size = dataset_num_elements(loc, path + "/" + CELL_INDEX);
      value_size = dataset_num_elements(loc, path + "/" + ATTR_VAL);
    }


    template <typename T>
    void create_cell_attribute_datasets
    (
     const h5::fd_t&  fd,
     const string&    attr_namespace,
     const string&    pop_name,
     const string&    attr_name,
     const CellIndex& index_type,
     const CellPtr&   ptr_type,
     const size_t     chunk_size,
     const size_t     value_chunk_size
     )
    {
      const h5::max_dims_t max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

#if H5_VERSION_GE(1,10,2)
      const h5::dcpl_t dcpl = h5::deflate{9} | h5::chunk{chunk_size} | h5::alloc_time(h5::alloc_time_early);
      const h5::dcpl_t value_dcpl = h5::deflate{9} | h5::chunk{value_chunk_size} | h5::alloc_time(h5::alloc_time_early);
#else
      const h5::dcpl_t dcpl = h5::chunk{chunk_size} | h5::alloc_time(h5::alloc_time_early);
      const h5::dcpl_t value_dcpl = h5::chunk{value_chunk_size} | h5::alloc_time(h5::alloc_time_early);
#endif
      
      switch (index_type)
        {
        case IndexOwner:
          {
            h5::ds_t ds = h5::create<T>(fd, attr_path + "/" + CELL_INDEX, max_dims, dcpl);
          }
          break;
        case IndexShared:
          {
            h5::ds_t ds = h5::open(fd, attr_prefix + "/" + hdf5::CELL_INDEX);
            H5CPP_CHECK_NZ((H5Olink(static_cast<hid_t>(ds), static_cast<hid_t>(fd),
                                    (attr_path + "/" + hdf5::CELL_INDEX).c_str(),
                                    H5P_DEFAULT, H5P_DEFAULT)));
          }
          break;
        case IndexNone:
          break;
        }
      
      switch (ptr_type.type)
        {
        case PtrOwner:
          {
            std::string ptr_name;
            if (ptr_type.shared_ptr_name.has_value())
              {
                ptr_name = ptr_type.shared_ptr_name.value();
              }
            else
              {
                ptr_name = hdf5::ATTR_PTR;
              }

            h5::ds_t ds = h5::create(fd, attr_path + "/" + ptr_name, max_dims, dcpl);
            if (ptr_name.compare(ATTR_PTR) != 0)
              {
                H5CPP_CHECK_NZ((H5Olink(static_cast<hid_t>(ds), static_cast<hid_t>(fd),
                                        (attr_path + "/" + hdf5::ATTR_PTR).c_str(),
                                        H5P_DEFAULT, H5P_DEFAULT)));
              }
          }
          break;
        case PtrShared:
          {
            throw_assert(ptr_type.shared_ptr_name.has_value(),
                         "create_cell_attribute_datasets: shared pointer data set name has not been set");

            h5::ds_t ds = h5::open(fd, ptr_type.shared_ptr_name.value());
            H5CPP_CHECK_NZ((H5Olink(static_cast<hid_t>(ds), static_cast<hid_t>(fd),
                                    (attr_path + "/" + hdf5::ATTR_PTR).c_str(),
                                    H5P_DEFAULT, H5P_DEFAULT)));
          }
          break;
        case PtrNone:
          break;
        }

      h5::ds_t ds = h5::create(fd, attr_path + "/" + hdf5::ATTR_VAL, max_dims, value_dcpl);
    }
  
  
    template <typename T, class args_t...>
    void read_cell_attribute_datasets_block
    (
     const h5::fd_t&           fd,
     const std::string&        path,
     const CELL_IDX_T          pop_start,
     std::vector<CELL_IDX_T>&  index,
     std::vector<ATTR_PTR_T>&  ptr,
     std::vector<T> &          values,
     args_t&&...               args)
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<h5::offset_t>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<h5::count_t>(args_tuple, default_count);

      MPI_Comm comm = h5::get_mpi_comm(fd);

      unsigned int size, rank;

      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");

      throw_assert(exists_group (fd, path),
                   "read_cell_attribute_datasets_block: " << path << " does not exist");

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t& dxpl = get<const h5::dxpl_t&>(args_tuple, default_dxpl);
      
      size_t dset_size = dataset_num_elements (fd, path + "/" + CELL_INDEX);
      size_t read_size = 0;
      if (count > 0) 
        {
          if (offset < dset_size)
            {
              read_size = min(count, dset_size-offset);
            }
        }
      else
        {
          read_size = dset_size;
        }
    
      if (read_size > 0)
        {
          // determine which blocks of block_ptr are read by which rank
          vector< pair<size_t, size_t> > ranges;
          mpi::rank_ranges(read_size, size, ranges);
        
          size_t start = ranges[rank].first + offset;
          size_t end   = start + ranges[rank].second;
          size_t block = end - start;
    
          string index_path = path + "/" + CELL_INDEX;
          string ptr_path = path + "/" + ATTR_PTR;
          string value_path = path + "/" + ATTR_VAL;

          h5::read<NODE_IDX_T> (fd, index_path, index,
                                h5::offset{start}, h5::count{block},
                                dxpl);
          for (size_t i=0; i<index.size(); i++)
            {
              index[i] += pop_start;
            }
              
          // read pointer and determine ranges
          if (exists_dataset (fd, ptr_path))
            {
              if (block > 0)
                {
                  ptr.resize(block+1);
                }
              h5::read<ATTR_PTR_T> (fd, ptr_path, ptr,
                                    h5::offset{start}, h5::count{block > 0 ? block+1 : 0},
                                    dxpl);
            }
        }
    
      
      size_t value_start, value_block;
      if (ptr.size() > 0)
        {
          value_start = ptr[0];
          value_block = ptr.back()-value_start;
        }
      else
        {
          value_start = 0;
          value_block = block > 0 ? dataset_num_elements (fd, value_path) : 0;
        }
      
      
      h5::read<T> (loc, value_path, values,
                   h5::offset{value_start}, h5::count{value_block},
                   dxpl);
    
      if (ptr.size() > 0)
        {
          for (size_t i=0; i<block+1; i++)
            {
              ptr[i] -= value_start;
            }
        }
    
      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm),
                        std::runtime_error,
                        "unable to free MPI communicator");
      
    }
  


    
    template <typename T, class args_t...>
    void read_cell_attribute_datasets_selection
    (
     const h5::fd_t&           fd,
     const std::string&        path,
     const CELL_IDX_T          pop_start,
     const std::vector<CELL_IDX_T>&  selection,
     std::vector<CELL_IDX_T> & selection_index,
     std::vector<ATTR_PTR_T> & selection_ptr,
     std::vector<T> &          values,
     args_t&&... args)
    {

      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<h5::offset_t>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<h5::count_t>(args_tuple, default_count);
      
      MPI_Comm comm = h5::get_mpi_comm(fd);
      
      unsigned int rank, size;
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");

      throw_assert(exists_group (fd, path),
                   "read_cell_attribute_selection: " << path << " does not exist");

      std::vector<ATTR_PTR_T> ptr;
      std::vector<CELL_IDX_T> index;

      size_t dset_size = dataset_num_elements (fd, path + "/" + CELL_INDEX);
      vector< pair<size_t, size_t> > ranges;

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t& dxpl = get<const h5::dxpl_t&>(args_tuple, default_dxpl);
      
      if (dset_size > 0)
        {
          string index_path = path + "/" + CELL_INDEX;
          string ptr_path = path + "/" + ATTR_PTR;
          string value_path = path + "/" + ATTR_VAL;

          read<NODE_IDX_T> (fd, index_path, index,
                            h5::start{0}, h5::count{dset_size},
                            dxpl);

          // read pointer and determine ranges
          if (exists_dataset (fd, ptr_path))
            {
              read<ATTR_PTR_T> (fd, ptr_path, ptr,
                                h5::start{0}, h5::count{dset_size+1},
                                dxpl);
              assert (status >= 0);
            }

        }
    
      ATTR_PTR_T selection_ptr_pos = 0;
      if (ptr.size() > 0)
        {
          for (const CELL_IDX_T& s : selection) 
            {
              if (s < pop_start) continue;
              auto it = std::find(index.begin(), index.end(), s-pop_start);
              if (it == index.end()) continue;
            
              throw_assert(it != index.end(),
                           "read_cell_attribute_selection: unable to find attribute "
                           << path << " for gid " << s);
            
              ptrdiff_t pos = it - index.begin();
            
              size_t value_start=ptr[pos];
              size_t value_block=ptr[pos+1]-value_start;
            
              ranges.push_back(make_pair(value_start, value_block));
              selection_ptr.push_back(selection_ptr_pos);
              selection_ptr_pos += value_block;
              selection_index.push_back(s);
            }
          selection_ptr.push_back(selection_ptr_pos);
        }
          
      h5::read_selection<T> (fd, value_path, values, ranges, dxpl);

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm),
                        std::runtime_error,
                        "unable to free MPI communicator");
    }


    
    template <class T, class args_t ...>
    void read_cell_attribute_datasets
    (
     const h5::fd_t& fd,
     const string& attr_path,
     const CELL_IDX_T& pop_start,
     vector<CELL_IDX_T>&  index,
     vector<ATTR_PTR_T>&  ptr,
     vector<T> values,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const std::vector<CELL_IDX_T> default_selection;
      const std::vector<CELL_IDX_T>& selection = get<const std::vector<CELL_IDX_T>&>(args_tuple, default_selection);

      const collective_t collective = get<collective_t>(args_tuple, true);

      const h5::dxpl_t dxpl = collective ? h5::mpio_collective_opt : h5::default_dxpl;
      
      if ( has_type<const std::vector<CELL_IDX_T>&, args_t...>::value )
        {
          read_cell_attribute_datasets_selection<T>(fd, attr_path, pop_start,
                                                    selection, index, ptr,
                                                    attr_values_uint32,
                                                    dxpl, args...);
        }
      else
        {
          read_cell_attribute_datasets_block<T>(fd, attr_path, pop_start,
                                                index, ptr, values,
                                                dxpl, args...);
        }
    }

            
    
    template <typename T, class args_t...>
    void read_cell_attribute_datasets
    (
     const std::string&        file_path,
     const std::string&        path,
     const CELL_IDX_T          pop_start,
     std::vector<CELL_IDX_T>&  index,
     std::vector<ATTR_PTR_T>&  ptr,
     std::vector<T> &          values,
     args_t&&...               args)
    {
      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);
    
      const h5::fapl_t& fapl = comm == MPI_COMM_NULL ? default_fapl :
        fapl_mpio{comm, MPI_INFO_NULL};
    
      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDONLY, fapl );
    
      read_cell_attribute_datasets(fd, path, pop_start, index, ptr, values, args...);
    }

    
    template <typename T, class args_t...>
    void append_cell_attribute
    (
     const fd_t&                     fd,
     const std::string&              path,
     const std::vector<CELL_IDX_T>&  index,
     const std::vector<ATTR_PTR_T>&  attr_ptr,
     const std::vector<T>&           value,
     const CellIndex                 index_type,
     const CellPtr                   ptr_type,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      int status;
      throw_assert(index.size() == attr_ptr.size()-1,
                   "append_cell_attribute: size mismatch between index and attribute pointers");

      MPI_Comm comm = h5::get_mpi_comm(fd);

      unsigned int size, rank;
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");

      const collective_t collective = get<collective_t>(args_tuple, true);

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t dxpl = collective ? h5::mpio_collective_opt : h5::default_dxpl;
      const h5::dcpl_t& dcpl = get<const h5::dcpl_t&>(args_tuple, h5::alloc_time(h5::alloc_time_early));

      const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

      std::vector<ATTR_PTR_T>  local_attr_ptr;

      
      // Determine the total size of index
      size_t local_index_size=index.size();
      std::vector<size_t> index_size_vector;
      index_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_index_size, 1, MPI_SIZE_T, &index_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");

      // Determine the total number of ptrs, add 1 to ptr of last rank
      size_t local_ptr_size;

      if (attr_ptr.size() > 0)
        {
          local_ptr_size = attr_ptr.size()-1;
        }
      else
        {
          local_ptr_size = 0;
        }
        
      if (rank == size-1)
        {
          local_ptr_size=local_ptr_size+1;
        }
    
      std::vector<size_t> ptr_size_vector;
      ptr_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_ptr_size, 1, MPI_SIZE_T, &ptr_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
      
      hsize_t local_value_size = value.size();
      std::vector<size_t> value_size_vector;
      value_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_value_size, 1, MPI_SIZE_T, &value_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
      
      // create datasets
      hsize_t ptr_size=0, index_size=0, value_size=0;
      size_cell_attributes(fd, path, ptr_type, ptr_size, index_size, value_size);

      // Determine starting positions
      hsize_t ptr_start=0, index_start=0, value_start=0;
      if (ptr_size>0)
        {
          ptr_start=ptr_size-1;
        }
      index_start=index_size; value_start=value_size;

      hsize_t local_value_start=value_start, local_index_start=index_start, local_ptr_start=ptr_start;
      // calculate the starting positions of this rank
      for (size_t i=0; i<rank; i++)
        {
          local_value_start = local_value_start + value_size_vector[i];
          local_index_start = local_index_start + index_size_vector[i];
          local_ptr_start = local_ptr_start + ptr_size_vector[i];
        }

      // calculate the new sizes of the datasets
      size_t global_value_size=0, global_index_size=0, global_ptr_size=0;
      for (size_t i=0; i<size; i++)
        {
          global_value_size  = global_value_size + value_size_vector[i];
          global_index_size  = global_index_size + index_size_vector[i];
          global_ptr_size  = global_ptr_size + ptr_size_vector[i];
        }

      // add local value offset to attr_ptr
      local_attr_ptr.resize(attr_ptr.size());
      for (size_t i=0; i<local_attr_ptr.size(); i++)
        {
          local_attr_ptr[i] = attr_ptr[i] + local_value_start;
        }

      switch (index_type)
        {
        case IndexOwner:
          {
            const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_index_size});
            h5::append<CELL_IDX_T> (fd, path + "/" + CELL_INDEX, *(std::begin(index)),
                                    h5::count{local_index_size}, h5::offset{local_index_start}, 
                                    data_dims, max_dims, dcpl, dxpl);
          }
          break;
        case IndexShared:
          break;
        case IndexNone:
          break;
        }
    
      switch (ptr_type.type)
        {
        case PtrOwner:
          {
            // TODO: save to prefix and link to index in path
            std::string ptr_name;
            if (ptr_type.shared_ptr_name.has_value())
              {
                ptr_name = ptr_type.shared_ptr_name.value();
              }
            else
              {
                ptr_name = ATTR_PTR;
              }
            const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_ptr_size});
            h5::append<ATTR_PTR_T> (fd, path + "/" + ptr_name, *(std::begin(local_attr_ptr)),
                                    h5::count{local_ptr_size}, h5::offset{local_ptr_start}, 
                                    data_dims, max_dims, dcpl, dxpl);
          }
          break;
        case PtrShared:
          break;
        case PtrNone:
          break;
        }

      if (global_value_size > 0)
        {
          const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_value_size});
          h5::append<T> (fd, path + "/" + ATTR_VAL,  *(std::begin(value)),
                         h5::count{local_value_size}, h5::offset{local_value_start}, 
                         data_dims, max_dims, dcpl, dxpl);
        }

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm),
                        std::runtime_error, "unable to free MPI communicator");
      
    }


  
    template <typename T, class args_t...>
    void write_cell_attribute
    (
     const fd_t&                      fd,
     const std::string&              path,
     const std::vector<CELL_IDX_T>&  index,
     const std::vector<ATTR_PTR_T>&  attr_ptr,
     const std::vector<T>&           value,
     const CellIndex                 index_type,
     const CellPtr                   ptr_type,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      int status;
      throw_assert(index.size() == attr_ptr.size()-1,
                   "write_cell_attribute: size mismatch between index and attribute pointers");

      MPI_Comm comm = h5::get_mpi_comm(fd);

      unsigned int size, rank;
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");

      const collective_t collective = get<collective_t>(args_tuple, true);

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t& dxpl = collective ? h5::mpio_collective_opt : h5::default_dxpl;
      const h5::dcpl_t& dcpl = get<const h5::dcpl_t&>(args_tuple, h5::alloc_time(h5::alloc_time_early));

      const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

      std::vector<ATTR_PTR_T>  local_attr_ptr;
      
      // Determine the total number of index
      size_t local_index_size=index.size();
      std::vector<size_t> index_size_vector;
      index_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_index_size, 1, MPI_SIZE_T, &index_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");

      // Determine the total number of ptrs, add 1 to ptr of last rank
      size_t local_ptr_size=attr_ptr.size()-1;
      if (rank == size-1)
        {
          local_ptr_size=local_ptr_size+1;
        }
    
      std::vector<size_t> ptr_size_vector;
      ptr_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_ptr_size, 1, MPI_SIZE_T, &ptr_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
    
      size_t local_value_size = value.size();
      std::vector<size_t> value_size_vector;
      value_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_value_size, 1, MPI_SIZE_T, &value_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");

      size_t local_value_start=0, local_index_start=0, local_ptr_start=0;
      // calculate the starting positions of this rank
      for (size_t i=0; i<rank; i++)
        {
          local_value_start = local_value_start + value_size_vector[i];
          local_index_start = local_index_start + index_size_vector[i];
          local_ptr_start = local_ptr_start + ptr_size_vector[i];
        }
      // calculate the new sizes of the datasets
      hsize_t global_value_size=0, global_index_size=0, global_ptr_size=0;
      for (size_t i=0; i<size; i++)
        {
          global_value_size  = global_value_size + value_size_vector[i];
          global_index_size  = global_index_size + index_size_vector[i];
          global_ptr_size  = global_ptr_size + ptr_size_vector[i];
        }

      // add local value offset to attr_ptr
      local_attr_ptr.resize(attr_ptr.size());
      for (size_t i=0; i<local_attr_ptr.size(); i++)
        {
          local_attr_ptr[i] = attr_ptr[i] + local_value_start;
        }
    
      if (global_value_size > 0)
        {
          // write to datasets

          switch (index_type)
            {
            case IndexOwner:
              {
                const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_index_size});
                h5::write<CELL_IDX_T> (file, path + "/" + CELL_INDEX, *(std::begin(index)),
                                       h5::count{local_index_size}, h5::offset{local_index_start}, 
                                       data_dims, max_dims, dcpl, dxpl);
              }
              break;
            case IndexShared:
              // TODO: validate index
              break;
            case IndexNone:
              break;
            }

          switch (ptr_type.type)
            {
            case PtrOwner:
              {
                const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_ptr_size});
                h5::write<ATTR_PTR_T> (file, path + "/" + ATTR_PTR, *(std::begin(local_attr_ptr)), 
                                       h5::count{local_ptr_size}, h5::offset{local_ptr_start}, 
                                       data_dims, max_dims, dcpl, dxpl);
              }
            case PtrShared:
              // TODO: validate ptr
              break;

            case PtrNone:
              break;
            }

          if (global_value_size > 0)
            {
              const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_value_size});
              write<T> (file, path + "/" + ATTR_VAL, *(std::begin(value)),
                        h5::count{local_value_size}, h5::offset{local_value_start},
                        data_dims, max_dims, dcpl, dxpl);
                        
            }
        }

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm),
                        std::runtime_error, "unable to free MPI communicator");

    }

  }
}



#endif
