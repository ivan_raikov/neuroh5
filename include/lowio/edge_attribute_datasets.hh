#ifndef NEUROH5_EDGE_ATTRIBUTE_DATASETS
#define NEUROH5_EDGE_ATTRIBUTE_DATASETS

#include <vector>


#include "h5cpp/all"

#include "neuroh5_types.hh"
#include "path_names.hh"
#include "rank_range.hh"
#include "dataset_num_elements.hh"


namespace neuroh5
{
  namespace lowio
  {

    void size_edge_attributes
    (
     hid_t          loc,
     const string&  src_pop_name,
     const string&  dst_pop_name,
     const string&  attr_namespace,
     const string&  attr_name,
     hsize_t&       value_size
     )
    {
      string path = hdf5::edge_attribute_path(src_pop_name, dst_pop_name,
                                              attr_namespace, attr_name);
      value_size = hdf5::dataset_num_elements(loc, path);
    }

    
    void create_projection_groups
    (
     const hid_t&   file,
     const string&  src_pop_name,
     const string&  dst_pop_name
     )
    {
      string path = "/" + hdf5::PROJECTIONS;
      if (!(hdf5::exists_dataset (file, path) > 0))
        {
          hdf5::create_group(file, path.c_str());
        }
      
      path = "/" + hdf5::PROJECTIONS + "/" + dst_pop_name;
            
      if (!(hdf5::exists_dataset (file, path) > 0))
        {
          hdf5::create_group(file, path.c_str());
        }

      path = hdf5::projection_prefix(src_pop_name, dst_pop_name);

      if (!(hdf5::exists_dataset (file, path) > 0))
        {
          hdf5::create_group(file, path.c_str());
        }
    
    }

    void create_edge_attribute_datasets
    (
     const hid_t&   file,
     const string&  src_pop_name,
     const string&  dst_pop_name,
     const string&  attr_namespace,
     const string&  attr_name,
     const hid_t&   ftype,
     const size_t   chunk_size
     )
    {
      herr_t status;
      hsize_t maxdims[1] = {H5S_UNLIMITED};
      hsize_t cdims[1]   = {chunk_size}; /* chunking dimensions */		
      hsize_t initial_size = 0;
    
      hid_t plist  = H5Pcreate (H5P_DATASET_CREATE);
      status = H5Pset_chunk(plist, 1, cdims);
      assert(status == 0);
#if H5_VERSION_GE(1,10,2)
      status = H5Pset_deflate(plist, 9);
      assert(status == 0);
#endif
      
      hid_t lcpl = H5Pcreate(H5P_LINK_CREATE);
      assert(lcpl >= 0);
      assert(H5Pset_create_intermediate_group(lcpl, 1) >= 0);

      create_projection_groups(file, src_pop_name, dst_pop_name);

      string attr_path = hdf5::edge_attribute_path(src_pop_name,
                                                   dst_pop_name,
                                                   attr_namespace,
                                                   attr_name);
      
      hid_t mspace = H5Screate_simple(1, &initial_size, maxdims);
      hid_t dset = H5Dcreate2(file, attr_path.c_str(), ftype, mspace,
                              lcpl, plist, H5P_DEFAULT);
      assert(H5Dclose(dset) >= 0);
      assert(H5Sclose(mspace) >= 0);
    
      assert(H5Pclose(lcpl) >= 0);
    
      status = H5Pclose(plist);
      assert(status == 0);
    
    }


    //////////////////////////////////////////////////////////////////////////
    // Callback for H5Literate
    static herr_t edge_attribute_cb
    (
     hid_t             group_id,
     const char*       name,
     const H5L_info_t* info,
     void*             op_data
     )
    {
      hid_t dset = H5Dopen2(group_id, name, H5P_DEFAULT);
      if (dset < 0) // skip the link, if this is not a dataset
        {
          return 0;
        }
      
      hid_t ftype = H5Dget_type(dset);
      assert(ftype >= 0);
      
      vector< pair<string,AttrKind> >* ptr =
        (vector< pair<string,AttrKind> >*) op_data;
      ptr->push_back(make_pair(name, hdf5::h5type_attr_kind(ftype)));
      
      assert(H5Dclose(dset) >= 0);

      return 0;
    }


    
    /// @brief Discovers the list of edge attributes.
    ///
    /// @param file_name      Input file name
    ///
    /// @param src_pop_name   The name of the source population
    ///
    /// @param dst_pop_name   The name of the destination population
    ///
    /// @param out_attributes    A vector of pairs, one for each edge attribute
    ///                          discovered. The pairs contain the attribute
    ///                          name and the attributes HDF5 file datatype.
    ///                          NOTE: The datatype handles MUST be closed by
    ///                          the caller (via H5Tclose).
    ///
    /// @return                  HDF5 error code.
    herr_t get_edge_attributes
    (
     MPI_Comm                                     comm,
     const std::string&                           file_name,
     const std::string&                           src_pop_name,
     const std::string&                           dst_pop_name,
     const string&                                name_space,
     std::vector< std::pair<std::string,AttrKind> >& out_attributes
     )
    {
      herr_t ierr=0;
      int root=0;
      int rank, size;
      assert(MPI_Comm_size(comm, &size) == MPI_SUCCESS);
      assert(MPI_Comm_rank(comm, &rank) == MPI_SUCCESS);

      if (rank == root)
        {
          hid_t in_file = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
          assert(in_file >= 0);
          out_attributes.clear();
          
          string path = hdf5::edge_attribute_prefix(src_pop_name, dst_pop_name, name_space);
          
          ierr = hdf5::exists_dataset (in_file, path.c_str());
          if (ierr > 0)
            {
              hid_t grp = H5Gopen2(in_file, path.c_str(), H5P_DEFAULT);
              if (grp >= 0)
                {
                  
                  hsize_t idx = 0;
                  ierr = H5Literate(grp, H5_INDEX_NAME, H5_ITER_NATIVE, &idx,
                                    &edge_attribute_cb, (void*) &out_attributes);
                  
                  assert(H5Gclose(grp) >= 0);
                }
            }
          ierr = H5Fclose(in_file);
        }

      vector<char> edge_attributes_sendbuf;  size_t edge_attributes_sendbuf_size=0;
      if (rank == root)
        {
          data::serialize_data(out_attributes, edge_attributes_sendbuf);
          edge_attributes_sendbuf_size = edge_attributes_sendbuf.size();
        }

      assert(MPI_Bcast(&edge_attributes_sendbuf_size, 1, MPI_SIZE_T, root, comm) == MPI_SUCCESS);
      edge_attributes_sendbuf.resize(edge_attributes_sendbuf_size);

      ierr = MPI_Bcast (&edge_attributes_sendbuf[0], edge_attributes_sendbuf_size,
                        MPI_CHAR, root, comm);
      assert(ierr == MPI_SUCCESS);

      out_attributes.clear();

      data::deserialize_data(edge_attributes_sendbuf, out_attributes);

      return ierr;

    }
    

    /// @brief Determines the number of edge attributes for each supported
    //         type.
    ///
    ///
    /// @param attributes    A vector of pairs, one for each edge attribute
    ///                      discovered. The pairs contain the attribute name
    ///                      and the attributes HDF5 file datatype.
    ///
    /// @param num_attrs     A vector which indicates the number of attributes
    ///                      of each type.
    ///
    /// @return                  HDF5 error code.
    herr_t num_edge_attributes
    (
     const std::vector< std::pair<std::string,AttrKind> >& attributes,
     std:: vector <size_t> &num_attrs
     )
    {
      herr_t ierr = 0;
      num_attrs.resize(data::AttrVal::num_attr_types);
      for (size_t i = 0; i < attributes.size(); i++)
        {
          AttrKind attr_kind = attributes[i].second;
          size_t attr_size = attr_kind.size;
          switch (attr_kind.type)
            {
            case UIntVal:
              if (attr_size == 4)
                {
                  num_attrs[data::AttrVal::attr_index_uint32]++;
                }
              else if (attr_size == 2)
                {
                  num_attrs[data::AttrVal::attr_index_uint16]++;
                }
              else if (attr_size == 1)
                {
                  num_attrs[data::AttrVal::attr_index_uint8]++;
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case SIntVal:
              if (attr_size == 4)
                {
                  num_attrs[data::AttrVal::attr_index_int32]++;
                }
              else if (attr_size == 2)
                {
                  num_attrs[data::AttrVal::attr_index_int16]++;
                }
              else if (attr_size == 1)
                {
                  num_attrs[data::AttrVal::attr_index_int8]++;
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case FloatVal:
              num_attrs[data::AttrVal::attr_index_float]++;
              break;
            case EnumVal:
              if (attr_size == 1)
                {
                  num_attrs[data::AttrVal::attr_index_uint8]++;
                }
              else
                {
                  throw runtime_error("Unsupported enumerated attribute size");
                };
              break;
            default:
              throw runtime_error("Unsupported attribute type");
              break;
            }

          assert(ierr >= 0);
        }

      return ierr;
      
    }
    
    template <typename T, class args_t...>
    void append_edge_attribute
    (
     const h5::fd_t& fd,
     const string&  src_pop_name,
     const string&  dst_pop_name,
     const string&  attr_namespace,
     const string&  attr_name,
     const std::vector<T>& value
     )
    {
      int status;

      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<h5::offset_t>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<h5::count_t>(args_tuple, default_count);

      MPI_Comm comm = h5::get_mpi_comm(fd);

      unsigned int size, rank;

      throw_assert(MPI_Comm_size(comm, (int*)&size) == MPI_SUCCESS,
                   "unable to determine MPI communicator size");
      throw_assert(MPI_Comm_rank(comm, (int*)&rank) == MPI_SUCCESS,
                   "unable to determine MPI communicator rank");

      const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

      size_t my_count = value.size();
      std::vector<size_t> all_counts(size);
      throw_assert(MPI_Allgather(&my_count, 1, MPI_SIZE_T, &all_counts[0], 1,
                                 MPI_SIZE_T, comm) == MPI_SUCCESS,
                   "append_edge_attribute: error in MPI_Allgather");

      // calculate the total dataset size and the offset of my piece
      size_t local_value_start = 0,
        global_value_size = 0,
        local_value_size = my_count;
      
      for (size_t p = 0; p < size; ++p)
        {
          if (p < rank)
            {
              local_value_start += (size_t) all_counts[p];
            }
          global_value_size += (size_t) all_counts[p];
        }

      if (global_value_size > 0)
        {
          string path = edge_attribute_path(src_pop_name, dst_pop_name,
                                            attr_namespace, attr_name);
          
          const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_value_size});
          h5::append<T> (file, path, *(std::begin(value)),
                         h5::count{local_value_size}, h5::offset{local_value_start}, 
                         data_dims, max_dims, dcpl, dxpl);
          
        }

      throw_assert(MPI_Comm_free(&comm) == MPI_SUCCESS,
                   "append_edge_attribute: unable to free MPI communicator");

    }

  }
}

#endif
