// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file group_contents.hh
///
///  Reads the contents of a group.
///
///  Copyright (C) 2016-2019 Project Neurograph.
//==============================================================================

#ifndef GROUP_CONTENTS_HH
#define GROUP_CONTENTS_HH

#include <string>
#include <vector>

#include "throw_assert.hh"
#include "h5cpp/all"

namespace neuroh5
{
  namespace lowio
  {
    //////////////////////////////////////////////////////////////////////////
    herr_t contents_cb
    (
     hid_t             grp,
     const char*       name,
     const H5L_info_t* info,
     void*             op_data
     )
    {
      vector<string>* ptr = (vector<string>*)op_data;
      ptr->push_back(string(name));
      return 0;
    }
    
    

    template <>
    void group_contents (const h5::fd_t& fd,
                         const std::string& path,
                         std::vector <std::string>& obj_names)
    {
        const h5::gr_t& grp = h5::open(fd, path);

        hsize_t idx = 0;
        vector<string> op_data;
        
        H5CPP_CHECK_NZ((H5Literate(static_cast<hid_t>(grp), H5_INDEX_NAME, H5_ITER_NATIVE,
                                   &idx, &contents_cb, (void*)&op_data )),
                        std::runtime_error,	h5::error::msg::create_dataset );
        
        for (size_t i = 0; i < op_data.size(); ++i)
          {
            assert(op_data[i].size() > 0);
            obj_names.push_back(op_data[i]);
          }
    }

    
    template <>
    void group_contents (const std::string& file_path,
                         const std::string& group_path,
                         std::vector <std::string>& obj_names)
    {
      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR, h5::default_fapl );

      group_contents(fd, group_path, obj_names);
    }

    
    template <>
    void group_contents_types (const h5::fd_t& fd,
                               const std::string& path,
                               std::vector < std::pair<std::string, hid_t> >& obj_names_types)
    {
        const h5::gr_t& grp = h5::open(fd, path);

        hsize_t idx = 0;
        vector<string, hid_t> op_data;
        
        H5CPP_CHECK_NZ((H5Literate(static_cast<hid_t>(grp), H5_INDEX_NAME, H5_ITER_NATIVE,
                                   &idx, &contents_types_cb, (void*)&op_data )),
                        std::runtime_error,	h5::error::msg::create_dataset );
        
        for (size_t i = 0; i < op_data.size(); ++i)
          {
            assert(op_data[i].size() > 0);
            obj_names_types.push_back(op_data[i]);
          }
    }

    
    template <>
    void group_contents_types (const std::string& file_path,
                               const std::string& group_path,
                               std::vector < std::pair<std::string, hid_t> >& obj_names_types)
    {
      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR, h5::default_fapl );

      group_contents_types(fd, group_path, obj_names_types);
    }

  }
}

#endif
