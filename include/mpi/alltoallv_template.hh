// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file bcast_string_vector.cc
///
///  Template function for sending data via MPI Alltoallv.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#include <mpi.h>

#include <vector>
#include <map>

#include "neuroh5_types.hh"
#include "attr_map.hh"
#include "mpi_error.hh"
#include "mpi_debug.hh"
#include "throw_assert.hh"

using namespace std;


namespace neuroh5
{

  namespace mpi
  {

    template<class T>
    int alltoallv (MPI_Comm comm,
                   const MPI_Datatype datatype,
                   const vector<int>& sendcounts,
                   const vector<int>& sdispls,
                   const T& sendbuf,
                   vector<int>& recvcounts,
                   vector<int>& rdispls,
                   T& recvbuf)
    {
      int ssize; size_t size;
      throw_assert(MPI_Comm_size(comm, &ssize) == MPI_SUCCESS,
                   "alltoallv: unable to obtain size of MPI communicator")

      size = ssize;

      
    /***************************************************************************
     * Send MPI packed data with Alltoallv 
     **************************************************************************/
      recvcounts.resize(size,0);
      rdispls.resize(size,0);
      
      // 1. Each ALL_COMM rank sends a data size to every other rank and
      //    creates sendcounts and sdispls arrays

      MPI_CHECK_SUCCESS(MPI_Alltoall(&sendcounts[0], 1, MPI_INT,
                                     &recvcounts[0], 1, MPI_INT, comm),
                        std::runtime_error, "error in MPI_Alltoall");

    
      // 2. Each rank accumulates the vector sizes and allocates
      //    a receive buffer, recvcounts, and rdispls
      
      size_t recvbuf_size = recvcounts[0];
      for (size_t p = 1; p < size; ++p)
        {
          rdispls[p] = rdispls[p-1] + recvcounts[p-1];
          recvbuf_size += recvcounts[p];
        }

      //assert(recvbuf_size > 0);
      recvbuf.resize(recvbuf_size, 0);

      size_t global_recvbuf_size=0;
      MPI_CHECK_SUCCESS(MPI_Allreduce(&recvbuf_size, &global_recvbuf_size, 1, MPI_SIZE_T, MPI_SUM,
                                      comm),
                        std::runtime_error, "error in MPI_Allreduce");

      if (global_recvbuf_size > 0)
        {
          int status;

          // 3. Each ALL_COMM rank participates in the MPI_Alltoallv
          MPI_CHECK_SUCCESS(MPI_Alltoallv(&sendbuf, &sendcounts[0], &sdispls[0], datatype,
                                          &recvbuf, &recvcounts[0], &rdispls[0], datatype,
                                          comm),
                            std::runtime_error, "error in  MPI_Alltoallv");
        }

      return 0;
    }
  }
}
