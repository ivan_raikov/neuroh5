// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file mpi_error.hh
///
///  MPI-specific error handling.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#ifndef MPI_ERROR_HH
#define MPI_ERROR_HH

#include <mpi.h>
#include <cstring>
#include <vector>
#include <string>


using namespace std;

#define MPI_ERROR_MSG( msg ) std::string( __FILE__ ) + " line#  " + std::to_string( __LINE__ ) + " : " + msg

#define MPI_CHECK_SUCCESS( call, exception, msg ) if ( call != MPI_SUCCESS ) throw exception( MPI_ERROR_MSG( msg ));

namespace neuroh5
{
  namespace mpi
  {
    namespace error
    {
      struct any : public std::runtime_error {
		any() : std::runtime_error("MPI ERROR") {}
		any(const std::string& msg ) : std::runtime_error( msg ){}
      };
      
      struct rank : public any {
		rank() : any() {}
		rank( const std::string& msg ) : any( msg ){}
      };

      struct size : public any {
		size() : any() {}
		size( const std::string& msg ) : any( msg ){}
      };

      struct alltoall : public any {
		alltoall() : any() {}
		alltoall( const std::string& msg ) : any( msg ){}
      };

      struct alltoall : public any {
		alltoall() : any() {}
		alltoall( const std::string& msg ) : any( msg ){}
      };
      
      struct alltoallv : public any {
		alltoallv() : any() {}
		alltoallv( const std::string& msg ) : any( msg ){}
      };
      
      struct allreduce : public any {
		allreduce() : any() {}
		allreduce( const std::string& msg ) : any( msg ){}
      };
    }

  }
}

#endif
