// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file edge_attributes.hh
///
///  Top-level functions for reading and writing edge attributes in DBS
///  (Destination Block Sparse) format.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#ifndef NEUROH5_EDGE_ATTRIBUTES_HH
#define NEUROH5_EDGE_ATTRIBUTES_HH

#include <string>
#include <vector>


#include "h5cpp/all"

#include "neuroh5_types.hh"
#include "attr_val.hh"
#include "infer_datatype.hh"
#include "attr_kind_datatype.hh"
#include "edge_attribute_datasets.hh"
#include "exists_dataset.hh"

namespace neuroh5
{
  namespace io
  {

    /// @brief Reads the values of edge attributes.
    ///
    /// @param file_name      Input file name
    ///
    /// @param src_pop_name   The name of the source population.
    ///
    /// @param dst_pop_name   The name of the destination population.
    ///
    /// @param attr_name      The name of the attribute.
    ///
    /// @param edge_base      Edge offset (returned by read_dbs_projection).
    ///
    /// @param edge_count     Edge count.
    ///
    /// @param attr_h5type    The HDF5 type of the attribute.
    ///
    /// @param attr_values    An EdgeNamedAttr object that holds attribute
    //                        values.
    void read_edge_attributes
    (
     MPI_Comm              comm,
     const std::string&    file_name,
     const std::string&    src_pop_name,
     const std::string&    dst_pop_name,
     const std::string&    name_space,
     const std::string&    attr_name,
     const DST_PTR_T       edge_base,
     const DST_PTR_T       edge_count,
     const AttrKind        attr_kind,
     data::NamedAttrVal&   attr_values,
     bool collective = true
     )
    {
      hid_t file;
      herr_t ierr = 0;
      hsize_t block = edge_count, base = edge_base;

      hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
      assert(fapl >= 0);
      assert(H5Pset_fapl_mpio(fapl, comm, MPI_INFO_NULL) >= 0);

      file = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, fapl);
      assert(file >= 0);

      /* Create property list for collective dataset operations. */
      hid_t rapl = H5Pcreate (H5P_DATASET_XFER);
      if (collective)
        {
          ierr = H5Pset_dxpl_mpio (rapl, H5FD_MPIO_COLLECTIVE);
        }

      string dset_path = hdf5::edge_attribute_path(src_pop_name, dst_pop_name, name_space, attr_name);
      ierr = hdf5::exists_dataset (file, dset_path.c_str());
      if (ierr > 0)
        {
          hsize_t one = 1;

          hid_t mspace = H5Screate_simple(1, &block, NULL);
          assert(mspace >= 0);
          ierr = H5Sselect_all(mspace);
          assert(ierr >= 0);
          
          hid_t dset = H5Dopen2 (file, dset_path.c_str(), H5P_DEFAULT);
          assert(dset >= 0);
          
          // make hyperslab selection
          hid_t fspace = H5Dget_space(dset);
          assert(fspace >= 0);
          
          if (block > 0)
            {
              ierr = H5Sselect_hyperslab(fspace, H5S_SELECT_SET, &base, NULL, &one, &block);
            }
          else
            {
              ierr = H5Sselect_none(fspace);
            }
          assert(ierr >= 0);
          
          size_t attr_size = attr_kind.size;
          hid_t attr_h5type = hdf5::attr_kind_h5type(attr_kind);
          switch (attr_kind.type)
            {
            case UIntVal:
              if (attr_size == 4)
                {
                  vector <uint32_t> attr_values_uint32;
                  attr_values_uint32.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_uint32[0]);
                  attr_values.insert(string(attr_name), attr_values_uint32);
                }
              else if (attr_size == 2)
                {
                  vector <uint16_t>    attr_values_uint16;
                  attr_values_uint16.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_uint16[0]);
                  attr_values.insert(string(attr_name), attr_values_uint16);
                }
              else if (attr_size == 1)
                {
                  vector <uint8_t> attr_values_uint8;
                  attr_values_uint8.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_uint8[0]);
                  attr_values.insert(string(attr_name), attr_values_uint8);
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case SIntVal:
              if (attr_size == 4)
                {
                  vector <int32_t>  attr_values_int32;
                  attr_values_int32.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_int32[0]);
                  attr_values.insert(string(attr_name), attr_values_int32);
                }
              else if (attr_size == 2)
                {
                  vector <int16_t>  attr_values_int16;
                  attr_values_int16.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_int16[0]);
                  attr_values.insert(string(attr_name), attr_values_int16);
                }
              else if (attr_size == 1)
                {
                  vector <int8_t>  attr_values_int8;
                  attr_values_int8.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_int8[0]);
                  attr_values.insert(string(attr_name), attr_values_int8);
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case FloatVal:
              {
                vector <float>  attr_values_float;
                attr_values_float.resize(edge_count);
                ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                               &attr_values_float[0]);
                attr_values.insert(string(attr_name), attr_values_float);
              }
              break;
            case EnumVal:
              if (attr_size == 1)
                {
                  vector <uint8_t>  attr_values_uint8;;
                  attr_values_uint8.resize(edge_count);
                  ierr = H5Dread(dset, attr_h5type, mspace, fspace, rapl,
                                 &attr_values_uint8[0]);
                  attr_values.insert(string(attr_name), attr_values_uint8);
                }
              else
                {
                  throw runtime_error("Unsupported enumerated attribute size");
                };
              break;
            default:
              throw runtime_error("Unsupported attribute type");
              break;
            }
          
          assert(ierr >= 0);
          
          assert(H5Sclose(fspace) >= 0);
          assert(H5Dclose(dset) >= 0);
          assert(H5Sclose(mspace) >= 0);
        }

      assert(H5Fclose(file) >= 0);
      assert(H5Pclose(fapl) >= 0);
      assert(H5Pclose(rapl) >= 0);

      return ierr;
      
    }

    void read_all_edge_attributes
    (
     MPI_Comm                                           comm,
     const std::string&                                 file_name,
     const std::string&                                 src_pop_name,
     const std::string&                                 dst_pop_name,
     const std::string&                                 name_space,
     const DST_PTR_T                                    edge_base,
     const DST_PTR_T                                    edge_count,
     const std::vector< std::pair<std::string,AttrKind> >& edge_attr_info,
     data::NamedAttrVal&                              edge_attr_values
     )
    {
    
      int ierr = 0;
      vector<NODE_IDX_T> src_vec, dst_vec;

      for (size_t j = 0; j < edge_attr_info.size(); j++)
        {
          string attr_name   = edge_attr_info[j].first;
          AttrKind attr_kind = edge_attr_info[j].second;
          assert ((ierr = read_edge_attributes(comm, file_name, src_pop_name, dst_pop_name,
                                               name_space, attr_name, edge_base, edge_count,
                                               attr_kind, edge_attr_values))
                  >= 0);
        }

      return ierr;
    }

    
    template <typename T>
    void write_edge_attribute
    (
     h5::fd_t&                fd,
     const std::string&       path,
     const std::vector<T>&    value,
     const bool collective = true
     )
    {
      // get a file handle and retrieve the MPI info
      hid_t file = H5Iget_file_id(loc);
      assert(file >= 0);

      MPI_Comm comm;
      MPI_Info info;
      hid_t fapl = H5Fget_access_plist(file);
      assert(H5Pget_fapl_mpio(fapl, &comm, &info) >= 0);

      hid_t wapl = H5P_DEFAULT;
      if (collective)
	{
	  wapl = H5Pcreate(H5P_DATASET_XFER);
	  assert(wapl >= 0);
	  assert(H5Pset_dxpl_mpio(wapl, H5FD_MPIO_COLLECTIVE) >= 0);
	}

      int ssize, srank;
      assert(MPI_Comm_size(comm, &ssize) == MPI_SUCCESS);
      assert(MPI_Comm_rank(comm, &srank) == MPI_SUCCESS);
      size_t size, rank;
      size = (size_t)ssize;
      rank = (size_t)srank;

      uint32_t my_count = (uint32_t)value.size();
      std::vector<uint32_t> all_counts(size);
      assert(MPI_Allgather(&my_count, 1, MPI_UINT32_T, &all_counts[0], 1,
                           MPI_UINT32_T, comm) == MPI_SUCCESS);

      // calculate the total dataset size and the offset of my piece
      hsize_t start = 0, total = 0, count = 1, block = my_count;
      for (size_t p = 0; p < size; ++p)
        {
          if (p < rank)
            {
              start += (hsize_t) all_counts[p];
            }
          total += (hsize_t) all_counts[p];
        }

      // create dataspaces and selections
      hid_t mspace = H5Screate_simple(1, &block, &block);
      assert(mspace >= 0);
      assert(H5Sselect_all(mspace) >= 0);
      hid_t fspace = H5Screate_simple(1, &total, &total);
      assert(fspace >= 0);
      assert(H5Sselect_hyperslab(fspace, H5S_SELECT_SET, &start, NULL,
                                 &count, &block) >= 0);

      // figure the type

      T dummy;
      hid_t ftype = infer_datatype(dummy);
      assert(ftype >= 0);
      hid_t mtype = H5Tget_native_type(ftype, H5T_DIR_ASCEND);
      assert(mtype >= 0);


      hid_t lcpl = H5Pcreate(H5P_LINK_CREATE);
      assert(lcpl >= 0);
      assert(H5Pset_create_intermediate_group(lcpl, 1) >= 0);

      hid_t dset = H5Dcreate(loc, path.c_str(), ftype, fspace,
                             lcpl, H5P_DEFAULT, H5P_DEFAULT);
      assert(dset >= 0);
      assert(H5Dwrite(dset, mtype, mspace, fspace, wapl, &value[0])
             >= 0);

      assert(H5Dclose(dset) >= 0);
      assert(H5Tclose(mtype) >= 0);
      assert(H5Sclose(fspace) >= 0);
      assert(H5Sclose(mspace) >= 0);
      assert(H5Pclose(lcpl) >= 0);
      assert(H5Pclose(wapl) >= 0);

      assert(MPI_Comm_free(&comm) == MPI_SUCCESS);
      if (info != MPI_INFO_NULL)
        {
          assert(MPI_Info_free(&info) == MPI_SUCCESS);
        }

      assert(H5Pclose (file) >= 0);
      assert(H5Pclose (fapl) >= 0);

      return 0;
    }
    
    template <typename T>
    herr_t append_edge_attribute
    (
     hid_t                    loc,
     const string&            src_pop_name,
     const string&            dst_pop_name,
     const std::string&       attr_namespace,
     const std::string&       attr_name,
     const std::vector<T>&    value,
     const size_t chunk_size = 4000,
     const bool collective = true
     )
    {
      // get a file handle and retrieve the MPI info
      hid_t file = H5Iget_file_id(loc);
      assert(file >= 0);

      MPI_Comm comm;
      MPI_Info info;
      hid_t fapl = H5Fget_access_plist(file);
      assert(H5Pget_fapl_mpio(fapl, &comm, &info) >= 0);

      /*
      int ssize, srank;
      assert(MPI_Comm_size(comm, &ssize) == MPI_SUCCESS);
      assert(MPI_Comm_rank(comm, &srank) == MPI_SUCCESS);
      size_t size, rank;
      size = (size_t)ssize;
      rank = (size_t)srank;
      */

      T dummy;
      hid_t ftype = infer_datatype(dummy);
      assert(ftype >= 0);
      hid_t mtype = H5Tget_native_type(ftype, H5T_DIR_ASCEND);
      assert(mtype >= 0);

      string attr_prefix = hdf5::edge_attribute_prefix(src_pop_name,
                                                       dst_pop_name,
                                                       attr_namespace);
      string attr_path = hdf5::edge_attribute_path(src_pop_name, dst_pop_name,
                                                   attr_namespace, attr_name);
      
      if (!(hdf5::exists_dataset (file, attr_path) > 0))
        {
          hdf5::create_edge_attribute_datasets(file, src_pop_name, dst_pop_name,
                                               attr_namespace, attr_name,
                                               ftype, chunk_size);
        }

      hdf5::append_edge_attribute<T>(file, src_pop_name, dst_pop_name,
                                     attr_namespace, attr_name,
                                     value);
      
      assert(MPI_Comm_free(&comm) == MPI_SUCCESS);
      if (info != MPI_INFO_NULL)
        {
          assert(MPI_Info_free(&info) == MPI_SUCCESS);
        }
      
      assert(H5Fclose (file) >= 0);
      assert(H5Pclose (fapl) >= 0);

      return 0;
    }

    template <class T, class args_t ...>
    void write_edge_attribute_map
    (
     h5::fd_t& fd,
     const string &src_pop_name,
     const string &dst_pop_name,
     const map <string, data::NamedAttrVal>& edge_attr_map,
     const map <string, pair <size_t, data::AttrIndex > >& edge_attr_index,
     args_t&&... args)
    {
      for (auto const& iter : edge_attr_map)
        {
          const string& attr_namespace = iter.first;
          const data::NamedAttrVal& edge_attr_values = iter.second;
          
          for (size_t i=0; i<edge_attr_values.size_attr_vec<T>(); i++)
            {
              const string& attr_name = edge_attr_index.at(attr_namespace).attr_names<T>()[i];
              string path = edge_attribute_path(src_pop_name, dst_pop_name, attr_namespace, attr_name);
              write_edge_attribute<T>(fd, path, edge_attr_values.attr_vec<T>(i), args);
            }
        }
    }


    template <class T>
    void append_edge_attribute_map (h5::fd_t& fd,
                                    const string &src_pop_name,
                                    const string &dst_pop_name,
                                    const map <string, data::NamedAttrVal>& edge_attr_map,
                                    const map <string, pair <size_t, data::AttrIndex > >& edge_attr_index)
    {
      for (auto const& iter : edge_attr_map)
        {
          const string& attr_namespace = iter.first;
          const data::NamedAttrVal& edge_attr_values = iter.second;
          
          for (size_t i=0; i<edge_attr_values.size_attr_vec<T>(); i++)
            {
              const string& attr_name = edge_attr_index.at(attr_namespace).attr_names<T>()[i];
              append_edge_attribute<T>(fd, src_pop_name, dst_pop_name, attr_namespace, attr_name, edge_attr_values.attr_vec<T>(i));
            }
        }
    }

        /////////////////////////////////////////////////////////////////////////
    int read_all_edge_attributes
    (
     MPI_Comm                            comm,
     const string&                       file_name,
     const string&                       src_pop_name,
     const string&                       dst_pop_name,
     const string&                       name_space,
     const DST_PTR_T                     edge_base,
     const DST_PTR_T                     edge_count,
     const vector< pair<string,AttrKind> >& edge_attr_info,
     data::NamedAttrVal&                 edge_attr_values
     )
    {
      int ierr = 0;
      vector<NODE_IDX_T> src_vec, dst_vec;

      for (size_t j = 0; j < edge_attr_info.size(); j++)
        {
          string attr_name   = edge_attr_info[j].first;
          AttrKind attr_kind = edge_attr_info[j].second;
          assert ((ierr = read_edge_attributes(comm, file_name, src_pop_name, dst_pop_name,
                                               name_space, attr_name, edge_base, edge_count,
                                               attr_kind, edge_attr_values))
                  >= 0);
        }

      return ierr;
    }
    

    /////////////////////////////////////////////////////////////////////////
    herr_t read_edge_attribute_selection
    (
     MPI_Comm              comm,
     const string&         file_name,
     const string&         src_pop_name,
     const string&         dst_pop_name,
     const string&         name_space,
     const string&         attr_name,
     const DST_PTR_T&       edge_base,
     const DST_PTR_T&       edge_count,
     const vector<NODE_IDX_T>&   selection_dst_idx,
     const vector<DST_PTR_T>&    selection_dst_ptr,
     const vector< pair<hsize_t,hsize_t> >& ranges,
     const AttrKind        attr_kind,
     data::NamedAttrVal&   attr_values,
     bool collective
     )
    {
      hid_t file;
      herr_t ierr = 0;
      hsize_t block = 0;
      
      hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
      assert(fapl >= 0);
      assert(H5Pset_fapl_mpio(fapl, comm, MPI_INFO_NULL) >= 0);

      file = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, fapl);
      assert(file >= 0);

      /* Create property list for collective dataset operations. */
      hid_t rapl = H5Pcreate (H5P_DATASET_XFER);
      if (collective)
        {
          ierr = H5Pset_dxpl_mpio (rapl, H5FD_MPIO_COLLECTIVE);
        }

      
      string dset_path = hdf5::edge_attribute_path(src_pop_name, dst_pop_name, name_space, attr_name);
      ierr = hdf5::exists_dataset (file, dset_path.c_str());
      if (ierr > 0)
        {
          size_t attr_size = attr_kind.size;
          hid_t attr_h5type = hdf5::attr_kind_h5type(attr_kind);
          switch (attr_kind.type)
            {
            case UIntVal:
              if (attr_size == 4)
                {
                  vector <uint32_t> attr_values_uint32;
                  ierr = hdf5::read_selection<uint32_t>(file, dset_path, attr_h5type, ranges,
                                                        attr_values_uint32, rapl);
                  attr_values.insert(string(attr_name), attr_values_uint32);
                }
              else if (attr_size == 2)
                {
                  vector <uint16_t>    attr_values_uint16;
                  ierr = hdf5::read_selection<uint16_t>(file, dset_path, attr_h5type, ranges,
                                                        attr_values_uint16, rapl);
                  attr_values.insert(string(attr_name), attr_values_uint16);
                }
              else if (attr_size == 1)
                {
                  vector <uint8_t> attr_values_uint8;
                  ierr = hdf5::read_selection<uint8_t>(file, dset_path, attr_h5type, ranges,
                                                       attr_values_uint8, rapl);
                  attr_values.insert(string(attr_name), attr_values_uint8);
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case SIntVal:
              if (attr_size == 4)
                {
                  vector <int32_t>  attr_values_int32;
                  ierr = hdf5::read_selection<int32_t>(file, dset_path, attr_h5type, ranges,
                                                       attr_values_int32, rapl);
                  attr_values.insert(string(attr_name), attr_values_int32);
                }
              else if (attr_size == 2)
                {
                  vector <int16_t>  attr_values_int16;
                  ierr = hdf5::read_selection<int16_t>(file, dset_path, attr_h5type, ranges,
                                                       attr_values_int16, rapl);
                  attr_values.insert(string(attr_name), attr_values_int16);
                }
              else if (attr_size == 1)
                {
                  vector <int8_t>  attr_values_int8;
                  ierr = hdf5::read_selection<int8_t>(file, dset_path, attr_h5type, ranges,
                                                      attr_values_int8, rapl);
                  attr_values.insert(string(attr_name), attr_values_int8);
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case FloatVal:
              {
                vector <float>  attr_values_float;
                ierr = hdf5::read_selection<float>(file, dset_path, attr_h5type, ranges,
                                                   attr_values_float, rapl);
                attr_values.insert(string(attr_name), attr_values_float);
              }
              break;
            case EnumVal:
              if (attr_size == 1)
                {
                  vector <uint8_t>  attr_values_uint8;
                  ierr = hdf5::read_selection<uint8_t>(file, dset_path, attr_h5type, ranges,
                                                       attr_values_uint8, rapl);

                  attr_values.insert(string(attr_name), attr_values_uint8);
                }
              else
                {
                  throw runtime_error("Unsupported enumerated attribute size");
                };
              break;
            default:
              throw runtime_error("Unsupported attribute type");
              break;
            }
          
          assert(ierr >= 0);
          
        }

      assert(H5Fclose(file) >= 0);
      assert(H5Pclose(fapl) >= 0);
      assert(H5Pclose(rapl) >= 0);

      return ierr;
    }


    /////////////////////////////////////////////////////////////////////////
    int read_all_edge_attribute_selection
    (
     MPI_Comm                            comm,
     const string&                       file_name,
     const string&                       src_pop_name,
     const string&                       dst_pop_name,
     const string&                       name_space,
     const DST_PTR_T&       edge_base,
     const DST_PTR_T&       edge_count,
     const vector<NODE_IDX_T>&   selection_dst_idx,
     const vector<DST_PTR_T>&    selection_dst_ptr,
     const vector< pair<hsize_t,hsize_t> >& src_idx_ranges,
     const vector< pair<string,AttrKind> >& edge_attr_info,
     data::NamedAttrVal&                 edge_attr_values
     )
    {
      herr_t ierr = 0;
      vector<NODE_IDX_T> src_vec, dst_vec;

      for (size_t j = 0; j < edge_attr_info.size(); j++)
        {
          string attr_name   = edge_attr_info[j].first;
          AttrKind attr_kind = edge_attr_info[j].second;
          assert ((ierr = read_edge_attribute_selection(comm, file_name, src_pop_name, dst_pop_name,
                                                        name_space, attr_name, edge_base, edge_count,
                                                        selection_dst_idx, selection_dst_ptr, src_idx_ranges,
                                                        attr_kind, edge_attr_values))
                  >= 0);
        }

      return ierr;
    }


  }
}


#endif
