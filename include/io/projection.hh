// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file projection.hh
///
///  Top-level functions for reading and writing projections in DBS
///  (Destination Block Sparse) format.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#ifndef NEUROH5_PROJECTION_HH
#define NEUROH5_PROJECTION_HH

#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include <mpi.h>

#include "h5cpp/all"

#include "neuroh5_types.hh"
#include "neuroh5_paths.hh"
#include "create_group.hh"
#include "exists_dataset.hh"
#include "edge_attributes.hh"
#include "throw_assert.hh"
#include "mpe_seq.hh"
#include "mpi_debug.hh"

using namespace std;

namespace neuroh5
{
  namespace io
  {

    /// @brief Reads the edges for the given projections
    ///
    /// @param comm          MPI communicator
    ///
    /// @param file_name     Input file name
    ///
    /// @param src_pop_name  Source population name
    ///
    /// @param dst_pop_name  Destination population name
    ///
    /// @param dst_start     Updated with global starting index of destination
    ///                      population
    ///
    /// @param src_start     Updated with global starting index of source
    ///                      population
    ///
    /// @param nedges        Total number of edges in the projection
    ///
    /// @param block_base    Global index of the first block read by this task
    ///
    /// @param edge_base     Global index of the first edge read by this task
    ///
    /// @param dst_blk_ptr   Destination Block Pointer (pointer to Destination
    ///                      Pointer for blocks of connectivity)
    ///
    /// @param dst_idx       Destination Index (starting destination index for
    ///                      each block)
    ///
    /// @param dst_ptr       Destination Pointer (pointer to Source Index where
    ///                      the source indices for a given destination can be
    ///                      located)
    ///
    /// @param src_idx       Source Index (source indices of edges)
    ///
    /// @return              HDF5 error code
    template <class args_t ...>
    void read_projection
    (
     const h5::fd_t&                 fd,
     const pop_range_map_t&          pop_ranges,
     const std::set< std::pair<pop_t, pop_t> >& pop_pairs,
     const std::string&              src_pop_name,
     const std::string&              dst_pop_name,
     const NODE_IDX_T&               dst_start,
     const NODE_IDX_T&               src_start,
     const vector<string>&           edge_attr_name_spaces,
     std::vector<edge_map_t>&        prj_vector,
     vector < map <string, vector < vector<string> > > > & edge_attr_names_vector,
     size_t&                         local_num_nodes,
     size_t&                         local_num_edges,
     size_t&                         total_num_edges,
     size_t&                         local_read_blocks,
     size_t&                         total_read_blocks,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = h5::get_mpi_comm(fd);

      DST_BLK_PTR_T block_base;
      DST_PTR_T edge_base, edge_count;
      vector<DST_BLK_PTR_T> dst_blk_ptr;
      vector<NODE_IDX_T> dst_idx;
      vector<DST_PTR_T> dst_ptr;
      vector<NODE_IDX_T> src_idx;
      map<string, data::NamedAttrVal> edge_attr_map;
      
      mpi::MPI_DEBUG(comm, "read_projection: ", src_pop_name, " -> ", dst_pop_name);
      lowio::read_projection_datasets(fd, src_pop_name, dst_pop_name,
                                      dst_start, src_start, block_base, edge_base,
                                      dst_blk_ptr, dst_idx, dst_ptr, src_idx,
                                      total_num_edges, total_read_blocks, local_read_blocks,
                                      args...);
      
      mpi::MPI_DEBUG(comm, "read_projection: validating projection ", src_pop_name, " -> ", dst_pop_name);
      
      // validate the edges
      throw_assert(validate_edge_list(dst_start, src_start, dst_blk_ptr, dst_idx,
                                      dst_ptr, src_idx, pop_ranges, pop_pairs) ==
                   true, "read_projection: invalid edge list");
      
      edge_count = src_idx.size();
      local_num_edges = edge_count;
      
      map <string, vector < vector<string> > > edge_attr_names;
      for (string attr_namespace : attr_namespaces) 
        {
          vector< pair<string,AttrKind> > edge_attr_info;
          
          get_edge_attributes(fd, src_pop_name, dst_pop_name,
                              attr_namespace, edge_attr_info, args...);
          
          read_all_edge_attributes(fd, src_pop_name, dst_pop_name, attr_namespace,
                                   edge_base, edge_count, edge_attr_info,
                                   edge_attr_map[attr_namespace], args...);
          
          edge_attr_map[attr_namespace].attr_names(edge_attr_names[attr_namespace]);
        }
      
      size_t local_prj_num_edges=0;

      edge_map_t prj_edge_map;
      // append to the vectors representing a projection (sources,
      // destinations, edge attributes)
      data::append_edge_map(dst_start, src_start, dst_blk_ptr, dst_idx,
                            dst_ptr, src_idx, attr_namespaces, edge_attr_map,
                            local_prj_num_edges, prj_edge_map,
                            EdgeMapDst);
      local_num_nodes = prj_edge_map.size();
      
      // ensure that all edges in the projection have been read and
      // appended to edge_list
      throw_assert(local_prj_num_edges == edge_count,
                   "read_projection: edge count mismatch");
    
      prj_vector.push_back(prj_edge_map);
      edge_attr_names_vector.push_back (edge_attr_names);

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm), std::runtime_error,
                        "append_projection: unable to free MPI communicator");
      
    }


    template <class args_t ...>
    void read_projection
    (
     const string&                   file_name,
     const pop_range_map_t&          pop_ranges,
     const std::set< std::pair<pop_t, pop_t> >& pop_pairs,
     const std::string&              src_pop_name,
     const std::string&              dst_pop_name,
     const NODE_IDX_T&               dst_start,
     const NODE_IDX_T&               src_start,
     const vector<string>&           edge_attr_name_spaces,
     std::vector<edge_map_t>&        prj_vector,
     vector < map <string, vector < vector<string> > > > & edge_attr_names_vector,
     size_t&                         local_num_nodes,
     size_t&                         local_num_edges,
     size_t&                         total_num_edges,
     size_t&                         local_read_blocks,
     size_t&                         total_read_blocks,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      h5::fapl_t fapl = h5::fclose_default | h5::fapl_core{4096,1} |
        h5::core_write_tracking{false,1} | h5::fapl_family{H5F_FAMILY_DEFAULT,0};
      
      auto fd = h5::open(file_name.c_str(), H5F_ACC_RDONLY,
                         MPI_COMM_NULL ? fapl : fapl | h5::fapl_mpio{comm, MPI_INFO_NULL});

      read_projection(fd, pop_ranges, pop_pairs, src_pop_name, dst_pop_name,
                      dst_start, src_start, edge_attr_name_spaces, prj_vector,
                      edge_attr_names_vector, local_num_nodes, local_num_edges,
                      local_read_blocks, total_read_blocks, comm, args...);

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm),
                        std::runtime_error, "read_projection: unable to free MPI communicator");

    }


    template <class args_t ...>
    void scatter_read_projection
    (
     const h5::fd_t& fd,
     const string& src_pop_name,
     const string& dst_pop_name, 
     const std::vector< std::string >&  attr_namespaces,
     const std::map<NODE_IDX_T, rank_t>&  node_rank_map,
     const std::vector<pop_range_t>& pop_vector,
     const std::map<NODE_IDX_T, std::pair<uint32_t,pop_t> >& pop_ranges,
     const std::vector< std::pair<pop_t, string> >& pop_labels,
     const std::set< std::pair<pop_t, pop_t> >& pop_pairs,
     const EdgeMapType edge_map_type, 
     MPI_Comm all_comm,
     const int io_size,
     std::vector < edge_map_t >& prj_vector,
     std::vector < map <string, std::vector < std::vector<string> > > > & edge_attr_names_vector,
     size_t &local_num_nodes, size_t &local_num_edges, size_t &total_num_edges,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm all_comm = h5::get_mpi_comm(fd);

      // MPI Communicator for I/O ranks
      MPI_Comm io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1;

      unsigned int rank, size;
      MPI_CHECK_SUCCESS(MPI_Comm_size(all_comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(all_comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");

      unsigned int io_size_value;
      if (size < io_size)
        {
          io_size_value = size > 0 ? size : 1;
        }
      else
        {
          io_size_value = io_size > 0 ? (size_t)io_size : 1;
        }

      set<size_t> io_rank_set;
      data::range_sample(size, io_size_value, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
      
      // Am I an I/O rank?
      if (is_io_rank)
        {
          MPI_Comm_split(all_comm,io_color,rank,&io_comm);
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);
        }
      else
        {
          MPI_Comm_split(all_comm,0,rank,&io_comm);
        }

      vector<NODE_IDX_T> send_edges, recv_edges, total_recv_edges;
      rank_edge_map_t prj_rank_edge_map;
      edge_map_t prj_edge_map;
      size_t num_edges = 0;
      map<string, vector< vector<string> > > edge_attr_names;
      
      local_num_nodes=0; local_num_edges=0;
      
      {
        vector<char> recvbuf;
        vector<int> recvcounts, rdispls;

        {
          vector<char> sendbuf; 
          vector<int> sendcounts(size,0), sdispls(size,0);

          mpi::MPI_DEBUG(all_comm, "scatter_read_projection: ", src_pop_name, " -> ", dst_pop_name, "\n");

          if (is_io_rank)
            {
              DST_BLK_PTR_T block_base;
              DST_PTR_T edge_base, edge_count;
              NODE_IDX_T dst_start, src_start;
              vector<DST_BLK_PTR_T> dst_blk_ptr;
              vector<NODE_IDX_T> dst_idx;
              vector<DST_PTR_T> dst_ptr;
              vector<NODE_IDX_T> src_idx;
              map<string, data::NamedAttrVal> edge_attr_map;
              hsize_t local_read_blocks, total_read_blocks;
              
              uint32_t dst_pop_idx=0, src_pop_idx=0;
              bool src_pop_set = false, dst_pop_set = false;
            
              for (size_t i=0; i< pop_labels.size(); i++)
                {
                  if (src_pop_name == get<1>(pop_labels[i]))
                    {
                      src_pop_idx = get<0>(pop_labels[i]);
                      src_pop_set = true;
                    }
                  if (dst_pop_name == get<1>(pop_labels[i]))
                    {
                      dst_pop_idx = get<0>(pop_labels[i]);
                      dst_pop_set = true;
                    }
                }
              throw_assert(dst_pop_set && src_pop_set,
                           "scatter_read_projection: unable to determine source or target population index");

              dst_start = pop_vector[dst_pop_idx].start;
              src_start = pop_vector[src_pop_idx].start;

              mpi::MPI_DEBUG(io_comm, "scatter_read_projection: reading projection ", src_pop_name, " -> ", dst_pop_name);
              lowio::read_projection_datasets(fd, src_pop_name, dst_pop_name,
                                              dst_start, src_start, block_base, edge_base,
                                              dst_blk_ptr, dst_idx, dst_ptr, src_idx,
                                              total_num_edges, total_read_blocks, local_read_blocks,
                                              io_comm, args...);
          
              mpi::MPI_DEBUG(io_comm, "scatter_read_projection: validating projection ", src_pop_name, " -> ", dst_pop_name);
              // validate the edges
              throw_assert(validate_edge_list(dst_start, src_start, dst_blk_ptr, dst_idx, dst_ptr, src_idx,
                                              pop_ranges, pop_pairs) == true,
                           "scatter_read_projection: invalid edge list");

          
              edge_count = src_idx.size();
              mpi::MPI_DEBUG(io_comm, "scatter_read_projection: reading attributes for ", src_pop_name, " -> ", dst_pop_name);
              for (const string& attr_namespace : attr_namespaces) 
                {
                  vector< pair<string,AttrKind> > edge_attr_info;
                  graph::get_edge_attributes(fd, src_pop_name, dst_pop_name,
                                             attr_namespace, edge_attr_info, io_comm, args...);

                  graph::read_all_edge_attributes(fd, src_pop_name, dst_pop_name, attr_namespace,
                                                  edge_base, edge_count, edge_attr_info,
                                                  edge_attr_map[attr_namespace], io_comm, args...);
                  
                  edge_attr_map[attr_namespace].attr_names(edge_attr_names[attr_namespace]);
                }

              
              // append to the edge map
              data::append_rank_edge_map(size, dst_start, src_start, dst_blk_ptr, dst_idx, dst_ptr, src_idx,
                                         attr_namespaces, edge_attr_map, node_rank_map, num_edges,
                                         prj_rank_edge_map, edge_map_type);
          
              mpi::MPI_DEBUG(io_comm, "scatter_read_projection: read ", num_edges,
                             " edges from projection ", src_pop_name, " -> ", dst_pop_name);
          
              // ensure that all edges in the projection have been read and appended to edge_list
              throw_assert(num_edges == src_idx.size(),
                           "scatter_read_projection: edge count mismatch");
          
              size_t num_packed_edges = 0;
          
              data::serialize_rank_edge_map (size, rank, prj_rank_edge_map, 
                                             num_packed_edges, sendcounts, sendbuf, sdispls);

              // ensure the correct number of edges is being packed
              throw_assert(num_packed_edges == num_edges,
                           "scatter_read_projection: packed edge count mismatch");
              
              mpi::MPI_DEBUG(io_comm, "scatter_read_projection: packed ", num_packed_edges,
                        " edges from projection ", src_pop_name, " -> ", dst_pop_name);

            } // is_io_rank

          MPI_CHECK_SUCCESS(MPI_Barrier(all_comm), std::runtime_error,
                            "scatter_read_graph: error in MPI barrier");
          MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm),
                            std::runtime_error,
                            "scatter_read_graph: unable to free MPI communicator");

          MPI_CHECK_SUCCESS( mpi::alltoallv<char>(all_comm, MPI_CHAR, sendcounts, sdispls,  *(std::cbegin(sendbuf)),
                                                  recvcounts, rdispls, *(std::begin(recvbuf))),
                             std::runtime_error, "error in mpi::alltoallv");
        }

        if (recvbuf.size() > 0)
          {
            data::deserialize_rank_edge_map (size, recvbuf, recvcounts, rdispls, 
                                             prj_edge_map, local_num_nodes, local_num_edges);
          }

        if (!attr_namespaces.empty())
          {
            vector<char> sendbuf; uint32_t sendbuf_size=0;
            if (rank == 0)
              {
                data::serialize_data(edge_attr_names, sendbuf);
                sendbuf_size = sendbuf.size();
              }
            
            MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf_size, 1, MPI_UINT32_T, 0, all_comm),
                              std::runtime_error, "error in MPI_Bcast");
                              
            sendbuf.resize(sendbuf_size);
            MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf[0], sendbuf_size, MPI_CHAR, 0, all_comm),
                              std::runtime_error, "error in MPI_Bcast");
            
            if (rank != 0)
              {
                data::deserialize_data(sendbuf, edge_attr_names);
              }
            edge_attr_names_vector.push_back(edge_attr_names);
            
          }
      }

      
      mpi::MPI_DEBUG(all_comm, "scatter_read_projection: unpacked ", local_num_edges,
                     " edges for projection ", src_pop_name, " -> ", dst_pop_name);
      
      prj_vector.push_back(prj_edge_map);
      MPI_CHECK_SUCCESS(MPI_Comm_free(&all_comm),
                        std::runtime_error, "scatter_read_projection: unable to free MPI communicator");

      
    }
    
  
    template <class args_t ...>
    void write_projection
    (
     const h5::fd_t&           fd,
     const string&             src_pop_name,
     const string&             dst_pop_name,
     const NODE_IDX_T&         src_start,
     const NODE_IDX_T&         src_end,
     const NODE_IDX_T&         dst_start,
     const NODE_IDX_T&         dst_end,
     const size_t&             num_edges,
     const edge_map_t&         prj_edge_map,
     const std::map <std::string, std::pair <size_t, data::AttrIndex > >& edge_attr_index,
     const size_t             block_size,
     const size_t             chunk_size,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = h5::get_mpi_comm(fd);

      unsigned int size=1, rank=0;
      if (comm != MPI_COMM_NULL)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }

      // do a sanity check on the input
      throw_assert(src_start < src_end,
                   "write_projection: invalid source index range");
      throw_assert(dst_start < dst_end,
                   "write_projection: invalid destination index range");
      
      size_t num_dest = prj_edge_map.size();
      size_t num_blocks = num_dest > 0 ? 1 : 0;

      // create relative destination pointers and source index
      vector<DST_BLK_PTR_T> dst_blk_ptr; 
      vector<DST_PTR_T> dst_ptr;
      vector<NODE_IDX_T> dst_blk_idx, src_idx;
      NODE_IDX_T first_idx = 0, last_idx = 0;
      size_t num_block_edges = 0, num_prj_edges = 0;
      if (!prj_edge_map.empty())
        {
          first_idx = (prj_edge_map.begin())->first;
          last_idx  = first_idx;
          dst_blk_idx.push_back(first_idx - dst_start);
          dst_blk_ptr.push_back(0);
          for (auto iter = prj_edge_map.begin(); iter != prj_edge_map.end(); ++iter)
            {
              NODE_IDX_T dst = iter->first;
              edge_tuple_t et = iter->second;
              vector<NODE_IDX_T> &v = get<0>(et);
              
              // creates new block if non-contiguous dst indices
              if (((dst > 0) && ((dst-1) > last_idx)) || (num_block_edges > block_size))
                {
                  dst_blk_idx.push_back(dst - dst_start);
                  dst_blk_ptr.push_back(dst_ptr.size());
                  num_blocks++;
                  num_block_edges = 0;
                }
              last_idx = dst;
              
              copy(v.begin(), v.end(), back_inserter(src_idx));
              dst_ptr.push_back(num_prj_edges);
              num_prj_edges += v.size();
              num_block_edges += v.size();
            }
        }
      throw_assert(num_edges == src_idx.size(),
                   "write_projection: mismatch between number of edges and size of source index array");

      size_t sum_num_edges = 0;
      MPI_CHECK_SUCCESS(MPI_Allreduce(&num_edges, &sum_num_edges, 1,
                                      MPI_SIZE_T, MPI_SUM, comm),
                        std::runtime_error, "append_projection: error in MPI_Allreduce");

      if (sum_num_edges == 0)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_free(&comm), std::runtime_error,
                                "append_projection: unable to free MPI communicator");
          return;
        }


      // exchange allocation data
      vector<size_t> sendbuf_num_blocks(size, num_blocks);
      vector<size_t> recvbuf_num_blocks(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_blocks[0], 1, MPI_SIZE_T,
                                      &recvbuf_num_blocks[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "append_projection: error in MPI_Allgather");

      vector<size_t> sendbuf_num_dest(size, num_dest);
      vector<size_t> recvbuf_num_dest(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_dest[0], 1, MPI_SIZE_T,
                                      &recvbuf_num_dest[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "append_projection: error in MPI_Allgather");

      vector<size_t> sendbuf_num_edge(size, num_edges);
      vector<size_t> recvbuf_num_edge(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_edge[0], 1, MPI_SIZE_T,
                                      &recvbuf_num_edge[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "append_projection: error in MPI_Allgather");

      // determine last rank that has data
      size_t last_rank = size-1;

      for (size_t r=last_rank; r >= 0; r--)
	{
	  if (recvbuf_num_blocks[r] > 0)
	    {
	      last_rank = r;
	      break;
	    }
	}

      if (num_blocks > 0)
        {
          for (size_t p = 0; p < rank; ++p)
            {
              dst_blk_ptr[0] += recvbuf_num_dest[p];
            }
          for (size_t i = 1; i < dst_blk_ptr.size(); i++)
            {
              dst_blk_ptr[i] += dst_blk_ptr[0];
            }
          if (rank == last_rank) // last rank writes the total destination count
            {
              dst_blk_ptr.push_back(dst_blk_ptr[0] + recvbuf_num_dest[rank] + 1);
            }
        }

      if (rank == last_rank) // only the last rank writes an additional element
        {
          dst_ptr.push_back(num_prj_edges);
        }

      size_t s = 0;
      for (size_t p = 0; p < rank; ++p)
        {
          s += recvbuf_num_edge[p];
        }

      for (size_t idst = 0; idst < dst_ptr.size(); ++idst)
        {
          dst_ptr[idst] += s;
        }
      
      size_t total_num_blocks=0;
      for (size_t p=0; p<size; p++)
        {
          total_num_blocks = total_num_blocks + recvbuf_num_blocks[p];
        }

      size_t total_num_dests=0;
      for (size_t p=0; p<size; p++)
        {
          total_num_dests = total_num_dests + recvbuf_num_dest[p];
        }

      size_t total_num_edges=0;
      for (size_t p=0; p<size; p++)
        {
          total_num_edges = total_num_edges + recvbuf_num_edge[p];
        }

      mpi::MPI_DEBUG(comm, "append_projection: ", src_pop_name, " -> ", dst_pop_name, ": ",
                     " total_num_dests = ", total_num_dests);


      {
        // Append to destination block index
        string dst_blk_idx_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                             lowio::EDGES, lowio::DST_BLK_IDX);

        size_t start = 0;
        size_t dims = total_num_blocks;
        size_t newsize = start + dims;
      
        size_t block = num_blocks;
        size_t start = dst_blk_idx_start;
        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_blocks[p];
          }

        lowio::write<NODE_IDX_T>(fd, path, newsize, start, block, dst_blk_idx,
                                 chunk_size, args...);
      }

      
      {
        // Append to destination block pointer
        // # dest. block pointers = number of destination blocks + 1
        
        string dst_blk_ptr_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                             lowio::EDGES, lowio::DST_BLK_PTR);
      
        size_t start = 0;
        size_t dims = total_num_blocks + 1;
        size_t newsize = start + dims;
        size_t block = dst_blk_ptr.size();

        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_blocks[p];
          }

        lowio::write<DST_BLK_PTR_T>(fd, path, newsize, start, block, dst_blk_ptr,
                                    chunk_size, args...);
      }
      
      {
        // Append to destination pointer
        // # dest. pointers = number of destinations + 1

        
        string dst_ptr_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                         lowio::EDGES, lowio::DST_PTR);

      
        size_t start = 0;
        size_t dims = total_num_dests+1;
        size_t newsize = start + dims;
        size_t block = dst_ptr.size();

        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_dest[p];
          }

        lowio::write<DST_PTR_T>(fd, path, newsize, start, block, dst_ptr,
                                chunk_size, args...);
        
      }


      {
        // Append to source index
        // # source indexes = number of edges

        string src_idx_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                         lowio::EDGES, lowio::SRC_IDX);

        size_t start = 0;
        size_t dims = total_num_edges;
        size_t newsize = start + dims;
      
        size_t block = src_idx.size();
        size_t start = src_idx_start;
        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_edge[p];
          }

        lowio::write<NODE_IDX_T>(fd, path, newsize, start, block, src_idx,
                                 chunk_size, args...);
        
      }

      vector <string> edge_attr_name_spaces;
      map <string, data::NamedAttrVal> edge_attr_map;
      
      for (auto const& iter : edge_attr_index)
        {
          const string & attr_namespace = iter.first;
          const data::AttrIndex& attr_index  = iter.second.second;
          
          data::NamedAttrVal& edge_attr_values = edge_attr_map[attr_namespace];
          
          edge_attr_values.float_values.resize(attr_index.size_attr_index<float>());
          edge_attr_values.uint8_values.resize(attr_index.size_attr_index<uint8_t>());
          edge_attr_values.uint16_values.resize(attr_index.size_attr_index<uint16_t>());
          edge_attr_values.uint32_values.resize(attr_index.size_attr_index<uint32_t>());
          edge_attr_values.int8_values.resize(attr_index.size_attr_index<int8_t>());
          edge_attr_values.int16_values.resize(attr_index.size_attr_index<int16_t>());
          edge_attr_values.int32_values.resize(attr_index.size_attr_index<int32_t>());

          edge_attr_name_spaces.push_back(attr_namespace);
        }
             
      for (auto const& iter : prj_edge_map)
        {
          const edge_tuple_t& et = iter.second;
          //const vector<NODE_IDX_T>& v = get<0>(et);
          const vector<data::AttrVal>& a = get<1>(et);
          size_t ni=0;
          for (auto const& attr_values : a)
            {
              const string & attr_namespace = edge_attr_name_spaces[ni];
              edge_attr_map[attr_namespace].append(attr_values);
              ni++;
            }
        }
      
      write_edge_attribute_map<float>(fd, src_pop_name, dst_pop_name,
                                       edge_attr_map, edge_attr_index, args...);
      write_edge_attribute_map<uint8_t>(fd, src_pop_name, dst_pop_name,
                                         edge_attr_map, edge_attr_index, args...);
      write_edge_attribute_map<uint16_t>(fd, src_pop_name, dst_pop_name,
                                          edge_attr_map, edge_attr_idnex, args...);
      write_edge_attribute_map<uint32_t>(fd, src_pop_name, dst_pop_name,
                                          edge_attr_map, edge_attr_index, args...);
      write_edge_attribute_map<int8_t>(fd, src_pop_name, dst_pop_name,
                                        edge_attr_map, edge_attr_index, args...);
      write_edge_attribute_map<int16_t>(fd, src_pop_name, dst_pop_name,
                                         edge_attr_map, edge_attr_index, args...);
      write_edge_attribute_map<int32_t>(fd, src_pop_name, dst_pop_name,
                                         edge_attr_map, edge_attr_index, args...);
        
      // clean-up

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm), std::runtime_error,
                        "append_projection: unable to close MPI communicator");
      
    }

    


    template <class args_t ...>
    void append_projection
    (
     h5::fd_t&                 fd,
     const string&             src_pop_name,
     const string&             dst_pop_name,
     const NODE_IDX_T&         src_start,
     const NODE_IDX_T&         src_end,
     const NODE_IDX_T&         dst_start,
     const NODE_IDX_T&         dst_end,
     const size_t&             num_edges,
     const edge_map_t&         prj_edge_map,
     const std::map <std::string, std::pair <size_t, data::AttrIndex > >& edge_attr_index,
     const size_t             block_size,
     const size_t             chunk_size,
     args_t&&... args
     )
    
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = h5::get_mpi_comm(fd);

      unsigned int size=1, rank=0;
      if (comm != MPI_COMM_NULL)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }

      // do a sanity check on the input
      throw_assert(src_start < src_end,
                   "append_projection: invalid source index range");
      throw_assert(dst_start < dst_end,
                   "append_projection: invalid destination index range");
      
      size_t num_dest = prj_edge_map.size();
      size_t num_blocks = num_dest > 0 ? 1 : 0;

      // create relative destination pointers and source index
      vector<DST_BLK_PTR_T> dst_blk_ptr; 
      vector<DST_PTR_T> dst_ptr;
      vector<NODE_IDX_T> dst_blk_idx, src_idx;
      NODE_IDX_T first_idx = 0, last_idx = 0;
      size_t num_block_edges = 0, num_prj_edges = 0;
      if (!prj_edge_map.empty())
        {
          first_idx = (prj_edge_map.begin())->first;
          last_idx  = first_idx;
          dst_blk_idx.push_back(first_idx - dst_start);
          dst_blk_ptr.push_back(0);
          for (auto iter = prj_edge_map.begin(); iter != prj_edge_map.end(); ++iter)
            {
              NODE_IDX_T dst = iter->first;
              edge_tuple_t et = iter->second;
              vector<NODE_IDX_T> &v = get<0>(et);
              
              // creates new block if non-contiguous dst indices
              if (((dst > 0) && ((dst-1) > last_idx)) || (num_block_edges > block_size))
                {
                  dst_blk_idx.push_back(dst - dst_start);
                  dst_blk_ptr.push_back(dst_ptr.size());
                  num_blocks++;
                  num_block_edges = 0;
                }
              last_idx = dst;
              
              copy(v.begin(), v.end(), back_inserter(src_idx));
              dst_ptr.push_back(num_prj_edges);
              num_prj_edges += v.size();
              num_block_edges += v.size();
            }
        }
      throw_assert(num_edges == src_idx.size(),
                   "append_projection: mismatch between number of edges and size of source index array");

      size_t sum_num_edges = 0;
      MPI_CHECK_SUCCESS(MPI_Allreduce(&num_edges, &sum_num_edges, 1,
                                      MPI_SIZE_T, MPI_SUM, comm),
                        std::runtime_error, "append_projection: error in MPI_Allreduce");

      if (sum_num_edges == 0)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_free(&comm), std::runtime_error,
                                "append_projection: unable to free MPI communicator");
          return;
        }


      size_t dst_blk_ptr_size = 0;
      lowio::size_edge_attributes(fd,
                                 src_pop_name,
                                 dst_pop_name,
                                 lowio::EDGES,
                                 lowio::DST_BLK_PTR,
                                 dst_blk_ptr_size);

      size_t dst_blk_idx_size = 0;
      lowio::size_edge_attributes(fd,
                                 src_pop_name,
                                 dst_pop_name,
                                 lowio::EDGES,
                                 lowio::DST_BLK_IDX,
                                 dst_blk_idx_size);

      size_t dst_ptr_size = 0; 
      lowio::size_edge_attributes(fd,
                                 src_pop_name,
                                 dst_pop_name,
                                 lowio::EDGES,
                                 lowio::DST_PTR,
                                 dst_ptr_size);

      size_t src_idx_size = 0;
      lowio::size_edge_attributes(fd,
                                 src_pop_name,
                                 dst_pop_name,
                                 lowio::EDGES,
                                 lowio::SRC_IDX,
                                 src_idx_size);
      
      // exchange allocation data
      vector<size_t> sendbuf_num_blocks(size, num_blocks);
      vector<size_t> recvbuf_num_blocks(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_blocks[0], 1, MPI_SIZE_T,
                                      &recvbuf_num_blocks[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "append_projection: error in MPI_Allgather");

      vector<size_t> sendbuf_num_dest(size, num_dest);
      vector<size_t> recvbuf_num_dest(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_dest[0], 1, MPI_SIZE_T,
                                      &recvbuf_num_dest[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "append_projection: error in MPI_Allgather");

      vector<size_t> sendbuf_num_edge(size, num_edges);
      vector<size_t> recvbuf_num_edge(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_edge[0], 1, MPI_SIZE_T,
                                      &recvbuf_num_edge[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "append_projection: error in MPI_Allgather");

      // determine last rank that has data
      size_t last_rank = size-1;

      for (size_t r=last_rank; r >= 0; r--)
	{
	  if (recvbuf_num_blocks[r] > 0)
	    {
	      last_rank = r;
	      break;
	    }
	}

      if (num_blocks > 0)
        {
          if (dst_ptr_size > 0)
            {
              dst_blk_ptr[0] += dst_ptr_size-1;
            }
          for (size_t p = 0; p < rank; ++p)
            {
              dst_blk_ptr[0] += recvbuf_num_dest[p];
            }
          for (size_t i = 1; i < dst_blk_ptr.size(); i++)
            {
              dst_blk_ptr[i] += dst_blk_ptr[0];
            }
          if (rank == last_rank) // last rank writes the total destination count
            {
              dst_blk_ptr.push_back(dst_blk_ptr[0] + recvbuf_num_dest[rank] + 1);
            }
        }

      if (rank == last_rank) // only the last rank writes an additional element
        {
          dst_ptr.push_back(num_prj_edges);
        }

      size_t s = src_idx_size;
      for (size_t p = 0; p < rank; ++p)
        {
          s += recvbuf_num_edge[p];
        }

      for (size_t idst = 0; idst < dst_ptr.size(); ++idst)
        {
          dst_ptr[idst] += s;
        }
      
      size_t total_num_blocks=0;
      for (size_t p=0; p<size; p++)
        {
          total_num_blocks = total_num_blocks + recvbuf_num_blocks[p];
        }

      size_t total_num_dests=0;
      for (size_t p=0; p<size; p++)
        {
          total_num_dests = total_num_dests + recvbuf_num_dest[p];
        }

      size_t total_num_edges=0;
      for (size_t p=0; p<size; p++)
        {
          total_num_edges = total_num_edges + recvbuf_num_edge[p];
        }

      mpi::MPI_DEBUG(comm, "append_projection: ", src_pop_name, " -> ", dst_pop_name, ": ",
                     " total_num_dests = ", total_num_dests);


      {
        // Append to destination block index
        string dst_blk_idx_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                             lowio::EDGES, lowio::DST_BLK_IDX);

        size_t start = dst_blk_idx_size;
        size_t dims = total_num_blocks;
        size_t newsize = start + dims;
      
        size_t block = num_blocks;
        size_t start = dst_blk_idx_start;
        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_blocks[p];
          }

        lowio::append<NODE_IDX_T>(fd, path, newsize, start, block, dst_blk_idx,
                                  chunk_size, args...);
      }

      
      {
        // Append to destination block pointer
        // # dest. block pointers = number of destination blocks + 1
        
        string dst_blk_ptr_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                             lowio::EDGES, lowio::DST_BLK_PTR);
      
        size_t start = dst_blk_ptr_size;
        if (start > 0)
          start = start - 1;
        
        size_t dims = total_num_blocks + 1;
        size_t newsize = start + dims;
        size_t block = dst_blk_ptr.size();

        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_blocks[p];
          }

        lowio::append<DST_BLK_PTR_T>(fd, path, newsize, start, block, dst_blk_ptr,
                                     chunk_size, args...);
      }
      
      {
        // Append to destination pointer
        // # dest. pointers = number of destinations + 1

        
        string dst_ptr_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                         lowio::EDGES, lowio::DST_PTR);

      
        size_t start = dst_ptr_size;
        if (start > 0)
          start = start - 1;

        size_t dims = total_num_dests+1;
        size_t newsize = start + dims;
        size_t block = dst_ptr.size();

        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_dest[p];
          }

        lowio::append<DST_PTR_T>(fd, path, newsize, start, block, dst_ptr,
                                 chunk_size, args...);
        
      }


      {
        // Append to source index
        // # source indexes = number of edges

        string src_idx_path = lowio::edge_attribute_path(src_pop_name, dst_pop_name,
                                                         lowio::EDGES, lowio::SRC_IDX);

        size_t start = src_idx_size;
        size_t dims = total_num_edges;
        size_t newsize = start + dims;
      
        size_t block = src_idx.size();
        size_t start = src_idx_start;
        for (size_t p = 0; p < rank; ++p)
          {
            start += recvbuf_num_edge[p];
          }

        lowio::append<NODE_IDX_T>(fd, path, newsize, start, block, src_idx,
                                  chunk_size, args...);
        
      }

      vector <string> edge_attr_name_spaces;
      map <string, data::NamedAttrVal> edge_attr_map;

     for (auto const& iter : edge_attr_index)
        {
          const string & attr_namespace = iter.first;
          const data::AttrIndex& attr_index  = iter.second.second;

          data::NamedAttrVal& edge_attr_values = edge_attr_map[attr_namespace];
          
          edge_attr_values.float_values.resize(attr_index.size_attr_index<float>());
          edge_attr_values.uint8_values.resize(attr_index.size_attr_index<uint8_t>());
          edge_attr_values.uint16_values.resize(attr_index.size_attr_index<uint16_t>());
          edge_attr_values.uint32_values.resize(attr_index.size_attr_index<uint32_t>());
          edge_attr_values.int8_values.resize(attr_index.size_attr_index<int8_t>());
          edge_attr_values.int16_values.resize(attr_index.size_attr_index<int16_t>());
          edge_attr_values.int32_values.resize(attr_index.size_attr_index<int32_t>());

          edge_attr_name_spaces.push_back(attr_namespace);
        }
               
      for (auto const& iter : prj_edge_map)
        {
          const edge_tuple_t& et = iter.second;
          //const vector<NODE_IDX_T>& v = get<0>(et);
          const vector<data::AttrVal>& a = get<1>(et);
          size_t ni=0;
          for (auto const& attr_values : a)
            {
              const string & attr_namespace = edge_attr_name_spaces[ni];
              edge_attr_map[attr_namespace].append(attr_values);
              ni++;
            }
        }
      append_edge_attribute_map<float>(fd, src_pop_name, dst_pop_name,
                                       edge_attr_map, edge_attr_index, args...);
      append_edge_attribute_map<uint8_t>(fd, src_pop_name, dst_pop_name,
                                         edge_attr_map, edge_attr_index, args...);
      append_edge_attribute_map<uint16_t>(fd, src_pop_name, dst_pop_name,
                                          edge_attr_map, edge_attr_index, args...);
      append_edge_attribute_map<uint32_t>(fd, src_pop_name, dst_pop_name,
                                          edge_attr_map, edge_attr_index, args...);
      append_edge_attribute_map<int8_t>(fd, src_pop_name, dst_pop_name,
                                        edge_attr_map, edge_attr_index, args...);
      append_edge_attribute_map<int16_t>(fd, src_pop_name, dst_pop_name,
                                         edge_attr_map, edge_attr_index, args...);
      append_edge_attribute_map<int32_t>(fd, src_pop_name, dst_pop_name,
                                         edge_attr_map, edge_attr_index, args...);
        
      // clean-up

      MPI_CHECK_SUCCESS(MPI_Comm_free(&comm), std::runtime_error,
                        "append_projection: unable to close MPI communicator");

    }

    
    template <class args_t ...>
    void append_projection
    (
     const string&             file_name,
     const string&             src_pop_name,
     const string&             dst_pop_name,
     const NODE_IDX_T&         src_start,
     const NODE_IDX_T&         src_end,
     const NODE_IDX_T&         dst_start,
     const NODE_IDX_T&         dst_end,
     const size_t&             num_edges,
     const edge_map_t&         prj_edge_map,
     const std::map <std::string, std::pair <size_t, data::AttrIndex > >& edge_attr_index,
     const size_t             block_size,
     const size_t             chunk_size,
     args_t&&... args
     )
    
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);
      
      h5::fapl_t fapl = h5::fclose_default | h5::fapl_core{4096,1} 
        | h5::core_write_tracking{false,1} 
        | h5::fapl_family{H5F_FAMILY_DEFAULT,0} 
        | h5::fapl_mpio{comm, MPI_INFO_NULL};

      auto fd = h5::open(file_name, H5F_ACC_RDWR, fapl);

      append_projection(fd, src_pop_name, dst_pop_name, src_start, src_end,
                        dst_start, dst_end, num_edges, prj_edge_map, edge_attr_index,
                        block_size, chunk_size, args...);
        
    }
  }
}

#endif
