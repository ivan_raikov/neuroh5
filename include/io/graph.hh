// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file graph.hh
///
///  Top-level functions for reading and writing graphs in DBS
///  (Destination Block Sparse) format.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#ifndef NEUROH5_GRAPH_HH
#define NEUROH5_GRAPH_HH

#include <cstdint>
#include <string>
#include <type_traits>
#include <vector>

#include <mpi.h>
#include "h5cpp/all"

#include "neuroh5_types.hh"
#include "neuroh5_paths.hh"
#include "cell_populations.hh"
#include "projection.hh"
#include "edge_attributes.hh"
#include "serialize_edge.hh"
//#include "graph_datasets.hh"
#include "attr_map.hh"
#include "sort_permutation.hh"
#include "tuple_type_index.hh"
#include "throw_assert.hh"
#include "mpe_seq.hh"
#include "mpi_debug.hh"

using namespace neuroh5::data;

namespace neuroh5
{
  namespace io
  {


    /// @brief Reads the edges of the given projections
    ///
    /// @param comm          MPI communicator
    ///
    /// @param file_name     Input file name
    ///
    /// @param attr_namespaces     Vector of edge attribute namespaces to read
    ///
    /// @param prj_names     Vector of <src, dst> projections to be read
    ///
    /// @param prj_list      Vector of projection tuples, to be filled with
    ///                      edge information by this procedure
    ///
    /// @param total_num_nodes  Updated with the total number of nodes
    ///                         (vertices) in the graph
    ///
    /// @param local_prj_num_edges  Updated with the number of edges in the
    ///                             graph read by the current (local) rank
    ///
    /// @param total_prj_num_edges  Updated with the total number of edges in
    ///                             the graph
    ///
    /// @return
    
    template <class args_t ...>
    void read_graph
    (
     const std::string&               file_name,
     const std::vector< std::string >& edge_attr_name_spaces,
     const std::vector< std::pair<std::string, std::string> >& prj_names,
     std::vector<edge_map_t>&        prj_vector,
     vector < map <string, vector < vector<string> > > > & edge_attr_names_vector,
     size_t&                          total_num_nodes,
     size_t&                          local_prj_num_edges,
     size_t&                          total_prj_num_edges,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      // read the population info
      vector<pop_range_t> pop_vector;
      vector< pair<pop_t, string> > pop_labels;
      map<NODE_IDX_T,pair<uint32_t,pop_t> > pop_ranges;
      set< pair<pop_t, pop_t> > pop_pairs;

      h5::fapl_t fapl = h5::fclose_default | h5::fapl_core{4096,1}
      | h5::fapl_family{H5F_FAMILY_DEFAULT,0}
      | h5::fapl_mpio{comm, MPI_INFO_NULL};
          
      auto fd = h5::open(file_name.c_str(), H5F_ACC_RDONLY);
      
      read_population_combs (fd, pop_pairs, args...);
      read_population_ranges (fd, pop_ranges, pop_vector, total_num_nodes, args...);
      read_population_labels (fd, pop_labels, args...);

      // read the edges
      for (size_t i = 0; i < prj_names.size(); i++)
        {
          size_t local_prj_num_nodes;
          size_t local_prj_num_edges;
          size_t total_prj_num_edges;
          size_t local_read_blocks;
          size_t total_read_blocks;

          string src_pop_name = prj_names[i].first, dst_pop_name = prj_names[i].second;
          uint32_t dst_pop_idx = 0, src_pop_idx = 0;
          bool src_pop_set = false, dst_pop_set = false;
      
          for (size_t i=0; i< pop_labels.size(); i++)
            {
              if (src_pop_name == get<1>(pop_labels[i]))
                {
                  src_pop_idx = get<0>(pop_labels[i]);
                  src_pop_set = true;
                }
              if (dst_pop_name == get<1>(pop_labels[i]))
                {
                  dst_pop_idx = get<0>(pop_labels[i]);
                  dst_pop_set = true;
                }
            }
          assert(dst_pop_set && src_pop_set);
      
          NODE_IDX_T dst_start = pop_vector[dst_pop_idx].start;
          NODE_IDX_T src_start = pop_vector[src_pop_idx].start;

          mpi::MPI_DEBUG(comm, "read_graph: src_pop_name = ", src_pop_name,
                         " dst_pop_name = ", dst_pop_name,
                         " dst_start = ", dst_start,
                         " src_start = ", src_start);

          read_projection (fd, pop_ranges, pop_pairs,
                           src_pop_name, dst_pop_name, 
                           dst_start, src_start, edge_attr_name_spaces, 
                           prj_vector, edge_attr_names_vector,
                           local_prj_num_nodes,
                           local_prj_num_edges, total_prj_num_edges,
                           local_read_blocks, total_read_blocks,
                           args...);
      
          mpi::MPI_DEBUG(comm, "read_graph: projection ", i, " has a total of ", total_prj_num_edges, " edges");
          
          total_num_edges = total_num_edges + total_prj_num_edges;
          local_num_edges = local_num_edges + local_prj_num_edges;
        }

      size_t sum_local_num_edges = 0;
      MPI_CHECK_SUCCESS(MPI_Reduce(&local_num_edges, &sum_local_num_edges, 1,
                                   MPI_SIZE_T, MPI_SUM, 0, MPI_COMM_WORLD),
                        std::runtime_error, "read_graph: error in MPI_Reduce");
      
      if (rank == 0)
        {
          throw_assert(sum_local_num_edges == total_num_edges,
                       "read_graph: " << "sum_local_num_edges = " <<
                       sum_local_num_edges << "total_num_edges = " <<
                       total_num_edges);
        }

    }

        
    /// @brief Reads the edges of the given projections and scatters to all
    ///        ranks
    ///
    /// @param comm          MPI communicator
    ///
    /// @param scatter_type  Enumerate edges by destination (default) or source          
    ///
    /// @param file_name     Input file name
    ///
    /// @param io_size       Number of I/O ranks (those ranks that conduct I/O
    ///                      operations)
    ///
    /// @param attrs_namespaces    Vector of attribute namespaces to read
    ///
    /// @param prj_names     Vector of projection names to be read
    ///
    /// @param prj_vector    Vector of maps where the key is destination index,
    ///                      and the value is a vector of source indices and
    ///                      optional edge attributes, to be filled by this
    ///                      procedure
    ///
    /// @param total_num_nodes  Updated with the total number of nodes
    ///                         (vertices) in the graph
    ///
    /// @return              
    template <class args_t ...>
    void scatter_read_graph
    (
     h5::fd_t& fd,
     const std::vector< std::string >&  attr_namespaces,
     const std::vector< std::pair<std::string,std::string> >&    prj_names,
     // A vector that maps nodes to compute ranks
     const std::map<NODE_IDX_T, rank_t>&  node_rank_map,
     const EdgeMapType                  edge_map_type,
     MPI_Comm                           all_comm,
     const int                          io_size,
     std::vector < edge_map_t >& prj_vector,
     vector < map <string, vector < vector<string> > > > & edge_attr_names_vector,
     size_t &local_num_nodes, size_t &total_num_nodes,
     size_t &local_num_edges, size_t &total_num_edges,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm all_comm = h5::get_mpi_comm(fd);

      unsigned int rank, size;
      MPI_CHECK_SUCCESS(MPI_Comm_size(all_comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(all_comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");

      // The set of compute ranks for which the current I/O rank is responsible
      set< pair<pop_t, pop_t> > pop_pairs;
      vector<pop_range_t> pop_vector;
      map<NODE_IDX_T,pair<uint32_t,pop_t> > pop_ranges;
      vector< pair<pop_t, string> > pop_labels;
      
      read_population_combs (file_name, pop_pairs, all_comm, args...);
      read_population_ranges (file_name, pop_ranges, pop_vector, total_num_nodes, all_comm, args...);
      read_population_labels (file_name, pop_labels, all_comm, args...);
          
      // For each projection, I/O ranks read the edges and scatter
      for (size_t i = 0; i < prj_names.size(); i++)
        {

          string src_pop_name = prj_names[i].first;
          string dst_pop_name = prj_names[i].second;

          scatter_read_projection(fd, src_pop_name, dst_pop_name, attr_namespaces,
                                  node_rank_map, pop_vector, pop_ranges, pop_labels, pop_pairs,
                                  edge_map_type, all_comm, io_size, 
                                  prj_vector, edge_attr_names_vector,
                                  local_num_nodes, local_num_edges, total_num_edges, args...);

        }

      MPI_CHECK_SUCCESS(MPI_Comm_free(&all_comm),
                        std::runtime_error, "bcast_graph: unable to free MPI communicator");

    }


    
    /// @brief Reads the edges of the given projections and broadcasts to all
    ///        ranks
    ///
    /// @param comm          MPI communicator
    ///
    /// @param edge_map_type  Enumerate edges by destination (default) or source          
    ///
    /// @param file_name     Input file name
    ///
    /// @param io_size       Number of I/O ranks (those ranks that conduct I/O
    ///                      operations)
    ///
    /// @param attr_namespaces  Namespaces to read edge attributes from
    ///
    /// @param prj_names     Vector of projection names to be read
    ///
    /// @param prj_vector    Vector of maps where the key is destination index,
    ///                      and the value is a vector of source indices and
    ///                      optional edge attributes, to be filled by this
    ///                      procedure
    ///
    /// @param total_num_nodes  Updated with the total number of nodes
    ///                         (vertices) in the graph
    ///
    /// @return              HDF5 error code
    template <class args_t ...>
    void bcast_graph
    (
     const EdgeMapType                  edge_map_type,
     const std::string&                 file_name,
     const std::vector< std::string > & attr_namespaces,
     const std::vector< std::pair<std::string,std::string> >& prj_names,
     MPI_Comm                           comm,
     const int                          root,
     std::vector < edge_map_t >& prj_vector,
     std::vector < std::map < std::string, std::vector <std::vector <std::string> > > >& edge_attr_names_vector,
     size_t                            &total_num_nodes,
     size_t                            &local_num_edges,
     size_t                            &total_num_edges,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      // The set of compute ranks for which the current I/O rank is responsible
      set< pair<pop_t, pop_t> > pop_pairs;
      vector<pop_range_t> pop_vector;
      map<NODE_IDX_T,pair<uint32_t,pop_t> > pop_ranges;
      size_t prj_size = 0;
      // MPI Communicator for I/O ranks
      MPI_Comm io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1;

      unsigned int rank=0, size=0;
                          
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");
      
      
      // Am I an I/O rank?
      if (rank == root)
        {
          MPI_Comm_split(comm,io_color,rank,&io_comm);
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);
      
          // read the population info
          read_population_combs(file_name, pop_pairs, io_comm, args...);
          read_population_ranges(file_name, pop_ranges, pop_vector, total_num_nodes, io_comm, args...);
          
          prj_size = prj_names.size();
        }
      else
        {
          MPI_Comm_split(all_comm,0,rank,&io_comm);
        }

      MPI_CHECK_SUCCESS(MPI_Bcast(&prj_size, 1, MPI_SIZE_T, 0, comm),
                        std::runtime_error, "bcast_graph: error in MPI_Bcast");

      // For each projection, I/O ranks read the edges and scatter
      for (size_t i = 0; i < prj_size; i++)
        {
          bcast_projection(comm, io_comm, edge_map_type, file_name,
                           prj_names[i].first, prj_names[i].second,
                           attr_namespaces, pop_vector, pop_ranges, pop_pairs,
                           prj_vector, edge_attr_names_vector);
                             
        }
      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm),
                        std::runtime_error, "bcast_graph: unable to free MPI communicator");
    }

    
    /// @brief Writes the edges of the given projection to an output file.
    ///
    /// @param comm          MPI communicator
    ///
    /// @param edge_map_type  Enumerate edges by destination (default) or source          
    ///
    /// @param file_name     Input file name
    ///
    /// @param io_size       Number of I/O ranks (those ranks that conduct I/O
    ///                      operations)
    ///
    /// @param attr_namespaces  Namespaces to read edge attributes from
    ///
    /// @param prj_names     Vector of projection names to be read
    ///
    /// @param prj_vector    Vector of maps where the key is destination index,
    ///                      and the value is a vector of source indices and
    ///                      optional edge attributes, to be filled by this
    ///                      procedure
    ///
    /// @param total_num_nodes  Updated with the total number of nodes
    ///                         (vertices) in the graph
    ///
    /// @return
    template <class args_t ...>
    void write_graph
    (
     const std::string&    file_name,
     const std::string&    src_pop_name,
     const std::string&    dst_pop_name,
     const std::map <std::string, std::pair <size_t, data::AttrIndex > >& edge_attr_index,
     const edge_map_t&  input_edge_map,
     const int io_size,
     args_t&&... args
     )
    {
      size_t io_size_value;
      size_t num_edges = 0;
      
      // read the population info
      set< pair<pop_t, pop_t> > pop_pairs;
      vector<pop_range_t> pop_vector;
      vector<pair <pop_t, string> > pop_labels;
      map<NODE_IDX_T,pair<uint32_t,pop_t> > pop_ranges;
      size_t src_pop_idx, dst_pop_idx; bool src_pop_set=false, dst_pop_set=false;
      size_t total_num_nodes;
      size_t dst_start, dst_end;
      size_t src_start, src_end;

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }

      if (size < io_size)
        {
          io_size_value = size > 0 ? size : 1;
        }
      else
        {
          io_size_value = io_size > 0 ? (size_t)io_size : 1;
        }
      
      set<size_t> io_rank_set;
      data::range_sample(size, io_size_value, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
      
      read_population_ranges(file_name, pop_ranges, pop_vector, total_num_nodes, comm, args...);
      read_population_labels(file_name, pop_labels, comm, args...);
      
      for (size_t i=0; i< pop_labels.size(); i++)
        {
          if (src_pop_name == get<1>(pop_labels[i]))
            {
              src_pop_idx = get<0>(pop_labels[i]);
              src_pop_set = true;
            }
          if (dst_pop_name == get<1>(pop_labels[i]))
            {
              dst_pop_idx = get<0>(pop_labels[i]);
              dst_pop_set = true;
            }
        }
      throw_assert(dst_pop_set && src_pop_set,
                   "write_graph: unable to determine source or target population index");
      
      dst_start = pop_vector[dst_pop_idx].start;
      dst_end   = dst_start + pop_vector[dst_pop_idx].count;
      src_start = pop_vector[src_pop_idx].start;
      src_end   = src_start + pop_vector[src_pop_idx].count;
      
      
      // A vector that maps nodes to compute ranks
      map< NODE_IDX_T, rank_t > node_rank_map;
      {
        rank_t r=0; 
        for (size_t i = 0; i < node_index.size(); i++)
          {
            node_rank_map.insert(make_pair(node_index[i], *std::next(io_rank_set.begin(), r++)));
            if (io_size_value <= r) r=0;
          }
      }

      // construct a map where each set of edges are arranged by destination I/O rank
      auto compare_nodes = [](const NODE_IDX_T& a, const NODE_IDX_T& b) { return (a < b); };
      rank_edge_map_t rank_edge_map;
      for (auto iter : input_edge_map)
        {
          NODE_IDX_T dst = iter.first;
          // all source/destination node IDs must be in range
          assert(dst_start <= dst && dst <= dst_end);
          edge_tuple_t& et        = iter.second;
          vector<NODE_IDX_T>& v   = get<0>(et);
          vector <AttrVal> & va   = get<1>(et);

          vector<NODE_IDX_T> adj_vector;
          for (auto & src: v)
            {
              if (!(src_start <= src && src <= src_end))
                {
                  printf("src = %u src_start = %lu src_end = %lu\n", src, src_start, src_end);
                }
              throw_assert(src_start <= src && src <= src_end,
                           "write_graph: invalid source index");
              adj_vector.push_back(src - src_start);
              num_edges++;
            }
          vector<size_t> p = sort_permutation(adj_vector, compare_nodes);

          apply_permutation_in_place(adj_vector, p);

          for (auto & a : va)
            {
              
              for (size_t i=0; i<a.size_attr_vec<float>(); i++)
                {
                  assert(a.float_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.float_values[i], p);
                }
              for (size_t i=0; i<a.size_attr_vec<uint8_t>(); i++)
                {
                  assert(a.uint8_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.uint8_values[i], p);
                }
              for (size_t i=0; i<a.size_attr_vec<uint16_t>(); i++)
                {
                  assert(a.uint16_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.uint16_values[i], p);
                }
              for (size_t i=0; i<a.size_attr_vec<uint32_t>(); i++)
                {
                  assert(a.uint32_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.uint32_values[i], p);
                }
              for (size_t i=0; i<a.size_attr_vec<int8_t>(); i++)
                {
                  assert(a.int8_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.int8_values[i], p);
                }
              for (size_t i=0; i<a.size_attr_vec<int16_t>(); i++)
                {
                  assert(a.int16_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.int16_values[i], p);
                }
              for (size_t i=0; i<a.size_attr_vec<int32_t>(); i++)
                {
                  assert(a.int32_values[i].size() == adj_vector.size());
                  apply_permutation_in_place(a.int32_values[i], p);
                }
            }
          
          auto it = node_rank_map.find(dst);
          throw_assert(it != node_rank_map.end(),
                       "write_graph: unable to find destination index in node rank map");
          size_t dst_rank = it->second;
          edge_tuple_t& et1 = rank_edge_map[dst_rank][dst];
          vector<NODE_IDX_T> &src_vec = get<0>(et1);
          src_vec.insert(src_vec.end(),adj_vector.begin(),adj_vector.end());
          vector <AttrVal> &edge_attr_vec = get<1>(et1);
          edge_attr_vec.resize(va.size());
          
          size_t i=0;
          for (auto & edge_attr : edge_attr_vec)
            {
              AttrVal& a = va[i];

              edge_attr.float_values.resize(a.size_attr_vec<float>());
              edge_attr.uint8_values.resize(a.size_attr_vec<uint8_t>());
              edge_attr.uint16_values.resize(a.size_attr_vec<uint16_t>());
              edge_attr.uint32_values.resize(a.size_attr_vec<uint32_t>());
              edge_attr.int8_values.resize(a.size_attr_vec<int8_t>());
              edge_attr.int16_values.resize(a.size_attr_vec<int16_t>());
              edge_attr.int32_values.resize(a.size_attr_vec<int32_t>());
              edge_attr.append(a);
              i++;
            }
        }


      // send buffer and structures for MPI Alltoall operation
      vector<char> sendbuf;
      vector<int> sendcounts(size,0), sdispls(size,0), recvcounts(size,0), rdispls(size,0);

      // Create serialized object with the edges of vertices for the respective I/O rank
      size_t num_packed_edges = 0; 

      data::serialize_rank_edge_map (size, rank, rank_edge_map, num_packed_edges,
                                     sendcounts, sendbuf, sdispls);
      rank_edge_map.clear();
      
      // 1. Each ALL_COMM rank sends an edge vector size to
      //    every other ALL_COMM rank (non IO_COMM ranks receive zero),
      //    and creates sendcounts and sdispls arrays
      
      MPI_CHECK_SUCCESS(MPI_Alltoall(&sendcounts[0], 1, MPI_INT, &recvcounts[0], 1, MPI_INT, all_comm),
                        std::runtime_error, "write_graph: error in MPI_Alltoall");
      
      // 2. Each ALL_COMM rank accumulates the vector sizes and allocates
      //    a receive buffer, recvcounts, and rdispls
      
      size_t recvbuf_size = recvcounts[0];
      for (int p = 1; p < size; p++)
        {
          rdispls[p] = rdispls[p-1] + recvcounts[p-1];
          recvbuf_size += recvcounts[p];
        }

      vector<char> recvbuf;
      recvbuf.resize(recvbuf_size > 0 ? recvbuf_size : 1, 0);
      
      // 3. Each ALL_COMM rank participates in the MPI_Alltoallv
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&sendbuf[0], &sendcounts[0], &sdispls[0], MPI_CHAR,
                                      &recvbuf[0], &recvcounts[0], &rdispls[0], MPI_CHAR,
                                      all_comm),
                        std::runtime_error, "write_graph: error in MPI_Alltoallv");
      
      sendbuf.clear();
      sendcounts.clear();
      sdispls.clear();

      size_t num_unpacked_edges = 0, num_unpacked_nodes = 0;
      edge_map_t prj_edge_map;
      if (recvbuf_size > 0)
        {
          data::deserialize_rank_edge_map (size, recvbuf, recvcounts, rdispls, 
                                           prj_edge_map, num_unpacked_nodes,
                                           num_unpacked_edges);
        }

      // Create an I/O communicator
      MPI_Comm  io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1;
      if (is_io_rank)
        {
          MPI_Comm_split(all_comm,io_color,rank,&io_comm);
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);
        }
      else
        {
          MPI_Comm_split(all_comm,0,rank,&io_comm);
        }

      if (is_io_rank)
        {
          h5::fapl_t fapl = h5::fclose_default
            | h5::fapl_core{4096,1}
          | h5::core_write_tracking{false,1}
          | h5::fapl_family{H5F_FAMILY_DEFAULT,0}
          | h5::fapl_mpio{io_comm, MPI_INFO_NULL};
          
          auto fd = h5::open(file_name.c_str(), H5F_ACC_RDWR);
          lowio::create_projection_groups(fd, src_pop_name, dst_pop_name);
          
          write_projection (fd, src_pop_name, dst_pop_name,
                            src_start, src_end, dst_start, dst_end,
                            num_unpacked_edges, prj_edge_map, edge_attr_index,
                            args...);
          
        }

      MPI_CHECK_SUCCESS(MPI_Barrier(all_comm), std::runtime_error,
                        "write_graph: error in MPI barrier");

      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm),
                        std::runtime_error,
                        "write_graph: unable to free MPI communicator");

    }
    

    
    /// @brief Appends the edges of the given projection to an output file.
    ///
    /// @param comm          MPI communicator
    ///
    /// @param edge_map_type  Enumerate edges by destination (default) or source          
    ///
    /// @param file_name     Input file name
    ///
    /// @param io_size       Number of I/O ranks (those ranks that conduct I/O
    ///                      operations)
    ///
    /// @param attr_namespaces  Namespaces to read edge attributes from
    ///
    /// @param prj_names     Vector of projection names to be read
    ///
    /// @param prj_vector    Vector of maps where the key is destination index,
    ///                      and the value is a vector of source indices and
    ///                      optional edge attributes, to be filled by this
    ///                      procedure
    ///
    /// @param total_num_nodes  Updated with the total number of nodes
    ///                         (vertices) in the graph
    ///
    /// @return
    template <class args_t ...>
    void append_graph
    (
     const string&    file_name,
     const string&    src_pop_name,
     const string&    dst_pop_name,
     const std::map <std::string, std::pair <size_t, data::AttrIndex > >& edge_attr_index,
     const edge_map_t&  input_edge_map,
     const int        io_size,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      size_t io_size_value;
      size_t num_edges = 0;
      
      // read the population info
      set< pair<pop_t, pop_t> > pop_pairs;
      vector<pop_range_t> pop_vector;
      vector<pair <pop_t, string> > pop_labels;
      map<NODE_IDX_T,pair<uint32_t,pop_t> > pop_ranges;
      size_t src_pop_idx, dst_pop_idx; bool src_pop_set=false, dst_pop_set=false;
      size_t total_num_nodes=0, pop_num_nodes=0;
      size_t dst_start, dst_end;
      size_t src_start, src_end;

      auto compare_nodes = [](const NODE_IDX_T& a, const NODE_IDX_T& b) { return (a < b); };

      MPI_Comm all_comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(all_comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(all_comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }

      if (size < io_size)
        {
          io_size_value = size > 0 ? size : 1;
        }
      else
        {
          io_size_value = io_size > 0 ? (size_t)io_size : 1;
        }

      set<size_t> io_rank_set;
      data::range_sample(size, io_size_value, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
                              
      read_population_ranges(file_name, pop_ranges, pop_vector, pop_num_nodes, args...);
      read_population_labels(file_name, pop_labels, args...);
      
      for (size_t i=0; i< pop_labels.size(); i++)
        {
          if (src_pop_name == get<1>(pop_labels[i]))
            {
              src_pop_idx = get<0>(pop_labels[i]);
              src_pop_set = true;
            }
          if (dst_pop_name == get<1>(pop_labels[i]))
            {
              dst_pop_idx = get<0>(pop_labels[i]);
              dst_pop_set = true;
            }
        }
      throw_assert(dst_pop_set && src_pop_set,
                   "append_graph: unable to determine source or target population index");
      
      dst_start = pop_vector[dst_pop_idx].start;
      dst_end   = dst_start + pop_vector[dst_pop_idx].count;
      src_start = pop_vector[src_pop_idx].start;
      src_end   = src_start + pop_vector[src_pop_idx].count;
      
      vector< NODE_IDX_T > node_index;

      { // Determine the destination node indices present in the input
        // edge map across all ranks
        size_t num_nodes = input_edge_map.size();
        vector<size_t> sendbuf_num_nodes(size, num_nodes);
        vector<size_t> recvbuf_num_nodes(size);
        vector<int> recvcounts(size, 0);
        vector<int> displs(size+1, 0);
        MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_nodes[0], 1, MPI_SIZE_T,
                                        &recvbuf_num_nodes[0], 1, MPI_SIZE_T, all_comm),
                          std::runtime_error,
                          "append_graph: error in MPI_Allgather");
                     
        for (size_t p=0; p<size; p++)
          {
            total_num_nodes = total_num_nodes + recvbuf_num_nodes[p];
            displs[p+1] = displs[p] + recvbuf_num_nodes[p];
            recvcounts[p] = recvbuf_num_nodes[p];
          }
        
        vector< NODE_IDX_T > local_node_index;
        for (auto iter: input_edge_map)
          {
            NODE_IDX_T dst          = iter.first;
            local_node_index.push_back(dst);
          }
        
        node_index.resize(total_num_nodes,0);
        MPI_CHECK_SUCCESS(MPI_Allgatherv(&local_node_index[0], num_nodes, MPI_NODE_IDX_T,
                                         &node_index[0], &recvcounts[0], &displs[0], MPI_NODE_IDX_T,
                                         all_comm),
                          std::runtime_error, "append_graph: error in MPI_Allgatherv");

        vector<size_t> p = sort_permutation(node_index, compare_nodes);
        apply_permutation_in_place(node_index, p);
      }

      throw_assert(node_index.size() == total_num_nodes,
                   "append_graph: mismatch between node index size and total number of nodes");
                   
      // A vector that maps nodes to compute ranks
      map< NODE_IDX_T, rank_t > node_rank_map;
      {
        rank_t r=0; 
        for (size_t i = 0; i < node_index.size(); i++)
          {
            node_rank_map.insert(make_pair(node_index[i], *std::next(io_rank_set.begin(), r++)));
            if (io_size_value <= r) r=0;
          }
      }
      rank_edge_map_t rank_edge_map;
      mpi::MPI_DEBUG(all_comm, "append_graph: ", src_pop_name, " -> ", dst_pop_name, ": ",
                     " total_num_nodes = ", total_num_nodes);

      // construct a map where each set of edges are arranged by destination I/O rank
      for (auto iter: input_edge_map)
        {
          NODE_IDX_T dst          = iter.first;
          // all source/destination node IDs must be in range
          throw_assert(dst_start <= dst && dst < dst_end,
                       "append_graph: invalid destination index");
          edge_tuple_t& et        = iter.second;
          const vector<NODE_IDX_T>& v   = get<0>(et);
          vector <AttrVal>& va    = get<1>(et);

          auto it = node_rank_map.find(dst);
          throw_assert(it != node_rank_map.end(),
                       "append_graph: unable to find destination index in node rank map");
          size_t dst_rank = it->second;
          edge_tuple_t& et1 = rank_edge_map[dst_rank][dst];

          if (v.size() > 0)
            {
              vector<NODE_IDX_T> adj_vector;
              for (const auto & src: v)
                {
                  if (!(src_start <= src && src <= src_end))
                    {
                      printf("src = %u src_start = %lu src_end = %lu\n", src, src_start, src_end);
                    }
                  throw_assert(src_start <= src && src <= src_end,
                               "append_graph: invalid source index");
                  adj_vector.push_back(src - src_start);
                  num_edges++;
                }
            
              vector<size_t> p = sort_permutation(adj_vector, compare_nodes);
              
              apply_permutation_in_place(adj_vector, p);
              
              for (auto & a : va)
                {
                  for (size_t i=0; i<a.float_values.size(); i++)
                    {
                      apply_permutation_in_place(a.float_values[i], p);
                    }
                  for (size_t i=0; i<a.uint8_values.size(); i++)
                    {
                      apply_permutation_in_place(a.uint8_values[i], p);
                    }
                  for (size_t i=0; i<a.uint16_values.size(); i++)
                    {
                      apply_permutation_in_place(a.uint16_values[i], p);
                    }
                  for (size_t i=0; i<a.uint32_values.size(); i++)
                    {
                      apply_permutation_in_place(a.uint32_values[i], p);
                    }
                  for (size_t i=0; i<a.int8_values.size(); i++)
                    {
                      apply_permutation_in_place(a.int8_values[i], p);
                    }
                  for (size_t i=0; i<a.int16_values.size(); i++)
                    {
                      apply_permutation_in_place(a.int16_values[i], p);
                    }
                  for (size_t i=0; i<a.int32_values.size(); i++)
                    {
                      apply_permutation_in_place(a.int32_values[i], p);
                    }
                }

              vector<NODE_IDX_T> &src_vec = get<0>(et1);
              src_vec.insert(src_vec.end(),adj_vector.begin(),adj_vector.end());
              vector <AttrVal> &edge_attr_vec = get<1>(et1);
              edge_attr_vec.resize(va.size());
              
              size_t i=0;
              for (auto & edge_attr : edge_attr_vec)
                {
                  AttrVal& a = va[i];
                  edge_attr.float_values.resize(a.float_values.size());
                  edge_attr.uint8_values.resize(a.uint8_values.size());
                  edge_attr.uint16_values.resize(a.uint16_values.size());
                  edge_attr.uint32_values.resize(a.uint32_values.size());
                  edge_attr.int8_values.resize(a.int8_values.size());
                  edge_attr.int16_values.resize(a.int16_values.size());
                  edge_attr.int32_values.resize(a.int32_values.size());
                  edge_attr.append(a);
                  i++;
                }
            }
        }


      // send buffer and structures for MPI Alltoall operation
      vector<char> sendbuf;
      vector<int> sendcounts(size,0), sdispls(size,0), recvcounts(size,0), rdispls(size,0);

      // Create serialized object with the edges of vertices for the respective I/O rank
      size_t num_packed_edges = 0; 


      data::serialize_rank_edge_map (size, rank, rank_edge_map, num_packed_edges,
                                     sendcounts, sendbuf, sdispls);
      rank_edge_map.clear();
      
      // 1. Each ALL_COMM rank sends an edge vector size to
      //    every other ALL_COMM rank (non IO_COMM ranks receive zero),
      //    and creates sendcounts and sdispls arrays
      
      MPI_CHECK_SUCCESS(MPI_Alltoall(&sendcounts[0], 1, MPI_INT, &recvcounts[0], 1, MPI_INT, all_comm),
                        std::runtime_error, "append_graph: error in MPI_Alltoall");
      
      // 2. Each ALL_COMM rank accumulates the vector sizes and allocates
      //    a receive buffer, recvcounts, and rdispls
      
      size_t recvbuf_size = recvcounts[0];
      for (int p = 1; p < size; p++)
        {
          rdispls[p] = rdispls[p-1] + recvcounts[p-1];
          recvbuf_size += recvcounts[p];
        }

      vector<char> recvbuf;
      recvbuf.resize(recvbuf_size > 0 ? recvbuf_size : 1, 0);
      
      // 3. Each ALL_COMM rank participates in the MPI_Alltoallv
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&sendbuf[0], &sendcounts[0], &sdispls[0], MPI_CHAR,
                                      &recvbuf[0], &recvcounts[0], &rdispls[0], MPI_CHAR,
                                      all_comm),
                        std::runtime_error,  "append_graph: error in MPI_Alltoallv");
      sendbuf.clear();
      sendcounts.clear();
      sdispls.clear();

      size_t num_unpacked_edges = 0, num_unpacked_nodes = 0;
      edge_map_t prj_edge_map;
      if (recvbuf_size > 0)
        {
          data::deserialize_rank_edge_map (size, recvbuf, recvcounts, rdispls, 
                                           prj_edge_map, num_unpacked_nodes, num_unpacked_edges);
        }

      // Create an I/O communicator
      MPI_Comm  io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1;
      if (is_io_rank)
        {
          MPI_Comm_split(all_comm,io_color,rank,&io_comm);
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);
        }
      else
        {
          MPI_Comm_split(all_comm,0,rank,&io_comm);
        }

      if (is_io_rank)
        {
          h5::fapl_t fapl = h5::fclose_default
            | h5::fapl_core{4096,1}
          | h5::core_write_tracking{false,1}
          | h5::fapl_family{H5F_FAMILY_DEFAULT,0}
          | h5::fapl_mpio{io_comm, MPI_INFO_NULL};
          
          auto fd = h5::open(file_name.c_str(), H5F_ACC_RDWR);
          lowio::create_projection_groups(fd, src_pop_name, dst_pop_name);

          append_projection (fd, src_pop_name, dst_pop_name,
                             src_start, src_end, dst_start, dst_end,
                             num_unpacked_edges, prj_edge_map,
                             edge_attr_index, io_comm);

        }
      MPI_CHECK_SUCCESS(MPI_Barrier(all_comm), std::runtime_error,
                        "append_graph: error in MPI barrier");
      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm), std::runtime_error, 
                   "append_graph: unable to free MPI communicator");

    }
  

    
  }
}

#endif
