// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file cell_trees.hh
///
///  Routines for reading and writing cell tree morphologies.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================

#ifndef CELL_TREES_HH
#define CELL_TREES_HH

#include <mpi.h>
#include <vector>
#include <map>

#include "neuroh5_types.hh"
#include "cell_index.hh"
#include "attr_map.hh"

namespace neuroh5
{
  namespace io
  {

    template <>
    void append_tree_list (const CELL_IDX_T pop_start,
                           data::NamedAttrMap&       attr_values,
                           std::vector<neurotree_t>& tree_list)
    {
      
      for (CELL_IDX_T idx : attr_values.index_set)
        {
          const vector<SECTION_IDX_T>& src_vector = attr_values.find_name<SECTION_IDX_T>(path::SRCSEC, idx);
          const vector<SECTION_IDX_T>& dst_vector = attr_values.find_name<SECTION_IDX_T>(path::DSTSEC, idx);

          const vector<SECTION_IDX_T>& sections = attr_values.find_name<SECTION_IDX_T>(path::SECTION, idx);
        
          const vector<COORD_T>& xcoords =  attr_values.find_name<COORD_T>(path::X_COORD, idx);
          const vector<COORD_T>& ycoords =  attr_values.find_name<COORD_T>(path::Y_COORD, idx);
          const vector<COORD_T>& zcoords =  attr_values.find_name<COORD_T>(path::Z_COORD, idx);

          const vector<REALVAL_T>& radiuses    =  attr_values.find_name<REALVAL_T>(path::RADIUS, idx);
          const vector<LAYER_IDX_T>& layers    =  attr_values.find_name<LAYER_IDX_T>(path::LAYER, idx);
          const vector<PARENT_NODE_IDX_T>& parents = attr_values.find_name<PARENT_NODE_IDX_T>(path::PARENT, idx);
          const vector<SWC_TYPE_T> swc_types   = attr_values.find_name<SWC_TYPE_T>(path::SWCTYPE, idx);

          CELL_IDX_T gid = idx;
          tree_list.push_back(make_tuple(gid,
                                         src_vector, dst_vector, sections,
                                         xcoords,ycoords,zcoords,
                                         radiuses,layers,parents,
                                         swc_types));
        }
    }


    
    template <class args_t ...>
    int build_tree_datasets
    (
     std::vector<neurotree_t> &tree_list,
    
     std::vector<CELL_IDX_T>& all_index_vector,
     std::vector<SECTION_IDX_T>& all_src_vector,
     std::vector<SECTION_IDX_T>& all_dst_vector,
     std::vector<COORD_T>& all_xcoords,
     std::vector<COORD_T>& all_ycoords,
     std::vector<COORD_T>& all_zcoords,  // coordinates of nodes
     std::vector<REALVAL_T>& all_radiuses,    // Radius
     std::vector<LAYER_IDX_T>& all_layers,        // Layer
     std::vector<SECTION_IDX_T>& all_sections,    // Section
     std::vector<PARENT_NODE_IDX_T>& all_parents, // Parent
     std::vector<SWC_TYPE_T>& all_swc_types, // SWC Types
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }

      std::vector<SEC_PTR_T> dflt_sec_ptr;
      std::vector<TOPO_PTR_T> dflt_topo_ptr;
      std::vector<ATTR_PTR_T> dflt_attr_ptr;
      
      std::vector<SEC_PTR_T>& sec_ptr = get<std::vector<SEC_PTR_T>& >(args_tuple, dflt_sec_ptr);
      std::vector<TOPO_PTR_T>& topo_ptr = get<std::vector<TOPO_PTR_T>& >(args_tuple, dflt_topo_ptr);
      std::vector<ATTR_PTR_T>& attr_ptr = get<std::vector<ATTR_PTR_T>& >(args_tuple, dflt_attr_ptr);
      
      size_t all_attr_size=0, all_sec_size=0,  all_topo_size=0;
      std::vector<size_t> attr_size_vector, sec_size_vector, topo_size_vector;

      attr_ptr.push_back(0);
      sec_ptr.push_back(0);
      topo_ptr.push_back(0);

      hsize_t local_ptr_size = tree_list.size();
      if (rank == size-1)
        {
          local_ptr_size=local_ptr_size+1;
        }

      std::vector<size_t> ptr_size_vector;
      if ( has_type<MPI_Comm, args_t...>::value )
        {
          ptr_size_vector.resize(size);
          MPI_CHECK_SUCCESS(MPI_Allgather(&local_ptr_size, 1, MPI_SIZE_T, &ptr_size_vector[0], 1, MPI_SIZE_T, comm),
                            std::runtime_error, "error in MPI_Allgather");
        }

      size_t block  = tree_list.size();

      for (size_t i = 0; i < block; i++)
        {
          neurotree_t &tree = tree_list[i];

          size_t attr_size=0, sec_size=0, topo_size=0;

          const CELL_IDX_T &idx = get<0>(tree);
          const std::vector<SECTION_IDX_T> & src_vector=get<1>(tree);
          const std::vector<SECTION_IDX_T> & dst_vector=get<2>(tree);
          const std::vector<SECTION_IDX_T> & sections=get<3>(tree);
          const std::vector<COORD_T> & xcoords=get<4>(tree);
          const std::vector<COORD_T> & ycoords=get<5>(tree);
          const std::vector<COORD_T> & zcoords=get<6>(tree);
          const std::vector<REALVAL_T> & radiuses=get<7>(tree);
          const std::vector<LAYER_IDX_T> & layers=get<8>(tree);
          const std::vector<PARENT_NODE_IDX_T> & parents=get<9>(tree);
          const std::vector<SWC_TYPE_T> & swc_types=get<10>(tree);

          
          topo_size = src_vector.size();
          throw_assert((src_vector.size() == topo_size) &&
                       (dst_vector.size() == topo_size),
                       "invalid topology vector sizes");

          topo_ptr.push_back(topo_size+topo_ptr.back());
        
          attr_size = xcoords.size();
          throw_assert((xcoords.size()  == attr_size) &&
                       (ycoords.size()  == attr_size) &&
                       (zcoords.size()  == attr_size) &&
                       (radiuses.size() == attr_size) &&
                       (layers.size()   == attr_size) &&
                       (parents.size()  == attr_size) &&
                       (swc_types.size()  == attr_size),
                       "invalid SWC data sizes");

          attr_ptr.push_back(attr_size+attr_ptr.back());

          sec_size = sections.size();
          sec_ptr.push_back(sec_size+sec_ptr.back());

          all_index_vector.push_back(idx);
          all_src_vector.insert(all_src_vector.end(),src_vector.begin(),src_vector.end());
          all_dst_vector.insert(all_dst_vector.end(),dst_vector.begin(),dst_vector.end());
          all_sections.insert(all_sections.end(),sections.begin(),sections.end());
          all_xcoords.insert(all_xcoords.end(),xcoords.begin(),xcoords.end());
          all_ycoords.insert(all_ycoords.end(),ycoords.begin(),ycoords.end());
          all_zcoords.insert(all_zcoords.end(),zcoords.begin(),zcoords.end());
          all_radiuses.insert(all_radiuses.end(),radiuses.begin(),radiuses.end());
          all_layers.insert(all_layers.end(),layers.begin(),layers.end());
          all_parents.insert(all_parents.end(),parents.begin(),parents.end());
          all_swc_types.insert(all_swc_types.end(),swc_types.begin(),swc_types.end());

          all_attr_size = all_attr_size + attr_size;
          all_sec_size  = all_sec_size + sec_size;
          all_topo_size = all_topo_size + topo_size;

        }

    }


    template <class args_t ...>
    void read_trees
    (
     const std::string& file_name,
     const std::string& pop_name,
     const CELL_IDX_T& pop_start,
     std::vector<neurotree_t> &tree_list,
     args_t&&... args
     )
    {

      auto args_tuple = std::forward_as_tuple(args...);

      data::NamedAttrMap attr_values;
      
      read_cell_attributes (file_name, path::TREES,
                            pop_name, pop_start, attr_values,
                            args...);

      append_tree_list (pop_start, attr_values, tree_list);
    }

    template <class args_t ...>
    void scatter_read_trees
    (
     MPI_Comm                         all_comm,
     const std::string               &file_name,
     const int                        io_size,
     const std::vector<std::string>  &attr_name_spaces,
     // A vector that maps nodes to compute ranks
     const map<CELL_IDX_T, rank_t>   &node_rank_map,
     const string                    &pop_name,
     const CELL_IDX_T                 pop_start,
     std::map<CELL_IDX_T, neurotree_t>    &tree_map,
     std::map<std::string, data::NamedAttrMap> &attr_maps,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<const h5::offset_t&>(args_tuple, h5::default_offset);

      vector<char> sendbuf; 
      vector<int> sendcounts, sdispls;

      unsigned int rank, size;
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");
    
      // MPI Communicator for I/O ranks
      MPI_Comm io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1;

      throw_assert(io_size > 0,
                   "io_size must be a positive integer");
      
      set<size_t> io_rank_set;
      data::range_sample(size, io_size, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
      
      // Am I an I/O rank?
      if (is_io_rank)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_split(all_comm,io_color,rank,&io_comm),
                            std::runtime_error, "error in MPI_Comm_split");
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);

          map <rank_t, map<CELL_IDX_T, neurotree_t> > rank_tree_map;
          {
            data::NamedAttrMap attr_values;

            read_cell_attributes (file_name, path::TREES,
                                  pop_name, pop_start, attr_values,
                                  io_comm, args...);

            
            data::append_rank_tree_map(attr_values, node_rank_map, rank_tree_map);
          }

          data::serialize_rank_tree_map (size, rank, rank_tree_map, sendcounts, sendbuf, sdispls);
        }
      else
        {
          MPI_Comm_split(all_comm,0,rank,&io_comm);
        }
      MPI_Barrier(all_comm);

      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm),
                        std::runtime_error, "error in MPI_Comm_free");
    
      sendcounts.resize(size,0);
      sdispls.resize(size,0);

      vector<int> recvcounts, rdispls;
      vector<char> recvbuf;


      MPI_CHECK_SUCCESS(mpi::alltoallv<char>(all_comm, MPI_CHAR, sendcounts, sdispls, *(std::cbegin(sendbuf)),
                                             recvcounts, rdispls, *(std::begin(recvbuf))),
                        std::runtime_error, "error in mpi::alltoallv");
                   
      sendbuf.clear();

      if (recvbuf.size() > 0)
        {
          data::deserialize_rank_tree_map (size, recvbuf, recvcounts, rdispls, tree_map);
        }
      recvbuf.clear();


      for (string attr_name_space : attr_name_spaces)
        {
          data::NamedAttrMap attr_map;
          scatter_read_cell_attributes(all_comm, file_name, io_size,
                                       attr_name_space, node_rank_map,
                                       pop_name, pop_start, attr_map,
                                       args...);
          attr_maps.insert(make_pair(attr_name_space, attr_map));
        }

    }
    

    template <class args_t ...>
    void append_trees
    (const std::string& file_name,
     const std::string& pop_name,
     const CELL_IDX_T& pop_start,
     std::vector<neurotree_t> &tree_list,
     CellPtr ptr_type = CellPtr(PtrOwner),
     args_t&&... args
     )
    {

      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      std::vector<SEC_PTR_T> sec_ptr;
      std::vector<TOPO_PTR_T> topo_ptr;
      std::vector<ATTR_PTR_T> attr_ptr;
    
      std::vector<CELL_IDX_T> all_index_vector;
      std::vector<SECTION_IDX_T> all_src_vector, all_dst_vector;
      std::vector<COORD_T> all_xcoords, all_ycoords, all_zcoords;  // coordinates of nodes
      std::vector<REALVAL_T> all_radiuses;    // Radius
      std::vector<LAYER_IDX_T> all_layers;        // Layer
      std::vector<SECTION_IDX_T> all_sections;    // Section
      std::vector<PARENT_NODE_IDX_T> all_parents; // Parent
      std::vector<SWC_TYPE_T> all_swc_types; // SWC Types

      if (ptr_type.type == PtrNone)
        {
          throw_assert(tree_list.size() == 1,
                       "append_trees: pointer type is PtrNone but number of trees is greater than one");

          build_tree_datasets(tree_list,
                              all_src_vector, all_dst_vector,
                              all_xcoords, all_ycoords, all_zcoords, 
                              all_radiuses, all_layers, all_sections,
                              all_parents, all_swc_types,
                              comm);
        }
      else
        {
          build_tree_datasets(tree_list,
                              all_index_vector, all_src_vector, all_dst_vector,
                              all_xcoords, all_ycoords, all_zcoords, 
                              all_radiuses, all_layers, all_sections,
                              all_parents, all_swc_types,
                              sec_ptr, topo_ptr, attr_ptr,
                              comm);
        }

      
      append_cell_index (file_name, pop_name, path::TREES, all_index_vector, comm);

      string attr_ptr_owner_path = path::cell_attribute_path(path::TREES, pop_name, path::X_COORD) +
        "/" + path::ATTR_PTR;
      string sec_ptr_owner_path  = path::cell_attribute_path(path::TREES, pop_name, path::SRCSEC) +
        "/" + path::SEC_PTR;
      
      append_cell_attribute<COORD_T> (file_name, path::TREES, pop_name, pop_start, path::X_COORD,
                                      all_index_vector, attr_ptr, all_xcoords,
                                      IndexShared, CellPtr (PtrOwner, path::ATTR_PTR),
                                      comm);
      append_cell_attribute<COORD_T>  (file_name, path::TREES, pop_name, pop_start, path::Y_COORD,
                                       all_index_vector, attr_ptr, all_ycoords,
                                       IndexShared, CellPtr (PtrShared, attr_ptr_owner_path),
                                       comm);
      append_cell_attribute<COORD_T>  (file_name, path::TREES, pop_name, pop_start, path::Z_COORD,
                                       all_index_vector, attr_ptr, all_zcoords,
                                       IndexShared, CellPtr (PtrShared, attr_ptr_owner_path),
                                       comm);
      append_cell_attribute<REALVAL_T> (file_name, path::TREES, pop_name, pop_start, path::RADIUS,
                                        all_index_vector, attr_ptr, all_radiuses,
                                        IndexShared, CellPtr (PtrShared, attr_ptr_owner_path),
                                        comm);
      append_cell_attribute<LAYER_IDX_T> (file_name, path::TREES, pop_name, pop_start, path::LAYER,
                                          all_index_vector, attr_ptr, all_layers,
                                          IndexShared, CellPtr (PtrShared, attr_ptr_owner_path),
                                          comm);
      append_cell_attribute<PARENT_NODE_IDX_T> (file_name, path::TREES, pop_name, pop_start, path::PARENT,
                                                all_index_vector, attr_ptr, all_parents,
                                                IndexShared, CellPtr (PtrShared, attr_ptr_owner_path),
                                                comm);
      append_cell_attribute<SWC_TYPE_T> (file_name, path::TREES, pop_name, pop_start, path::SWCTYPE,
                                         all_index_vector, attr_ptr, all_swc_types,
                                         IndexShared, CellPtr (PtrShared, attr_ptr_owner_path),
                                         comm);
      append_cell_attribute<SECTION_IDX_T> (file_name, path::TREES, pop_name, pop_start, path::SRCSEC,
                                            all_index_vector, topo_ptr, all_src_vector,
                                            IndexShared, CellPtr (PtrOwner, path::SEC_PTR),
                                            comm);
      append_cell_attribute<SECTION_IDX_T> (file_name, path::TREES, pop_name, pop_start, path::DSTSEC,
                                            all_index_vector, topo_ptr, all_dst_vector,
                                            IndexShared, CellPtr (PtrShared, sec_ptr_owner_path),
                                            comm);

      append_cell_attribute<SECTION_IDX_T> (file_name, path::TREES, pop_name, pop_start, path::SECTION,
                                            all_index_vector, sec_ptr, all_sections,
                                            IndexShared, CellPtr (PtrOwner, path::SEC_PTR),
                                            comm);
      
      
      
    }
  }
}

#endif
