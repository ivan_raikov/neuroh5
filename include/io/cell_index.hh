// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file cell_index
///
///  Routines for reading and writing the indices of cells for which attributes are defined.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================
#ifndef CELL_INDEX_HH
#define CELL_INDEX_HH

#include <mpi.h>
#include <vector>

#include "neuroh5_types.hh"
#include "neuroh5_paths.hh"
#include "mpi_error.hh"
#include "tuple_type_index.hh"
#include "throw_assert.hh"

using namespace neuroh5;

namespace neuroh5
{
  namespace io
  {
    template <class args_t ...>
    void create_cell_index
    (
     const string&        file_name,
     const string&        pop_name,
     const string&        attr_name_space,
     const size_t         chunk_size = 1000,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      string attr_prefix = path::cell_attribute_prefix(attr_name_space, pop_name);
      string index_path   = path::cell_attribute_path(attr_name_space, pop_name, path::CELL_INDEX);

      bool has_index=false, has_group=false;
      if (rank == 0)
        {
          h5::fd_t fd = h5::open(file_name, H5F_ACC_RDWR);

          has_group = lowio::exists_dataset (fd, attr_prefix);
          if (!has_group)
            {
              lowio::create_group (fd, attr_prefix);
            }
          else
            {
              has_index = lowio::exists_dataset (fd, index_path);
            }

          if (!has_index)
            {

              const h5::max_dims_t max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});

#if H5_VERSION_GE(1,10,2)
              const h5::dcpl_t dcpl = h5::deflate{9} | h5::chunk{chunk_size} | h5::alloc_time(h5::alloc_time_early);
#else
              const h5::dcpl_t dcpl = h5::chunk{chunk_size} | h5::alloc_time(h5::alloc_time_early);
#endif

              h5::ds_t ds = h5::create<CELL_IDX_T>(fd, index_path, max_dims, dcpl);
            }
        }
    }
    
    
    template <class args_t ...>
    void read_cell_index
    (
     const string&        file_name,
     const string&        pop_name,
     const string&        attr_name_space,
     vector<CELL_IDX_T>&  cell_index,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      if (rank == 0)
        {
          fd_t fd = h5::open(file_name, H5F_ACC_RDONLY);

          size_t dset_size = lowio::dataset_num_elements(fd, path::cell_attribute_path(attr_name_space, pop_name, path::CELL_INDEX));
          
          cell_index.resize(dset_size);
          h5::read<CELL_IDX_T> (fd, path::cell_attribute_path(attr_name_space, pop_name, path::CELL_INDEX),
                                dset_size,*(std::begin(cell_index)));
          
          // Ensure that every cell index is unique
          std::set<CELL_IDX_T> index_set;
          for (size_t i=0; i<cell_index.size(); i++)
            {
              index_set.insert(cell_index[i]);
            }
          throw_assert(cell_index.size() == index_set.size(),
                       "read_cell_index: duplicate elements in index");
        }
      
      if ( has_type<MPI_Comm, args_t...>::value )
        {
          size_t numitems = cell_index.size();
          MPI_CHECK_SUCCESS(MPI_Bcast(&numitems, 1, MPI_SIZE_T, 0, comm),
                            std::runtime_error, "error in MPI_Bcast");
          
          cell_index.resize(numitems);
          MPI_CHECK_SUCCESS(MPI_Bcast(&(cell_index[0]), numitems, MPI_CELL_IDX_T, 0, comm),
                            std::runtime_error, "error in MPI_Bcast");
        }
      
    }

    template <class args_t ...>
    void append_cell_index
    (
     const string&        file_name,
     const string&        pop_name,
     const string&        attr_name_space,
     const vector<CELL_IDX_T>&  cell_index,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const collective_t collective = get<collective_t>(args_tuple, true);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }

      size_t local_index_size = cell_index.size();

      std::vector<size_t> index_size_vector;
      index_size_vector.resize(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&local_index_size, 1, MPI_SIZE_T, &index_size_vector[0], 1, MPI_SIZE_T, comm),
                        std::runtime_error, "error in MPI_Allgather");

      size_t local_index_start = 0;
      for (size_t i=0; i<rank; i++)
        {
          local_index_start = local_index_start + index_size_vector[i];
        }
      size_t global_index_size = 0;
      for (size_t i=0; i<size; i++)
        {
          global_index_size = global_index_size + index_size_vector[i];
        }

      create_cell_index(file_name, pop_name, attr_name_space, comm);

      string path = path::cell_attribute_path(attr_name_space, pop_name, path::CELL_INDEX);

      /* Create property list for collective dataset operations. */
      const h5::dxpl_t dxpl = collective ? h5::mpio_collective_opt : h5::default_dxpl;
      const h5::dcpl_t& dcpl = get<const h5::dcpl_t&>(args_tuple, h5::alloc_time(h5::alloc_time_early));

      const h5::max_dims_t& max_dims = h5::max_dims_t(h5::max_dims{H5S_UNLIMITED});
      const h5::current_dims_t& data_dims = h5::current_dims_t(h5::current_dims{global_index_size});
      
      h5::append<CELL_IDX_T> (file_name, path, *(std::begin(cell_index)),
                              h5::count{local_index_size}, h5::offset{local_index_start},
                              data_dims, max_dims, dxpl, dcpl, args...);

    }

    void link_cell_index
    (
     const string&        file_name,
     const string&        pop_name,
     const string&        attr_name_space,
     const string&        attr_name
     )
    {
      
      string attr_path = path::cell_attribute_path(attr_name_space, pop_name, attr_name);
      string attr_prefix = path::cell_attribute_prefix(attr_name_space, pop_name);
      
      h5::fd_t fd = h5::open_file(file_name, H5F_ACC_RDWR);
      
      h5::ds_t ds = h5::open(fd, attr_prefix + "/" + hdf5::CELL_INDEX);

      H5CPP_CHECK_NZ((H5Olink(static_cast<hid_t>(ds), static_cast<hid_t>(fd),
                              (attr_path + "/" + path::CELL_INDEX).c_str(),
                              H5P_DEFAULT, H5P_DEFAULT)));

    }
    
    
  }
}

#endif
