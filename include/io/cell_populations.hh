// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file cell_populations.hh
///
///  Functions for reading population information and validating the
///  source and destination indices of edges.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================
#ifndef CELL_POPULATIONS_HH
#define CELL_POPULATIONS_HH

#include <mpi.h>
#include <vector>

#include "neuroh5_types.hh"
#include "mpi_error.hh"


namespace neuroh5
{
  namespace io
  {

    /// @brief Reads and optionally broadcasts the valid combinations of source/destination populations.
    ///
    /// @param file_name     Input file name
    ///
    /// @param pop_pairs     Set of source/destination pairs, filled by this
    ///                      procedure
    ///
    /// @param comm          MPI communicator [optional]
    ///
    /// @return
    template <class args_t...>
    void read_population_combs
    (const std::string& file_path,
     std::set< std::pair<pop_t,pop_t> >&  pop_pairs,
     args_t&&... args)
    {
      auto args_tuple = std::forward_as_tuple(args...);

      int rank=0, size=0;
      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, &size),
                            std::runtime_error, "unable to obtain MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, &rank),
                            std::runtime_error, "unable to obtain MPI communicator rank");
        }
      

      // rank 0 reads the number of pairs
      if (rank == 0)
        {
          size_t num_pairs = lowio::dataset_num_elements(file_path,
                                                         hdf5::h5types_path_join(hdf5::POP_COMBS));
          

          
          // allocate buffers
          vector<pop_t> v(2*num_pairs);

          // read the data in a vector
          h5::read(file_path, hdf5::h5types_path_join(hdf5::POP_COMBS),
                   *(std::begin(v)));

          // populate the set
          pop_pairs.clear();
          for (size_t i = 0; i < v.size(); i += 2)
            {
              pop_pairs.insert(make_pair(v[i], v[i+1]));
            }

        }
      
      // If MPI comm was provided, broadcast set
      if (size > 0)
      {
        vector<char> sendbuf;
        if (rank == 0)
          {
            data::serialize_data(pop_pairs, sendbuf);
            throw_assert(sendbuf.size() > 0,
                         "read_population_combs: error in serialization");
          }

        size_t sendbuf_size = sendbuf.size();
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf_size, 1, MPI_SIZE_T, 0, comm),
                          std::runtime_error, "error in MPI broadcast");
        sendbuf.resize(sendbuf_size);
        
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf[0], sendbuf.size(), MPI_CHAR, 0, comm),
                          std::runtime_error, "error in MPI broadcast");
        
        if (rank != 0)
          {
            data::deserialize_data(sendbuf, pop_pairs);
          }
      }


    }

    /// @brief Reads and optionally broadcasts the id ranges of each population
    ///
    /// @param file_name     Input file name
    ///
    /// @param pop_ranges    Map where the key is the starting index of a
    ///                      population, and the value is the number of nodes
    ///                      (vertices) and population index, filled by this
    ///                      procedure
    ///
    /// @param pop_vector    Vector of tuples <start,count,population index>
    ///                      for each population, filled by this procedure
    ///
    /// @param total_num_nodes    Updated with total number of nodes (vertices)
    ///
    /// @param comm          MPI communicator [optional]
    ///
    /// @return
    template <class args_t...>
    void read_population_ranges
    (const std::string&        file_path,
     pop_range_map_t&          pop_ranges,
     std::vector<pop_range_t>& pop_vector,
     size_t&                   n_nodes,
     args_t&&... args)
    {
      auto args_tuple = std::forward_as_tuple(args...);

      int rank=0, size=0;
      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);
      
      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, &size),
                            std::runtime_error, "unable to obtain MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, &rank) == MPI_SUCCESS,
                            std::runtime_error, "unable to obtain MPI communicator rank");
        }

      size_t num_ranges = 0;
      
      // rank 0 reads the population ranges
      if (rank == 0)
        {
          num_ranges = lowio::dataset_num_elements(file_path,
                                                   lowio::h5types_path_join(hdf5::POPULATIONS));
          pop_vector.clear();
          pop_vector.resize(num_ranges);

          // read the data in a vector
          h5::read(file_path, hdf5::h5types_path_join(hdf5::POPULATIONS),
                   *(std::begin(pop_vector)));
          
        }

      // If MPI comm was provided, broadcast population range vector
      if (size > 0)
        {
          MPI_CHECK_SUCCESS(MPI_Bcast(&num_ranges, 1, MPI_SIZE_T, 0, comm),
                            std::runtime_error, "read_population_ranges: error in MPI broadcast");

          throw_assert(num_ranges > 0,
                       "read_population_ranges: invalid number of ranges");
                                    
          // allocate buffers
          pop_vector.resize(num_ranges);
          
          // MPI rank 0 broadcasts the population ranges
          MPI_CHECK_SUCCESS(MPI_Bcast(&pop_vector[0], (int)num_ranges*sizeof(pop_range_t),
                                      MPI_BYTE, 0, comm),
                            std::runtime_error, "error in MPI broadcast");
          
        }

      pop_ranges.clear();
      n_nodes = 0;
      for(size_t i = 0; i < pop_vector.size(); ++i)
        {
          pop_ranges.insert(make_pair(pop_vector[i].start,
                                      make_pair(pop_vector[i].count,
                                                pop_vector[i].pop)));
          n_nodes = n_nodes + pop_vector[i].count;
        }

    }

    
    template <class args_t...>
    void read_population_labels
    (const std::string& file_path,
     std::vector< std::pair<pop_t, std::string> > & pop_labels,
     args_t&&... args)
    {
      vector <string> pop_name_vector;

      auto args_tuple = std::forward_as_tuple(args...);

      int rank=0, size=0;
      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);
      
      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, &size),
                            std::runtime_error, "unable to obtain MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, &rank),
                            std::runtime_error, "unable to obtain MPI communicator rank");
        }

      size_t num_labels = 0;
      
      // rank 0 reads the population labels
      if (rank == 0)
        {

          h5::cdt_t& pop_labels_type = h5::open(file_path,
                                                lowio::h5types_path_join(hdf5::POPULATIONS));

          num_labels = h5::get_nmembers(pop_labels_type);

          for (size_t i=0; i<num_labels; i++)
            {
              
              pop_name_vector.push_back(h5::nameof(pop_labels_type, i));
            }
          
        }

      for (uint16_t i=0; i<pop_name_vector.size(); i++)
        {
          pop_labels.push_back(make_pair(i, pop_name_vector[i]));
        }
      
      // If MPI comm was provided, broadcast population label vector
      if (size > 0)
        {
          vector<char> sendbuf; size_t sendbuf_size=0;
          if (rank == 0)
            {
              data::serialize_data(pop_labels, sendbuf);
              sendbuf_size = sendbuf.size();
            }

          MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf_size, 1, MPI_SIZE_T, 0, comm),
                            std::runtime_error, "error in MPI broadcast");
          throw_assert(sendbuf_size > 0,
                       "read_population_labels: invalid broadcast size");

          sendbuf.resize(sendbuf_size);
          MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf[0], sendbuf_size, MPI_CHAR, 0, comm),
                            std::runtime_error, "error in MPI broadcast");
        
          if (rank != 0)
            {
              data::deserialize_data(sendbuf, pop_labels);
            }
          
        }
    }

    
    
    template <class args_t...>
    void read_population_names
    (const std::string&   file_path,
     vector<string>&      pop_names,
     args_t&&... args)
    {
      auto args_tuple = std::forward_as_tuple(args...);

      int rank=0, size=0;
    
      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, &size),
                            std::runtime_error, "unable to obtain MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, &rank),
                            std::runtime_error, "unable to obtain MPI communicator rank");
        }
    
      // Rank 0 reads the names of populations
      if (rank == 0)
        {
          lowio::group_contents(file_path, hdf5::POPULATIONS, pop_names);
        }

      // If MPI comm is given, broadcasts population names
      if (size > 0)
      {
        vector<char> sendbuf;
        if (rank == 0)
          {
            data::serialize_data(pop_names, sendbuf);
            throw_assert(sendbuf.size() > 0,
                         "read_population_names: invalid broadcast size");
          }

        size_t sendbuf_size = sendbuf.size();
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf_size, 1, MPI_SIZE_T, 0, comm),
                          std::runtime_error, "error in MPI broadcast");
        sendbuf.resize(sendbuf_size);
        
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf[0], sendbuf.size(), MPI_CHAR, 0, comm),
                          std::runtime_error, "error in MPI broadcast");
        
        if (rank != 0)
          {
            data::deserialize_data(sendbuf, pop_names);
          }

      }


    }


    
  }
}

#endif
