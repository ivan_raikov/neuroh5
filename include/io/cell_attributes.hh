// -*- mode: c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
//==============================================================================
///  @file cell_attributes.hh
///
///  Routines for reading and writing cell attributes.
///
///  Copyright (C) 2016-2019 Project NeuroH5.
//==============================================================================
#ifndef NEUROH5_CELL_ATTRIBUTES_HH
#define NEUROH5_CELL_ATTRIBUTES_HH

#include <cstdint>
#include <string>
#include <type_traits>
#include <vector>

#include <mpi.h>

#include "mpe_seq.hh"
#include "neuroh5_types.hh"
#include "neuroh5_paths.hh"
#include "infer_datatype.hh"
#include "infer_mpi_datatype.hh"
#include "mpi_error.hh"
#include "cell_attribute_datasets.hh"
#include "attr_map.hh"
#include "tuple_type_index.hh"
#include "throw_assert.hh"


namespace neuroh5
{
  namespace io
  {

    template <>
    void num_cell_attributes
    (
     const vector< pair<string,AttrKind> >& attributes,
     vector <size_t> &num_attrs
     )
    {
      num_attrs.resize(data::AttrMap::num_attr_types);
      for (size_t i = 0; i < attributes.size(); i++)
        {
          AttrKind attr_kind = attributes[i].second;
          size_t attr_size = attr_kind.size;
          switch (attr_kind.type)
            {
            case UIntVal:
              if (attr_size == 4)
                {
                  num_attrs[data::AttrMap::attr_index_uint32]++;
                }
              else if (attr_size == 2)
                {
                  num_attrs[data::AttrMap::attr_index_uint16]++;
                }
              else if (attr_size == 1)
                {
                  num_attrs[data::AttrMap::attr_index_uint8]++;
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case SIntVal:
              if (attr_size == 4)
                {
                  num_attrs[data::AttrMap::attr_index_int32]++;
                }
              else if (attr_size == 2)
                {
                  num_attrs[data::AttrMap::attr_index_int16]++;
                }
              else if (attr_size == 1)
                {
                  num_attrs[data::AttrMap::attr_index_int8]++;
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
              
            case FloatVal:
              num_attrs[data::AttrMap::attr_index_float]++;
              break;
            case EnumVal:
              if (attr_size == 1)
                {
                  num_attrs[data::AttrMap::attr_index_uint8]++;
                }
              else
                {
                  throw runtime_error("Unsupported enumerated attribute size");
                };
              break;
            default:
              throw runtime_error("Unsupported attribute type");
              break;
            }

        }
    }
    
    
    template <class args_t ...>
    void read_cell_attributes
    (
     const string& file_name,
     const string& name_space,
     const string& pop_name,
     const CELL_IDX_T& pop_start,
     data::NamedAttrMap& attr_values,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
    
      const h5::fapl_t& fapl = comm == MPI_COMM_NULL ? default_fapl :
        fapl_mpio{comm, MPI_INFO_NULL};


      h5::fd_t fd = h5::open( file_path, H5F_ACC_RDONLY, fapl );
      
      vector< pair<string,AttrKind> > attr_info;
      lowio::get_cell_attributes (fd, name_space, pop_name, attr_info);

      for (size_t i=0; i<attr_info.size(); i++)
        {
          vector<CELL_IDX_T>  index;
          vector<ATTR_PTR_T>  ptr;

          string attr_name  = attr_info[i].first;
          AttrKind attr_kind = attr_info[i].second;
          size_t attr_size  = attr_kind.size;
          string attr_path  = cell_attribute_path (name_space, pop_name, attr_name);
          
          switch (attr_kind.type)
            {
            case UIntVal:
              if (attr_size == 4)
                {
                  vector<uint32_t> attr_values_uint32;
                  lowio::read_cell_attribute_datasets<uint32_t>(fd, attr_path, pop_start,
                                                                index, ptr, attr_values_uint32,
                                                                args...);
                  attr_values.insert(attr_name, index, ptr, attr_values_uint32);
                }
              else if (attr_size == 2)
                {
                  vector<uint16_t> attr_values_uint16;
                  lowio::read_cell_attribute_datasets<uint16_t>(fd, attr_path, pop_start,
                                                                index, ptr, attr_values_uint16,
                                                                args...);
                  attr_values.insert(attr_name, index, ptr, attr_values_uint16);
                }
              else if (attr_size == 1)
                {
                  vector<uint8_t> attr_values_uint8;
                  lowio::read_cell_attribute_datasets<uint8_t>(fd, attr_path, pop_start,
                                                               index, ptr, attr_values_uint8,
                                                               args...);
                  attr_values.insert(attr_name, index, ptr, attr_values_uint8);
                }
              else
                {
                  throw runtime_error("Unsupported integer attribute size");
                };
              break;
            case SIntVal:
              {
                if (attr_size == 4)
                  {
                    vector<int32_t> attr_values_int32;
                    lowio::read_cell_attribute_datasets<int32_t>(fd, attr_path, pop_start,
                                                                 index, ptr, attr_values_int32,
                                                                 args...);
                    attr_values.insert(attr_name, index, ptr, attr_values_int32);
                  }
                else if (attr_size == 2)
                  {
                    vector<int16_t> attr_values_int16;
                    lowio::read_cell_attribute_datasets<int16_t>(fd, attr_path, pop_start,
                                                                 index, ptr, attr_values_int16,
                                                                 args...);
                    attr_values.insert(attr_name, index, ptr, attr_values_int16);
                  }
                else if (attr_size == 1)
                  {
                    vector<int8_t> attr_values_int8;
                    lowio::read_cell_attribute_datasets<int8_t>(fd, attr_path, pop_start,
                                                                index, ptr, attr_values_int8,
                                                                args...);
                    attr_values.insert(attr_name, index, ptr, attr_values_int8);
                  }
                else
                  {
                    throw runtime_error("Unsupported integer attribute size");
                  };
              }
              break;
            case FloatVal:
              {
                vector<float> attr_values_float;
                lowio::read_cell_attribute_datasets(fd, attr_path, pop_start,
                                                    index, ptr, attr_values_float,
                                                    args...);
                attr_values.insert(attr_name, index, ptr, attr_values_float);
              }
              break;
            case EnumVal:
              {
                if (attr_size == 1)
                  {
                    vector<uint8_t> attr_values_uint8;
                    lowio::read_cell_attribute_datasets<uint8_t>(fd, attr_path, pop_start,
                                                                 index, ptr, attr_values_uint8,
                                                                 args...);
                    attr_values.insert(attr_name, index, ptr, attr_values_uint8);
                  }
                else
                  {
                    throw runtime_error("Unsupported enumerated attribute size");
                  };
              }
              break;
            default:
              throw runtime_error("Unsupported attribute type");
              break;
            }
        }

    }

    
    template <class args_t ...>
    void bcast_cell_attributes
    (
     MPI_Comm               comm,
     const int              root,
     const string&          file_name,
     const string&          name_space,
     const string&          pop_name,
     const CELL_IDX_T&       pop_start,
     data::NamedAttrMap&    attr_values,
     args_t&&... args
     )
    {
      unsigned int rank, size;
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");
      
      MPI_Comm io_comm;
      int io_color = 1;
      if (rank == (unsigned int)root)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_split(comm,io_color,rank,&io_comm),
                            std::runtime_error, "error in MPI_Comm_split");
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);
        }
      else
        {
          MPI_CHECK_SUCCESS(MPI_Comm_split(comm,0,rank,&io_comm),
                            std::runtime_error, "error in MPI_Comm_split");
        }
      MPI_CHECK_SUCCESS(MPI_Barrier(comm),
                        std::runtime_error, "error in MPI_Barrier");

      if (rank == (unsigned int)root)
        {
          read_cell_attributes(file_name, name_space, pop_name, pop_start,
                               attr_values, io_comm, args...);
        }

      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm),
                        std::runtime_error, "error in MPI_Comm_free");
      
      vector<size_t> num_attrs_bcast(num_attrs.size());
      for (size_t i=0; i<num_attrs.size(); i++)
        {
          num_attrs_bcast[i] = num_attrs[i];
        }
      // Broadcast the number of attributes of each type to all ranks
      MPI_CHECK_SUCCESS(MPI_Bcast(&num_attrs_bcast[0], num_attrs_bcast.size(), MPI_SIZE_T, root, comm),
                        std::runtime_error, "error in MPI_Bcast");
                   
      for (size_t i=0; i<num_attrs.size(); i++)
        {
          num_attrs[i] = num_attrs_bcast[i];
        }
    
      // Broadcast the names of each attributes of each type to all ranks
      {
        vector<char> sendbuf;
        size_t sendbuf_size=0;
        if (rank == (unsigned int)root)
          {
            data::serialize_data(attr_names, sendbuf);
            sendbuf_size = sendbuf.size();
          }
        
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf_size, 1, MPI_SIZE_T, root, comm),
                          std::runtime_error, "error in MPI_Bcast");
        
        sendbuf.resize(sendbuf_size);
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf[0], sendbuf.size(), MPI_CHAR, root, comm),
                          std::runtime_error, "error in MPI_Bcast");
        
        if (rank != (unsigned int)root)
          {
            data::deserialize_data(sendbuf, attr_names);
          }
      }

      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_float]; i++)
        {
          attr_map.insert_name<float>(attr_names[data::AttrMap::attr_index_float][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_uint8]; i++)
        {
          attr_map.insert_name<uint8_t>(attr_names[data::AttrMap::attr_index_uint8][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_int8]; i++)
        {
          attr_map.insert_name<int8_t>(attr_names[data::AttrMap::attr_index_int8][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_uint16]; i++)
        {
          attr_map.insert_name<uint16_t>(attr_names[data::AttrMap::attr_index_uint16][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_int16]; i++)
        {
          attr_map.insert_name<int16_t>(attr_names[data::AttrMap::attr_index_int16][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_uint32]; i++)
        {
          attr_map.insert_name<uint32_t>(attr_names[data::AttrMap::attr_index_uint32][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_int32]; i++)
        {
          attr_map.insert_name<int32_t>(attr_names[data::AttrMap::attr_index_int32][i],i);
        }

      size_t sendrecvbuf_size = sendrecvbuf.size();
      MPI_CHECK_SUCCESS(MPI_Bcast(&sendrecvbuf_size, 1, MPI_SIZE_T, root, comm),
                        std::runtime_error, "error in MPI_Bcast");
      sendrecvbuf.resize(sendrecvbuf_size);
      MPI_CHECK_SUCCESS(MPI_Bcast(&sendrecvbuf[0], sendrecvbuf_size, MPI_CHAR, root, comm),
                        std::runtime_error, "error in MPI_Bcast");
      
      if (rank != (unsigned int)root)
        {
          data::deserialize_data(sendrecvbuf, attr_map);
        }
        
    }

    
    template <class args_t ...>
    void scatter_read_cell_attributes
    (
     MPI_Comm                      all_comm,
     const string                 &file_name,
     const int                     io_size,
     const string                 &attr_name_space,
     // A vector that maps nodes to compute ranks
     const map<CELL_IDX_T, rank_t> &node_rank_map,
     const string                 &pop_name,
     const CELL_IDX_T             &pop_start,
     data::NamedAttrMap           &attr_map,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, h5::default_offset);
      const h5::count_t& count = get<const h5::offset_t&>(args_tuple, h5::default_offset);
            
      unsigned int rank, size;
      MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                        std::runtime_error, "unable to determine MPI communicator size");
      MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                        std::runtime_error, "unable to determine MPI communicator rank");
      
      // MPI Communicator for I/O ranks
      MPI_Comm io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1;

      throw_assert(io_size > 0,
                   "io_size must be a positive integer");
      set<size_t> io_rank_set;
      data::range_sample(size, io_size, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
    
      vector<char> sendbuf; 
      vector<int> sendcounts(size,0), sdispls(size,0), recvcounts(size,0), rdispls(size,0);

      const h5::count_t new_count = h5::count{count[0] * size};
        
      // Am I an I/O rank?
      if (is_io_rank)
        {
          MPI_CHECK_SUCCESS(MPI_Comm_split(all_comm,io_color,rank,&io_comm),
                            std::runtime_error, "error in MPI_Comm_split");
          MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);

          map <rank_t, data::AttrMap > rank_attr_map;
          {
            data::NamedAttrMap  attr_values;
            read_cell_attributes(file_name, attr_name_space, pop_name, pop_start,
                                 attr_values, io_comm, new_count, args...);
            data::append_rank_attr_map(attr_values, node_rank_map, rank_attr_map);
            attr_values.num_attrs(num_attrs);
            attr_values.attr_names(attr_names);
          }
          data::serialize_rank_attr_map (size, rank, rank_attr_map, sendcounts, sendbuf, sdispls);
        }
      else
        {

          MPI_CHECK_SUCCESS(MPI_Comm_split(all_comm,0,rank,&io_comm),
                            std::runtime_error, "error in MPI_Comm_split");
        }

      MPI_CHECK_SUCCESS(MPI_Barrier(io_comm),
                        std::runtime_error, "error in MPI_Barrier");
      MPI_CHECK_SUCCESS(MPI_Barrier(all_comm),
                        std::runtime_error, "error in MPI_Barrier");

      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm),
                        std::runtime_error, "error in MPI_Comm_free");

          
      vector<size_t> num_attrs_bcast(num_attrs.size());
      for (size_t i=0; i<num_attrs.size(); i++)
        {
          num_attrs_bcast[i] = num_attrs[i];
        }
      
      // 4. Broadcast the number of attributes of each type to all ranks
      MPI_CHECK_SUCCESS(MPI_Bcast(&num_attrs_bcast[0], num_attrs_bcast.size(), MPI_SIZE_T, 0, all_comm),
                        std::runtime_error, "error in MPI_Bcast");
      for (size_t i=0; i<num_attrs.size(); i++)
        {
          num_attrs[i] = num_attrs_bcast[i];
        }
    
      // 5. Broadcast the names of each attributes of each type to all ranks
      {
        vector<char> sendbuf; size_t sendbuf_size=0;
        if (rank == 0)
          {
            data::serialize_data(attr_names, sendbuf);
            sendbuf_size = sendbuf.size();
          }

        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf_size, 1, MPI_SIZE_T, 0, all_comm),
                          std::runtime_error, "error in MPI_Bcast");
        sendbuf.resize(sendbuf_size);
        MPI_CHECK_SUCCESS(MPI_Bcast(&sendbuf[0], sendbuf_size, MPI_CHAR, 0, all_comm),
                          std::runtime_error, "error in MPI_Bcast");
        
        if (rank != 0)
          {
            data::deserialize_data(sendbuf, attr_names);
          }
      }
      
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_float]; i++)
        {
          attr_map.insert_name<float>(attr_names[data::AttrMap::attr_index_float][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_uint8]; i++)
        {
          attr_map.insert_name<uint8_t>(attr_names[data::AttrMap::attr_index_uint8][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_int8]; i++)
        {
          attr_map.insert_name<int8_t>(attr_names[data::AttrMap::attr_index_int8][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_uint16]; i++)
        {
          attr_map.insert_name<uint16_t>(attr_names[data::AttrMap::attr_index_uint16][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_int16]; i++)
        {
          attr_map.insert_name<int16_t>(attr_names[data::AttrMap::attr_index_int16][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_uint32]; i++)
        {
          attr_map.insert_name<uint32_t>(attr_names[data::AttrMap::attr_index_uint32][i],i);
        }
      for (size_t i=0; i<num_attrs[data::AttrMap::attr_index_int32]; i++)
        {
          attr_map.insert_name<int32_t>(attr_names[data::AttrMap::attr_index_int32][i],i);
        }
    
      // 6. Each ALL_COMM rank sends an attribute set size to
      //    every other ALL_COMM rank (non IO_COMM ranks pass zero)
    
      MPI_CHECK_SUCCESS(MPI_Alltoall(&sendcounts[0], 1, MPI_INT,
                                     &recvcounts[0], 1, MPI_INT, all_comm),
                   std::runtime_error, "error in MPI_Alltoall");
    
      // 7. Each ALL_COMM rank accumulates the vector sizes and allocates
      //    a receive buffer, recvcounts, and rdispls
      size_t recvbuf_size;
      vector<char> recvbuf;

      recvbuf_size = recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          rdispls[p] = rdispls[p-1] + recvcounts[p-1];
          recvbuf_size += recvcounts[p];
        }
      if (recvbuf_size > 0)
        recvbuf.resize(recvbuf_size);
    
      // 8. Each ALL_COMM rank participates in the MPI_Alltoallv
      MPI_CHECK_SUCCESS(mpi::alltoallv<char>(all_comm, MPI_CHAR, sendcounts, sdispls, *(std::cbegin(sendbuf)),
                                             recvcounts, rdispls, *(std::begin(recvbuf))),
                   std::runtime_error, "error in mpi::alltoallv");

    
      sendbuf.clear();
    
      if (recvbuf.size() > 0)
        {
          data::deserialize_rank_attr_map (size, recvbuf, recvcounts, rdispls, attr_map);
        }
      recvbuf.clear();
    }

    

  
    template <typename T, class args_t ...>
    void append_cell_attribute
    (
     const std::string&                    file_name,
     const std::string&                    attr_namespace,
     const std::string&                    pop_name,
     const CELL_IDX_T&                     pop_start,
     const std::string&                    attr_name,
     const std::vector<CELL_IDX_T>&        index,
     const std::vector<ATTR_PTR_T>         attr_ptr,
     const std::vector<T>&                 values,
     const CellIndex                       index_type,
     const CellPtr                         ptr_type,
     const size_t chunk_size = 4000,
     const size_t value_chunk_size = 4000,
     const size_t cache_size = 1*1024*1024,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      int status;
      throw_assert(index.size() == attr_ptr.size()-1,
                   "append_cell_attribute: size mismatch between index and attribute pointer arrays");
      std::vector<ATTR_PTR_T>  local_attr_ptr;

      
      const h5::fapl_t& fapl = comm == MPI_COMM_NULL ? h5::default_fapl :
        h5::fapl_mpio{comm, MPI_INFO_NULL} | h5::cache{cache_size};
      
      h5::fd_t fd = h5::open(file_name, H5F_ACC_RDWR, fapl);

      string attr_prefix = cell_attribute_prefix(attr_namespace, pop_name);
      string attr_path = cell_attribute_path(attr_namespace, pop_name, attr_name);

      if (!(lowio::exists_dataset (fd, attr_path)))
        {
          lowio::create_cell_attribute_datasets<T>(fd, attr_namespace, pop_name, attr_name,
                                                   index_type, ptr_type,
                                                   chunk_size, value_chunk_size);
        }

      vector<CELL_IDX_T> rindex;

      for (const CELL_IDX_T& gid: index)
        {
          rindex.push_back(gid - pop_start);
        }
      
      lowio::append_cell_attribute<T>(fd, attr_path, rindex, attr_ptr, values,
                                      index_type, ptr_type,
                                      args...);
    }


    template <typename T, class args_t ...>
    void append_cell_attribute_map
    (
     const std::string&              file_name,
     const std::string&              attr_namespace,
     const std::string&              pop_name,
     const CELL_IDX_T&               pop_start,
     const std::string&              attr_name,
     const std::map<CELL_IDX_T, vector<T>>& value_map,
     const size_t io_size,
     const CellIndex                 index_type = IndexOwner,
     const CellPtr                   ptr_type = CellPtr(PtrOwner),
     const size_t chunk_size = 4000,
     const size_t value_chunk_size = 4000,
     const size_t cache_size = 1*1024*1024,
     args_t&&... args
     )
    {
      vector<CELL_IDX_T>  index_vector;
      vector<T>  value_vector;

      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      if (size < io_size)
        {
          io_size_value = size;
        }
      else
        {
          io_size_value = io_size;
        }

      set<size_t> io_rank_set;
      data::range_sample(size, io_size_value, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
      
      vector< pair<hsize_t,hsize_t> > ranges;
      mpi::rank_ranges(size, io_size_value, ranges);

      // Determine I/O ranks to which to send the values
      vector <size_t> io_dests(size); 
      for (size_t r=0; r<size; r++)
        {
          for (size_t i=ranges.size()-1; i>=0; i--)
            {
              if (ranges[i].first <= r)
                {
                  io_dests[r] = *std::next(io_rank_set.begin(), i);
                  break;
                }
            }
        }

      // Determine number of values for each rank
      vector<uint32_t> sendbuf_num_values(size, value_map.size());
      vector<uint32_t> recvbuf_num_values(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_values[0], 1, MPI_UINT32_T,
                                      &recvbuf_num_values[0], 1, MPI_UINT32_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
      sendbuf_num_values.clear();

      // Determine local value size and offset
      uint32_t local_value_size=0;
      for (auto const& element : value_map)
        {
          const vector<T> &v = element.second;
          local_value_size += v.size();
        }
      vector<uint32_t> sendbuf_size_values(size, local_value_size);
      vector<uint32_t> recvbuf_size_values(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_size_values[0], 1, MPI_UINT32_T,
                                      &recvbuf_size_values[0], 1, MPI_UINT32_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
      sendbuf_size_values.clear();
    
      // Create gid, attr_ptr, value arrays
      vector<ATTR_PTR_T>  attr_ptr;
      ATTR_PTR_T value_offset = 0;
      hsize_t global_value_size = 0;
      for (size_t i=0; i<size; i++)
        {
          if (i<rank)
            {
              value_offset += recvbuf_size_values[i];
            }
          global_value_size += recvbuf_size_values[i];
        }

      for (auto const& element : value_map)
        {
          const CELL_IDX_T gid = element.first;
          const vector<T> &v = element.second;
          index_vector.push_back(gid);
          attr_ptr.push_back(value_offset);
          value_vector.insert(value_vector.end(),v.begin(),v.end());
          value_offset = value_offset + v.size();
        }
      //attr_ptr.push_back(value_offset);

      vector<int> value_sendcounts(size, 0), value_sdispls(size, 0), 
        value_recvcounts(size, 0), value_rdispls(size, 0);
      value_sendcounts[io_dests[rank]] = local_value_size;

    
      // Compute size of values to receive for all I/O rank
      if (is_io_rank)
        {
          for (size_t s=0; s<size; s++)
            {
              if (io_dests[s] == rank)
                {
                  value_recvcounts[s] += recvbuf_size_values[s];
                }
            }
        }

      // Each ALL_COMM rank accumulates the vector sizes and allocates
      // a receive buffer, recvcounts, and rdispls
      size_t value_recvbuf_size = value_recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          value_rdispls[p] = value_rdispls[p-1] + value_recvcounts[p-1];
          value_recvbuf_size += value_recvcounts[p];
        }
      //assert(recvbuf_size > 0);
      vector<T> value_recvbuf(value_recvbuf_size);

      T dummy;
      MPI_Datatype mpi_type = infer_mpi_datatype(dummy);
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&value_vector[0], &value_sendcounts[0], &value_sdispls[0], mpi_type,
                                      &value_recvbuf[0], &value_recvcounts[0], &value_rdispls[0], mpi_type,
                                      comm),
                        std::runtime_error, "error in MPI_Alltoallv");
      value_vector.clear();
    
      vector<int> ptr_sendcounts(size, 0), ptr_sdispls(size, 0), ptr_recvcounts(size, 0), ptr_rdispls(size, 0);
      ptr_sendcounts[io_dests[rank]] = attr_ptr.size();

      // Compute number of values to receive for all I/O rank
      if (is_io_rank)
        {
          for (size_t s=0; s<size; s++)
            {
              if (io_dests[s] == rank)
                {
                  ptr_recvcounts[s] += recvbuf_num_values[s];
                }
            }
        }
    
      // Each ALL_COMM rank accumulates the vector sizes and allocates
      // a receive buffer, recvcounts, and rdispls
      size_t ptr_recvbuf_size = ptr_recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          ptr_rdispls[p] = ptr_rdispls[p-1] + ptr_recvcounts[p-1];
          ptr_recvbuf_size += ptr_recvcounts[p];
        }
      //assert(recvbuf_size > 0);
      vector<ATTR_PTR_T> attr_ptr_recvbuf(ptr_recvbuf_size);
    
      // Each ALL_COMM rank participates in the MPI_Alltoallv
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&attr_ptr[0], &ptr_sendcounts[0], &ptr_sdispls[0], MPI_ATTR_PTR_T,
                                      &attr_ptr_recvbuf[0], &ptr_recvcounts[0], &ptr_rdispls[0], MPI_ATTR_PTR_T,
                                      comm),
                        std::runtime_error, "error in MPI_Alltoallv");
      if (is_io_rank && (attr_ptr_recvbuf.size() > 0))
        {
          attr_ptr_recvbuf.push_back(attr_ptr_recvbuf[0]+value_recvbuf.size());
        }
      else
        {
          attr_ptr_recvbuf.push_back(value_recvbuf.size());
        }
      if (attr_ptr_recvbuf.size() > 0)
        {
          ATTR_PTR_T attr_ptr_recvbuf_base = attr_ptr_recvbuf[0];
          for (size_t i=0; i<attr_ptr_recvbuf.size(); i++)
            {
              attr_ptr_recvbuf[i] -= attr_ptr_recvbuf_base;
            }
        }
    
      attr_ptr.clear();
      ptr_sendcounts.clear();
      ptr_sdispls.clear();
      ptr_recvcounts.clear();
      ptr_rdispls.clear();

      vector<int> idx_sendcounts(size, 0), idx_sdispls(size, 0), idx_recvcounts(size, 0), idx_rdispls(size, 0);
      idx_sendcounts[io_dests[rank]] = index_vector.size();

      // Compute number of values to receive for all I/O rank
      if (is_io_rank)
        {
          for (size_t s=0; s<size; s++)
            {
              if (io_dests[s] == rank)
                {
                  idx_recvcounts[s] += recvbuf_num_values[s];
                }
            }
        }
    
      // Each ALL_COMM rank accumulates the vector sizes and allocates
      // a receive buffer, recvcounts, and rdispls
      size_t idx_recvbuf_size = idx_recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          idx_rdispls[p] = idx_rdispls[p-1] + idx_recvcounts[p-1];
          idx_recvbuf_size += idx_recvcounts[p];
        }

      vector<CELL_IDX_T> gid_recvbuf(idx_recvbuf_size);
    
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&index_vector[0], &idx_sendcounts[0], &idx_sdispls[0], MPI_CELL_IDX_T,
                                      &gid_recvbuf[0], &idx_recvcounts[0], &idx_rdispls[0], MPI_CELL_IDX_T,
                                      comm),
                        std::runtime_error, "error in MPI_Alltoallv");
      index_vector.clear();
      idx_sendcounts.clear();
      idx_sdispls.clear();
      idx_recvcounts.clear();
      idx_rdispls.clear();

    
      // MPI Communicator for I/O ranks
      MPI_Comm io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1, color;
    
      // Am I an I/O rank?
      if (is_io_rank)
        {
          color = io_color;
        }
      else
        {
          color = 0;
        }
      MPI_CHECK_SUCCESS(MPI_Comm_split(comm,color,rank,&io_comm),
                        std::runtime_error, "error in MPI_Comm_split");
      MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);

      if (is_io_rank)
        {
          append_cell_attribute<T>(file_name,
                                   attr_namespace, pop_name, pop_start, attr_name,
                                   gid_recvbuf, attr_ptr_recvbuf, value_recvbuf,
                                   index_type, ptr_type, 
                                   chunk_size, value_chunk_size, cache_size,
                                   io_comm, args...);
        }
      
      MPI_CHECK_SUCCESS(MPI_Barrier(io_comm), std::runtime_error, "error in MPI_Barrier");
      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm), std::runtime_error, "error in MPI_Comm_free");
      MPI_CHECK_SUCCESS(MPI_Barrier(comm), std::runtime_error, "error in MPI_Barrier");
    }


  
    template <typename T, class args_t ...>
    void write_cell_attribute
    (
     const std::string&                    file_name,
     const std::string&                    attr_namespace,
     const std::string&                    pop_name,
     const CELL_IDX_T&                     pop_start,
     const std::string&                    attr_name,
     const std::vector<CELL_IDX_T>&        index,
     const std::vector<ATTR_PTR_T>         attr_ptr,
     const std::vector<T>&                 values,
     const CellIndex                       index_type,
     const CellPtr                         ptr_type,
     const size_t chunk_size = 4000,
     const size_t value_chunk_size = 4000,
     const size_t cache_size = 1*1024*1024,
     args_t&&... args
     )
    {
      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      int status;
      throw_assert(index.size() == attr_ptr.size()-1,
                   "write_cell_attribute: size mismatch between index and attribute pointer arrays");
      std::vector<ATTR_PTR_T>  local_attr_ptr;

      
      const h5::fapl_t& fapl = comm == MPI_COMM_NULL ? h5::default_fapl :
        h5::fapl_mpio{comm, MPI_INFO_NULL} | h5::cache{cache_size};
      
      h5::fd_t fd = h5::open(file_name, H5F_ACC_RDWR, fapl);

      string attr_prefix = cell_attribute_prefix(attr_namespace, pop_name);
      string attr_path = cell_attribute_path(attr_namespace, pop_name, attr_name);

      vector<CELL_IDX_T> rindex;

      for (const CELL_IDX_T& gid: index)
        {
          rindex.push_back(gid - pop_start);
        }
      
      lowio::write_cell_attribute<T>(fd, attr_path, rindex, attr_ptr, values,
                                     index_type, ptr_type,
                                     args...);
    }


    template <typename T, class args_t ...>
    void write_cell_attribute_map
    (
     const std::string&              file_name,
     const std::string&              attr_namespace,
     const std::string&              pop_name,
     const CELL_IDX_T&               pop_start,
     const std::string&              attr_name,
     const std::map<CELL_IDX_T, vector<T>>& value_map,
     const size_t io_size,
     const CellIndex                 index_type = IndexOwner,
     const CellPtr                   ptr_type = CellPtr(PtrOwner),
     const size_t chunk_size = 4000,
     const size_t value_chunk_size = 4000,
     const size_t cache_size = 1*1024*1024,
     args_t&&... args
     )
    {
      vector<CELL_IDX_T>  index_vector;
      vector<T>  value_vector;

      auto args_tuple = std::forward_as_tuple(args...);

      MPI_Comm comm = get<MPI_Comm>(args_tuple, MPI_COMM_NULL);

      unsigned int rank=0, size=0;

      if ( has_type<MPI_Comm, args_t...>::value )
        {
          MPI_CHECK_SUCCESS(MPI_Comm_size(comm, (int*)&size),
                            std::runtime_error, "unable to determine MPI communicator size");
          MPI_CHECK_SUCCESS(MPI_Comm_rank(comm, (int*)&rank),
                            std::runtime_error, "unable to determine MPI communicator rank");
        }
      
      if (size < io_size)
        {
          io_size_value = size;
        }
      else
        {
          io_size_value = io_size;
        }
      
      set<size_t> io_rank_set;
      data::range_sample(size, io_size_value, io_rank_set);
      bool is_io_rank = false;
      if (io_rank_set.find(rank) != io_rank_set.end())
        is_io_rank = true;
      vector< pair<hsize_t,hsize_t> > ranges;
      mpi::rank_ranges(size, io_size_value, ranges);
      
      // Determine I/O ranks to which to send the values
      vector <size_t> io_dests(size); 
      for (size_t r=0; r<size; r++)
        {
          for (size_t i=ranges.size()-1; i>=0; i--)
            {
              if (ranges[i].first <= r)
                {
                  io_dests[r] = *std::next(io_rank_set.begin(), i);
                  break;
                }
            }
        }

      // Determine number of values for each rank
      vector<uint32_t> sendbuf_num_values(size, value_map.size());
      vector<uint32_t> recvbuf_num_values(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_num_values[0], 1, MPI_UINT32_T,
                                      &recvbuf_num_values[0], 1, MPI_UINT32_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
      sendbuf_num_values.clear();

      // Determine local value size and offset
      uint32_t local_value_size=0;
      for (auto const& element : value_map)
        {
          const vector<T> &v = element.second;
          local_value_size += v.size();
        }
      vector<uint32_t> sendbuf_size_values(size, local_value_size);
      vector<uint32_t> recvbuf_size_values(size);
      MPI_CHECK_SUCCESS(MPI_Allgather(&sendbuf_size_values[0], 1, MPI_UINT32_T,
                                      &recvbuf_size_values[0], 1, MPI_UINT32_T, comm),
                        std::runtime_error, "error in MPI_Allgather");
      sendbuf_size_values.clear();
    
      // Create gid, attr_ptr, value arrays
      vector<ATTR_PTR_T>  attr_ptr;
      ATTR_PTR_T value_offset = 0;
      hsize_t global_value_size = 0;
      for (size_t i=0; i<size; i++)
        {
          if (i<rank)
            {
              value_offset += recvbuf_size_values[i];
            }
          global_value_size += recvbuf_size_values[i];
        }

      for (auto const& element : value_map)
        {
          const CELL_IDX_T gid = element.first;
          const vector<T> &v = element.second;
          index_vector.push_back(gid);
          attr_ptr.push_back(value_offset);
          value_vector.insert(value_vector.end(),v.begin(),v.end());
          value_offset = value_offset + v.size();
        }
      //attr_ptr.push_back(value_offset);

      vector<int> value_sendcounts(size, 0), value_sdispls(size, 0), 
        value_recvcounts(size, 0), value_rdispls(size, 0);
      value_sendcounts[io_dests[rank]] = local_value_size;

    
      // Compute size of values to receive for all I/O rank
      if (is_io_rank)
        {
          for (size_t s=0; s<size; s++)
            {
              if (io_dests[s] == rank)
                {
                  value_recvcounts[s] += recvbuf_size_values[s];
                }
            }
        }

      // Each ALL_COMM rank accumulates the vector sizes and allocates
      // a receive buffer, recvcounts, and rdispls
      size_t value_recvbuf_size = value_recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          value_rdispls[p] = value_rdispls[p-1] + value_recvcounts[p-1];
          value_recvbuf_size += value_recvcounts[p];
        }
      //assert(recvbuf_size > 0);
      vector<T> value_recvbuf(value_recvbuf_size);

      T dummy;
      MPI_Datatype mpi_type = infer_mpi_datatype(dummy);
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&value_vector[0], &value_sendcounts[0], &value_sdispls[0], mpi_type,
                                      &value_recvbuf[0], &value_recvcounts[0], &value_rdispls[0], mpi_type,
                                      comm),
                        std::runtime_error, "error in MPI_Alltoallv");
      value_vector.clear();
    
      vector<int> ptr_sendcounts(size, 0), ptr_sdispls(size, 0), ptr_recvcounts(size, 0), ptr_rdispls(size, 0);
      ptr_sendcounts[io_dests[rank]] = attr_ptr.size();

      // Compute number of values to receive for all I/O rank
      if (is_io_rank)
        {
          for (size_t s=0; s<size; s++)
            {
              if (io_dests[s] == rank)
                {
                  ptr_recvcounts[s] += recvbuf_num_values[s];
                }
            }
        }
    
      // Each ALL_COMM rank accumulates the vector sizes and allocates
      // a receive buffer, recvcounts, and rdispls
      size_t ptr_recvbuf_size = ptr_recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          ptr_rdispls[p] = ptr_rdispls[p-1] + ptr_recvcounts[p-1];
          ptr_recvbuf_size += ptr_recvcounts[p];
        }
      //assert(recvbuf_size > 0);
      vector<ATTR_PTR_T> attr_ptr_recvbuf(ptr_recvbuf_size);
    
      // Each ALL_COMM rank participates in the MPI_Alltoallv
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&attr_ptr[0], &ptr_sendcounts[0], &ptr_sdispls[0], MPI_ATTR_PTR_T,
                                      &attr_ptr_recvbuf[0], &ptr_recvcounts[0], &ptr_rdispls[0], MPI_ATTR_PTR_T,
                                      comm),
                        std::runtime_error, "error in MPI_Alltoallv");
      if (is_io_rank && (attr_ptr_recvbuf.size() > 0))
        {
          attr_ptr_recvbuf.push_back(attr_ptr_recvbuf[0]+value_recvbuf.size());
        }
      else
        {
          attr_ptr_recvbuf.push_back(value_recvbuf.size());
        }
      if (attr_ptr_recvbuf.size() > 0)
        {
          ATTR_PTR_T attr_ptr_recvbuf_base = attr_ptr_recvbuf[0];
          for (size_t i=0; i<attr_ptr_recvbuf.size(); i++)
            {
              attr_ptr_recvbuf[i] -= attr_ptr_recvbuf_base;
            }
        }
    
      attr_ptr.clear();
      ptr_sendcounts.clear();
      ptr_sdispls.clear();
      ptr_recvcounts.clear();
      ptr_rdispls.clear();

      vector<int> idx_sendcounts(size, 0), idx_sdispls(size, 0), idx_recvcounts(size, 0), idx_rdispls(size, 0);
      idx_sendcounts[io_dests[rank]] = index_vector.size();

      // Compute number of values to receive for all I/O rank
      if (is_io_rank)
        {
          for (size_t s=0; s<size; s++)
            {
              if (io_dests[s] == rank)
                {
                  idx_recvcounts[s] += recvbuf_num_values[s];
                }
            }
        }
    
      // Each ALL_COMM rank accumulates the vector sizes and allocates
      // a receive buffer, recvcounts, and rdispls
      size_t idx_recvbuf_size = idx_recvcounts[0];
      for (int p = 1; p < ssize; ++p)
        {
          idx_rdispls[p] = idx_rdispls[p-1] + idx_recvcounts[p-1];
          idx_recvbuf_size += idx_recvcounts[p];
        }

      vector<CELL_IDX_T> gid_recvbuf(idx_recvbuf_size);
    
      MPI_CHECK_SUCCESS(MPI_Alltoallv(&index_vector[0], &idx_sendcounts[0], &idx_sdispls[0], MPI_CELL_IDX_T,
                                      &gid_recvbuf[0], &idx_recvcounts[0], &idx_rdispls[0], MPI_CELL_IDX_T,
                                      comm),
                        std::runtime_error, "error in MPI_Alltoallv");
      index_vector.clear();
      idx_sendcounts.clear();
      idx_sdispls.clear();
      idx_recvcounts.clear();
      idx_rdispls.clear();

    
      // MPI Communicator for I/O ranks
      MPI_Comm io_comm;
      // MPI group color value used for I/O ranks
      int io_color = 1, color;
    
      // Am I an I/O rank?
      if (is_io_rank)
        {
          color = io_color;
        }
      else
        {
          color = 0;
        }
      MPI_CHECK_SUCCESS(MPI_Comm_split(comm,color,rank,&io_comm),
                        std::runtime_error, "error in MPI_Comm_split");
      MPI_Comm_set_errhandler(io_comm, MPI_ERRORS_RETURN);

      if (is_io_rank)
        {
          write_cell_attribute<T>(file_name,
                                  attr_namespace, pop_name, pop_start, attr_name,
                                  gid_recvbuf, attr_ptr_recvbuf, value_recvbuf,
                                  index_type, ptr_type, 
                                  chunk_size, value_chunk_size, cache_size,
                                  io_comm, args...);
        }
      
      MPI_CHECK_SUCCESS(MPI_Barrier(io_comm), std::runtime_error, "error in MPI_Barrier");
      MPI_CHECK_SUCCESS(MPI_Comm_free(&io_comm), std::runtime_error, "error in MPI_Comm_free");
      MPI_CHECK_SUCCESS(MPI_Barrier(comm), std::runtime_error, "error in MPI_Barrier");
    }




  }
  
}



#endif
