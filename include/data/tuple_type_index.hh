
// Getting value from tuple by type (instead of index) in C++11

#ifndef TUPLE_TYPE_INDEX_HH
#define TUPLE_TYPE_INDEX_HH

#include<tuple>
#include<type_traits>
#include<string>
#include<iostream>

template< int Index, class Search, class First, class... Types >
struct get_index
{
    typedef typename get_index< Index + 1, Search, Types...>::type type;
    static constexpr int index = Index;
};

template< int Index, class Search, class... Types >
struct get_index< Index, Search, Search, Types... >
{
    typedef get_index type;
    static constexpr int index = Index;
};

template< int Index, class Search >
struct get_index< Index, Search, Search >
{
    typedef get_index type;
    static constexpr int index = Index;
};

template< int Index, class Search, class Other >
struct get_index< Index, Search, Other >
{
    typedef get_index type;
    static constexpr int index = -1;
};

template <class... Types>
struct has_type;

template <class T>
struct has_type< T >
{
  static constexpr bool value = false;
};

template <class T, class... Types>
struct has_type< T, Types...>
{
  static constexpr bool value = get_index< 0, T, Types... >::type::index >= 0;
};
              
 
template< class T, class... Types, typename std::enable_if < has_type<T, Types...>::value, void >::type* = nullptr >
const T& get( std::tuple< Types... >& tuple, const T& dflt)
{
    return std::get< get_index< 0, T, Types... >::type::index >(tuple);
}
 
template< class T, class... Types, typename std::enable_if < !has_type<T, Types...>::value, void >::type* = nullptr >
const T& get( std::tuple< Types... >& tuple, const T& dflt)
{
  return dflt;
}
 
 

#endif

