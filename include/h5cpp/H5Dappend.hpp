/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 */

#ifndef  H5CPP_APPEND_HPP
#define H5CPP_APPEND_HPP

namespace h5 {
  
  /** \func_append_hdr
   *  TODO: append doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t >
  h5::ds_t append( const h5::ds_t& ds, const T* ptr,
                   const count_t& count,
                   const current_dims_t& data_dims,
                   const dxpl_t& dxpl,
                   args_t&&... args)
    
    try {
      
      auto args_tuple = std::forward_as_tuple(args...);
      
      h5::current_dims_t new_dims, current_dims; int rank = 0;
      
      const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, default_offset);
      const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, default_stride);
      const h5::block_t& block = get<const h5::block_t&>(args_tuple, default_block);
      
      {
        h5::sp_t file_space{H5Dget_space( static_cast<::hid_t>(ds) )};
        rank = get_simple_extent_dims(file_space, current_dims );
        for(int i = 0; i < rank; i++ )
          {
            new_dims[i] = current_dims[i] + data_dims[i];
          }
      }
      
      h5::offset_t new_offset = h5::offset_t(offset);
      for(int i = 0; i < rank; i++ )
        {
          new_offset[i] += current_dims[i];
        }
      
      h5::set_extent(ds, new_dims);
      h5::sp_t file_space{H5Dget_space( static_cast<::hid_t>(ds) )};
      
      hsize_t size = 1;
      for(int i=0;i<rank;i++)
        size *= count[i] * block[i];
      
      h5::sp_t mem_space = h5::create_simple( size );
      h5::select_all( mem_space );
      h5::select_hyperslab( file_space, new_offset, stride, count, block);
      
      // this can throw exception
      h5::write<T>(ds, mem_space, file_space, dxpl, ptr);
      
      return ds;
      
    } catch ( const std::exception& err ){
      throw h5::error::io::dataset::append( err.what() );
    }
  
  
  /** \func_append_hdr
   *  TODO: append doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t>
  h5::ds_t append( const h5::fd_t& fd, const std::string& dataset_path, const T* ptr,
                   const count_t& count, args_t&&... args)
  {
    auto args_tuple = std::forward_as_tuple(args...);

    h5::current_dims_t def_data_dims;
    
    int rank = def_data_dims.rank =  count.rank;


    const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);
    const h5::dxpl_t& dxpl = get<const dxpl_t&>(args_tuple, default_dxpl);
    
    if (!has_type<const h5::current_dims_t&, args_t...>::value)
      {
        for(int i =0; i < rank; i++ )
          {
            def_data_dims[i] = count[i];
          }
        if ( has_type<const h5::stride_t&, args_t...>::value )
          { //STRIDE
            const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, default_stride); 
            for(int i=0; i < rank; i++)
              def_data_dims[i] *= stride[i];
          }
        if ( has_type<const h5::offset_t&, args_t...>::value )
          { //OFFSET
            const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, default_offset);
            for(int i=0; i < rank; i++)
              def_data_dims[i] += offset[i];
          }
      }

    
    const h5::current_dims_t& data_dims = get<const current_dims_t&>(args_tuple,
                                                                     def_data_dims);
    
    h5::ds_t ds = (H5Lexists_path(fd, dataset_path.c_str() ) > 0) ? // will throw error
      h5::open( fd, dataset_path, dapl) : h5::create<T>(fd, dataset_path, dapl, args...);
    
    return h5::append<T>(ds, ptr, count, data_dims, dxpl, args...);
  }

  
  /** \func_append_hdr
   *  TODO: append doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t >
  h5::ds_t append( const h5::fd_t& fd, const std::string& dataset_path, const T& ref,
                   const count_t& count, args_t&&... args)
    
  {
    auto args_tuple = std::forward_as_tuple(args...);

    h5::current_dims_t def_data_dims;
    
    int rank = def_data_dims.rank =  count.rank;

    const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);
    const h5::dxpl_t& dxpl = get<const dxpl_t&>(args_tuple, default_dxpl);
    
    if (!has_type<const h5::current_dims_t&, args_t...>::value)
      {
        for(int i =0; i < rank; i++ )
          {
            def_data_dims[i] = count[i];
          }
        if ( has_type<const h5::stride_t&, args_t...>::value )
          { //STRIDE
            const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, default_stride); 
            for(int i=0; i < rank; i++)
              def_data_dims[i] *= stride[i];
          }
        if ( has_type<const h5::offset_t&, args_t...>::value )
          { //OFFSET
            const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, default_offset);
            for(int i=0; i < rank; i++)
              def_data_dims[i] += offset[i];
          }
      }
    
    const h5::current_dims_t& data_dims = get<const current_dims_t&>(args_tuple,
                                                                     def_data_dims);
    
    h5::ds_t ds = (H5Lexists_path(fd, dataset_path.c_str() ) > 0) ? // will throw error
      h5::open( fd, dataset_path, dapl) : h5::create<T>(fd, dataset_path, dapl, args...);
    
    return h5::append<T>(ds, &ref, count, data_dims, dxpl, args...);
          
  }
  
  /** \func_append_hdr
   *  \func_append_desc \par_file_path \par_dataset_path 
   *  \par_offset \par_stride \par_count \par_block  \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t >
  h5::ds_t append( const std::string& file_path, const std::string& dataset_path,
                   const T& ref, const count_t& count, args_t&&... args)
  {
    h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR, h5::default_fapl );
    
    return h5::append( fd, dataset_path, ref, args... );
  }
}
#endif
