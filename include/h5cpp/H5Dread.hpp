/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 */

#ifndef  H5CPP_DREAD_HPP
#define  H5CPP_DREAD_HPP

namespace h5 {
  
  /** \func_read_hdr
   *  Updates the content of passed reference.
   *  Optional arguments **args:= h5::offset | h5::stride | h5::count | h5::block** may be specified for partial IO, 
   *  to describe the retrieved hyperslab from hdf5 file space. Default case is to select and retrieve all elements from dataset. 
   *  **h5::dxpl_t** provides control to datatransfer. 
   * \code
   * h5::fd_t fd = h5::open("myfile.h5", H5F_ACC_RDWR);
   * h5::ds_t ds = h5::open(fd,"path/to/dataset");
   * std::vector<float> myvec(10*10);
   * auto err = h5::read( fd, "path/to/dataset", myvec.data(), h5::count{10,10}, h5::offset{5,0} );	
   * \endcode  
   * \par_file_path \par_dataset_path \par_ptr \par_offset \par_stride \par_count \par_block \par_dxpl  \tpar_T \returns_err
 	*/ 

  template<class T, class... args_t >
  void read( const h5::ds_t& ds, T* out, const count_t& count, args_t&&... args )
    
    try {

      auto args_tuple = std::forward_as_tuple(args...);
      
      const h5::dxpl_t& dxpl = get<dxpl_t>(args_tuple, default_dxpl);
      
      const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, h5::default_offset);
      const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, h5::default_stride);
      const h5::block_t& block = get<const h5::block_t&>(args_tuple, h5::default_block);
      
      h5::count_t size; // compute actual memory space
      for(int i=0;i<count.rank;i++) size[i] = count[i] * block[i];
      size.rank = count.rank;
      
      H5CPP_CHECK_PROP( dxpl, h5::error::property_list::misc, "invalid data transfer property" );
      
      h5::sp_t file_space = h5::get_space(ds);
      int rank = h5::get_simple_extent_ndims( file_space );
      
      if( rank != count.rank )
        throw h5::error::io::dataset::read( H5CPP_ERROR_MSG( h5::error::msg::rank_mismatch ));
      
      using element_t = typename impl::decay<T>::type;
      h5::dt_t<element_t> mem_type;

      h5::sp_t mem_space = h5::create_simple( size );
      h5::select_all( mem_space );
      h5::select_hyperslab( file_space, offset, stride, count, block);

      H5CPP_CHECK_NZ( H5Dread(static_cast<hid_t>( ds ), static_cast<hid_t>(mem_type), static_cast<hid_t>(mem_space),
                              static_cast<hid_t>(file_space),	static_cast<hid_t>(dxpl), &out[0] ),
                      h5::error::io::dataset::read, h5::error::msg::read_dataset);
      
    } catch ( const std::runtime_error& err ){
      throw h5::error::io::dataset::read( err.what() );
    }

  
  /** \func_read_hdr
   *  Updates the content of passed reference, which must have enough memory space to receive data.
   *  Optional arguments **args:= h5::offset | h5::stride | h5::count | h5::block** may be specified for partial IO, 
   *  to describe the retrieved hyperslab from hdf5 file space. Default case is to select and retrieve all elements from dataset. 
   *  **h5::dxpl_t** provides control to datatransfer.
   * \code
   * h5::fd_t fd = h5::open("myfile.h5", H5F_ACC_RDWR);
   * h5::ds_t ds = h5::open(fd,"path/to/dataset");
   * std::vector<float> myvec(10*10);
   * auto err = h5::read( fd, "path/to/dataset", myvec.data(), h5::count{10,10}, h5::offset{5,0} );	
   * \endcode  
   * \par_file_path \par_dataset_path \par_ptr \par_offset \par_stride \par_count \par_block \par_dxpl \tpar_T \returns_err
 	*/ 
  template<class T, class... args_t >
  void read( const h5::fd_t& fd, const std::string& dataset_path, T& ref,
             const count_t& count, args_t&&... args)
  {
    auto args_tuple = std::forward_as_tuple(args...);
    
    const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);

    h5::ds_t ds = h5::open(fd, dataset_path, dapl ); // will throw its exception
    h5::read<T>(ds, &ref, count, args...);
  }

  /** \func_read_hdr
   *  Updates the content of passed reference
   *  Optional arguments **args:= h5::offset | h5::stride | h5::count | h5::block** may be specified for partial IO,
   *  to describe the retrieved hyperslab from hdf5 file space. Default case is to select and retrieve all elements from dataset. 
   * \code
   * h5::fd_t fd = h5::open("myfile.h5", H5F_ACC_RDWR);
   * h5::ds_t ds = h5::open(fd,"path/to/dataset");
   * std::vector<float> myvec(10*10);
   * auto err = h5::read( fd, "path/to/dataset", myvec.data(), h5::count{10,10}, h5::offset{5,0} );	
   * \endcode  
   * \par_file_path \par_dataset_path \par_ptr \par_offset \par_stride \par_count \par_block \tpar_T \returns_err
   */ 
  template<class T, class... args_t >
  void read( const std::string& file_path, const std::string& dataset_path, T& out,
             const count_t& count, args_t&&... args)
  {
    h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR );
    h5::read( fd, dataset_path, out, count, args...);
  }

}
#endif
