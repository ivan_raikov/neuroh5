/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>

 */

#ifndef H5CPP_CONFIG_H
#define H5CPP_CONFIG_H

#ifndef H5CPP_MAX_RANK
	#define H5CPP_MAX_RANK 7 //< maximum dimensions of stored arrays
#endif

#ifndef H5CPP_MAX_FILTER
	#define H5CPP_MAX_FILTER 16 //< maximum number of filters in a chain
#endif
#ifndef H5CPP_MAX_FILTER_PARAM
	#define H5CPP_MAX_FILTER_PARAM 16 //< maximum number of filters in a chain
#endif
#ifndef H5CPP_MEM_ALIGNMENT
	#define H5CPP_MEM_ALIGNMENT 64 //< maximum number of filters in a chain
#endif

#ifndef H5CPP_NO_COMPRESSION 
	#define H5CPP_NO_COMPRESSION 0 //< maximum dimensions of stored arrays
#endif
#ifndef H5CPP_DEFAULT_COMPRESSION 
	#define H5CPP_DEFAULT_COMPRESSION 9 //< maximum dimensions of stored arrays
#endif
#ifndef H5CPP_RANK_VEC 
	#define H5CPP_RANK_VEC 1
#endif
#ifndef H5CPP_RANK_MAT 
	#define H5CPP_RANK_MAT 2
#endif
#ifndef H5CPP_RANK_CUBE 
	#define H5CPP_RANK_CUBE 3
#endif

// implicit conversion enabled by default `-DH5CPP_CONVERSION_EXPLICIT` to disable 
#ifndef H5CPP_CONVERSION_EXPLICIT
	#define H5CPP_CONVERSION_IMPLICIT
#endif
// conversion from CAPI enabled by default `-DH5CPP_CONVERSION_FROM_CAPI_DISABLED` to disable 
#ifndef H5CPP_CONVERSION_FROM_CAPI_DISABLED
	#define H5CPP_CONVERSION_FROM_CAPI
#endif
// conversion to CAPI enabled by default `-DH5CPP_CONVERSION_TO_CAPI_DISABLED` to disable 
#ifndef H5CPP_CONVERSION_TO_CAPI_DISABLED
	#define H5CPP_CONVERSION_TO_CAPI
#endif
// redefine to your liking
#ifndef H5CPP_ERROR_MSG
	#define H5CPP_ERROR_MSG( msg ) std::string( __FILE__ ) + " line#  " + std::to_string( __LINE__ ) + " : " + msg
#endif

// detecting c++17 if constexpr cond ( ... ){} 
#ifdef __cpp_if_constexpr
	#define h5cpp__constexpr constexpr
	#define h5cpp__assert( condition, msg ) static_assert( condition, msg )
#else
	#define h5cpp__constexpr
	#define h5cpp__assert( condition, msg ) if( !condition ) throw std::runtime_error( "ERROR: "  msg )
#endif


#define H5CPP_CHECK_EQ( call, exception, msg ) if( call == 0 ) throw exception( H5CPP_ERROR_MSG( msg ));
#define H5CPP_CHECK_NZ( call, exception, msg ) if( call < 0 ) throw exception( H5CPP_ERROR_MSG( msg ));
#define H5CPP_CHECK_NULL( call, exception, msg ) if( call == NULL  ) throw exception( H5CPP_ERROR_MSG( msg ));
#define H5CPP_CHECK_PROP( id, exception, msg ) if( static_cast<::hid_t>( id ) < 0 ) throw exception( H5CPP_ERROR_MSG( msg ));
#define H5CPP_CHECK_ID( id, exception, msg ) if( !static_cast<::hid_t>( id ) ) throw exception( H5CPP_ERROR_MSG( msg ));

#endif





