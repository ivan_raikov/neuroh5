/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 *
 */
#ifndef  H5CPP_GCREATE_HPP 
#define  H5CPP_GCREATE_HPP


namespace h5 {
 	/** \func_create_hdr
	* \code
	* \par_file_path \par_dataset_path \par_current_dims \par_max_dims 
	* \par_lcpl \par_dcpl \par_dapl  \tpar_T \returns_ds
 	*/ 
	template<class T, class... args_t>
	h5::gr_t create( const h5::fd_t& fd, const std::string& group_path,
                         args_t&&... args)
                         
          try {


            auto args_tuple = std::forward_as_tuple(args...);

            const h5::gcpl_t& default_gcpl{ H5Pcreate(H5P_GROUP_CREATE) };

            const h5::lcpl_t& lcpl = get<const lcpl_t&>(args_tuple, default_lcpl);
            const h5::gcpl_t& gcpl = get<const dcpl_t&>(args_tuple, default_gcpl);
            const h5::gapl_t& gapl = get<const dapl_t&>(args_tuple, default_gapl);

            H5CPP_CHECK_PROP( lcpl, h5::error::property_list::misc,
                              "invalid list control property" );
            H5CPP_CHECK_PROP( gcpl, h5::error::property_list::misc,
                              "invalid group creation property list" );
            H5CPP_CHECK_PROP( gapl, h5::error::property_list::misc,
                              "invalid group access property" );

            return h5::creategrp(fd, group_path, type, 
                                 lcpl, gcpl, gapl);

          } catch( const std::runtime_error& err ) {
            throw h5::error::io::group::create( err.what() );
          }

  // delegate to h5::fd_t 
  template<class T, class... args_t>
  inline h5::gr_t create( const std::string& file_path, const std::string& group_path, args_t&&... args ){
    h5::fd_t fd = h5::open(file_path, H5F_ACC_RDWR, h5::default_fapl);
    return h5::create(fd, file_path, args...);
  }
}
#endif

