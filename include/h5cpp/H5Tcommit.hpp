/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 */

#ifndef H5CPP_TCOMMIT_HPP
#define H5CPP_TCOMMIT_HPP


namespace h5 {

  template<class T, class... args_t >
  void commit(const fd_t& fd, const std::string& name, dt_t<T> &dt, args_t&&... args )
  {
    auto args_tuple = std::forward_as_tuple(args...);

    herr_t status;
    
    const h5::lcpl_t& lcpl = get<const lcpl_t&>(args_tuple, default_lcpl);
    const h5::tcpl_t& tcpl = get<const tcpl_t&>(args_tuple, default_tcpl);
    const h5::tapl_t& tapl = get<const tapl_t&>(args_tuple, default_tapl);
    
    H5CPP_CHECK_NZ((status = H5Tcommit2( static_cast<hid_t>( fd ),
                                         name.c_str(),
                                         static_cast<hid_t>( dt ),
                                         lcpl, tcpl, tapl )),
                   h5::error::io::type::any, h5::error::msg::commit );
    

  }
  
}

#endif
