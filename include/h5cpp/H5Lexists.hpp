
#include <string>
#include <vector>

#ifndef H5CPP_EXISTS_HPP
#define H5CPP_EXISTS_HPP

using namespace std;

namespace h5
{

    void tokenize (string str, const string& delimiter, vector<string> &token_vector)
    {
      size_t start = str.find_first_not_of(delimiter), end=start;
      
      while (start != string::npos)
        {
          // Find next occurence of delimiter
          end = str.find(delimiter, start);
          // Push back the token found into vector
          token_vector.push_back(str.substr(start, end-start));
          // Skip all occurrences of the delimiter to find new start
          start = str.find_first_not_of(delimiter, end);
        }
    }
      
    /*****************************************************************************
     * Check if path exists
     *****************************************************************************/
    bool H5Lexists_path
    (
     const h5::fd_t& fd, 
     const std::string& path
     )
    {
      const string delim = "/";
      herr_t status=-1;  string ppath;
      vector<string> tokens;
      tokenize (path, delim, tokens);
      
      for (string value : tokens)
        {
          ppath = ppath + delim + value;
          status = H5Lexists (fd, ppath.c_str(), H5P_DEFAULT);
          if (status <= 0)
            break;
        }

      return (status > 0);
    }

  
}
#endif
