/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 */

#ifndef H5CPP_TALL_HPP
#define H5CPP_TALL_HPP


namespace h5 {
    template<class T> hid_t register_struct(){ return H5I_UNINIT; }
}

/* template specialization from hid_t< .. > type which provides syntactic sugar in the form
 * h5::dt_t<int> dt; 
 * */
namespace h5 { namespace impl { namespace detail {
	template<class T> // parent type, data_type is inherited from, see H5Iall.hpp top section for details 
	using dt_p = hid_t<T,H5Tclose,true,true,hdf5::any>;
	/*type id*/
	template<class T>
		struct hid_t<T,H5Tclose,true,true,hdf5::type> : public dt_p<T> {
		using parent = dt_p<T>;
		using parent::hid_t; // is a must because of ds_t{hid_t} ctor 
		using hidtype = T;
		hid_t() : parent( H5I_UNINIT){};
	};
	template <class T> using dt_t = hid_t<T,H5Tclose,true,true,hdf5::type>;
}}}

/* template specialization is for the preceding class, and should be used only for HDF5 ELEMENT types
 * which are in C/C++ the integral types of: char,short,int,long, ... and C POD types. 
 * anything else, the ones which are considered objects/classes are broken down into integral types + container 
 * then pointer read|write is obtained to the continuous slab and delegated to h5::read | h5::write.
 * IF the data is not in a continuous memory region then it must be copied! 
 */

#define H5CPP_REGISTER_TYPE_( C_TYPE, H5_TYPE )                                           \
namespace h5 { namespace impl { namespace detail { 	                                      \
	template <> struct hid_t<C_TYPE,H5Tclose,true,true,hdf5::type> : public dt_p<C_TYPE> {\
		using parent = dt_p<C_TYPE>;                                                      \
		using parent::hid_t;                                                              \
		using hidtype = C_TYPE;                                                           \
		hid_t() : parent( H5Tcopy( H5_TYPE ) ) { 										  \
			hid_t id = static_cast<hid_t>( *this );                                       \
			if ( std::is_pointer<C_TYPE>::value )                               \
                          H5Tset_size (id,H5T_VARIABLE), H5Tset_cset(id, H5T_CSET_UTF8); \
		}                                                                                 \
	};                                                                                    \
}}                                                                                       \
  template <typename T> struct name; \
template <> struct name<C_TYPE> {                                       \
		static constexpr char const * value = #C_TYPE;                                    \
	};                                                                                    \
}                                                                                         \

/* registering integral data-types for NATIVE ones, which means all data is stored in the same way 
 * in file and memory: TODO: allow different types for file storage
 * */
	H5CPP_REGISTER_TYPE_(bool,H5T_NATIVE_HBOOL)

	H5CPP_REGISTER_TYPE_(unsigned char, H5T_NATIVE_UCHAR)
        H5CPP_REGISTER_TYPE_(char, H5T_NATIVE_CHAR)
	H5CPP_REGISTER_TYPE_(unsigned short, H5T_NATIVE_USHORT)
        H5CPP_REGISTER_TYPE_(short, H5T_NATIVE_SHORT)
	H5CPP_REGISTER_TYPE_(unsigned int, H5T_NATIVE_UINT)
        H5CPP_REGISTER_TYPE_(int, H5T_NATIVE_INT)
	H5CPP_REGISTER_TYPE_(unsigned long int, H5T_NATIVE_ULONG)
        H5CPP_REGISTER_TYPE_(long int, H5T_NATIVE_LONG)
	H5CPP_REGISTER_TYPE_(unsigned long long int, H5T_NATIVE_ULLONG)
        H5CPP_REGISTER_TYPE_(long long int, H5T_NATIVE_LLONG)
	H5CPP_REGISTER_TYPE_(float, H5T_NATIVE_FLOAT)
        H5CPP_REGISTER_TYPE_(double, H5T_NATIVE_DOUBLE)
	H5CPP_REGISTER_TYPE_(long double,H5T_NATIVE_LDOUBLE)


#define H5CPP_REGISTER_STRUCT( POD_STRUCT ) H5CPP_REGISTER_TYPE_( POD_STRUCT, h5::register_struct<POD_STRUCT>() )

/* type alias is responsible for ALL type maps through H5CPP if you want to screw things up
 * start here.
 * template parameters:
 *  hid_t< C_TYPE being mapped, conversion_from_capi, conversion_to_capi, marker_for_this_type>
 * */


namespace h5 {

  template <class T> using dt_t = h5::impl::detail::hid_t<T,H5Tclose,true,true,h5::impl::detail::hdf5::type>;

  const static h5::tapl_t default_tapl = static_cast<h5::tapl_t>( H5Pcreate(H5P_DATATYPE_ACCESS) );
  const static h5::tcpl_t default_tcpl = static_cast<h5::tcpl_t>( H5Pcreate(H5P_DATATYPE_CREATE) );

  
  template<class T>
  hid_t copy( const h5::dt_t<T>& dt )
  {
    hid_t id = static_cast<hid_t>(dt);
    H5Iinc_ref( id );
    return id;
  }


  
  template <class T>
  dt_t<T> type_open( const h5::fd_t& fd, const std::string & path, const h5::tapl_t& tapl = h5::default_tapl  )
  {
    hid_t dt;
    
    H5CPP_CHECK_NZ((dt = H5Topen2( static_cast<hid_t>(fd), path.data(), static_cast<hid_t>(tapl) )),
                   h5::error::io::type::open, h5::error::msg::open_type );

    return dt_t<T>{dt};
  }

  
  template <class T>
  size_t get_nmembers( const dt_t<T> &dt )
  {
    size_t res=0;

    H5CPP_CHECK_NZ((res = H5Tget_nmembers( static_cast<hid_t>(dt) )),
                   h5::error::io::type::any, h5::error::msg::get_nmembers );
    
    return res;
  }

  template <class T>
  dt_t<T> enum_create()
  {
    using element_t = typename impl::decay<T>::type;
    h5::dt_t<element_t> base_type;

    hid_t dt;
    H5CPP_CHECK_NZ((dt = H5Tenum_create( static_cast<hid_t>(base_type) )),
                   h5::error::io::type::any, h5::error::msg::enum_create );
    
    return dt_t<T>{dt};
  }


  template<class T>
  void enum_insert( dt_t<T> &dt, const std::string& name, T value)
  {
    herr_t status;
    H5CPP_CHECK_NZ((status = H5Tenum_insert( static_cast<hid_t>(dt), name.c_str(), &value )),
                   h5::error::io::type::any, h5::error::msg::enum_insert );
    

  }

  template<class T>
  const std::string enum_nameof( const dt_t<T> &dt, size_t i )
  {
    size_t res=0;
    const size_t max_dtype_len = 1024;
    
    char namebuf[max_dtype_len];

    H5CPP_CHECK_NZ((res = H5Tenum_nameof( static_cast<hid_t>(dt), &i, namebuf, max_dtype_len )),
                   h5::error::io::type::any, h5::error::msg::enum_nameof );
    
    return std::string(namebuf);
  }

  
}


template<class T>
inline std::ostream& operator<<(std::ostream &os, const h5::dt_t<T>& dt)
{
  hid_t id = static_cast<hid_t>( dt );
  os << "data type: " << h5::name<T>::value << " ";
  os << ( std::is_pointer<T>::value ? "pointer" : "value" );
  os << ( H5Iis_valid( id ) > 0 ? " valid" : " invalid");
  return os;
}

#endif
