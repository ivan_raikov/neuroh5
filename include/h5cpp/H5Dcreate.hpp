/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 *
 */
#ifndef  H5CPP_DCREATE_HPP 
#define  H5CPP_DCREATE_HPP


namespace h5 {
 	/** \func_create_hdr
	* \code
	* examples:
	* //creates a dataset with 2*myvec.size() + offset
	* auto ds = h5::create( "path/to/file.h5", "path/to/dataset", myvec, h5::offset{5}, h5::stride{2} );
	* // explicit dataset spec	
	* \endcode  
	* \par_file_path \par_dataset_path \par_current_dims \par_max_dims 
	* \par_lcpl \par_dcpl \par_dapl  \tpar_T \returns_ds
 	*/ 
	template<class T, class... args_t>
	h5::ds_t create( const h5::fd_t& fd, const std::string& dataset_path,
                         args_t&&... args)
                         
          try {


            auto args_tuple = std::forward_as_tuple(args...);

            const h5::dcpl_t& default_dcpl{ H5Pcreate(H5P_DATASET_CREATE) };

            const h5::lcpl_t& lcpl = get<const lcpl_t&>(args_tuple, default_lcpl);
            const h5::dcpl_t& dcpl = get<const dcpl_t&>(args_tuple, default_dcpl);
            const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);

            H5CPP_CHECK_PROP( lcpl, h5::error::property_list::misc,
                              "invalid list control property" );
            H5CPP_CHECK_PROP( dcpl, h5::error::property_list::misc,
                              "invalid data creation property list" );
            H5CPP_CHECK_PROP( dapl, h5::error::property_list::misc,
                              "invalid data access property" );

            // and dimensions
            h5::current_dims_t def_current_dims{0}; // if no current dims_present 

            const h5::max_dims_t& max_dims = get<const max_dims_t&>(args_tuple,
                                                                    h5::max_dims_t{0});
            
            bool has_unlimited_dimension = false;
            h5::sp_t space_id{H5I_UNINIT}; // set to invalid state 
            h5::ds_t ds{H5I_UNINIT};

            if (has_type<h5::max_dims_t, args_t...>::value)
              {
                size_t dims_rank = max_dims.size();
                if (has_type<const h5::current_dims_t&, args_t...>::value)
                  {
                    // set current dimensions to given one or zero if H5S_UNLIMITED 
                    for(hsize_t i=0; i<dims_rank; i++)
                      {
                        def_current_dims[i] = max_dims[i] != H5S_UNLIMITED
                          ? max_dims[i] : (has_unlimited_dimension=true, static_cast<hsize_t>(0));
                      }
                    def_current_dims.rank = dims_rank;
                  }
              }

            h5::current_dims_t current_dims = get<const current_dims_t&>(args_tuple,
                                                                         def_current_dims);

            dcpl_t this_dcpl = h5::dcpl_t(dcpl);
            if ( (H5Pget_layout(dcpl) != H5D_CHUNKED) &&
                 ( has_unlimited_dimension ||
                   (has_type<const h5::current_dims_t&, args_t...>::value &&
                    has_type<const h5::max_dims_t&, args_t...>::value)) )
              {
                chunk_t chunk{0};
                chunk.rank = current_dims.rank;
                if (has_type<h5::current_dims_t, args_t...>::value)
                  {
                    for(hsize_t i=0; (int)i<chunk.rank; i++)
                      {
                        chunk[i] = current_dims[i];
                      }
                  }
                else
                  {
                    for(hsize_t i=0; (int)i<chunk.rank; i++)
                      {
                        chunk[i] = 1;
                      }
                  }
                h5::set_chunk(this_dcpl, chunk );
              }
                
            // use move semantics to set space
            if (has_type<const h5::max_dims_t&, args_t...>::value)
              {
                space_id =  std::move( h5::create_simple( current_dims, max_dims ));
              }
            else
              {
                space_id = std::move( h5::create_simple( current_dims ) );
              }
            using element_t = typename impl::decay<T>::type;
            h5::dt_t<element_t> type;
            return h5::createds(fd, dataset_path, type, space_id,
                                lcpl, (const dcpl_t&)this_dcpl, dapl);

          } catch( const std::runtime_error& err ) {
            throw h5::error::io::dataset::create( err.what() );
          }

  // delegate to h5::fd_t 
  template<class T, class... args_t>
  inline h5::ds_t create( const std::string& file_path, const std::string& dataset_path, args_t&&... args ){
    h5::fd_t fd = h5::open(file_path, H5F_ACC_RDWR, h5::default_fapl);
    return h5::create<T>(fd, dataset_path, args...);
  }
}
#endif

