/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 *
 */

#ifndef  H5CPP_MISC_HPP
#define  H5CPP_MISC_HPP


#define H5CPP_supported_elementary_types "supported elementary types ::= pod_struct | float | double |  [signed](int8 | int16 | int32 | int64)"

namespace h5 { namespace utils {
    //template <class T>
	//static constexpr bool is_supported = std::is_arithmetic<T>::value;
	//static constexpr bool is_supported = std::is_pod<T>::value && std::is_class<T>::value | std::is_arithmetic<T>::value;
}}




#endif

