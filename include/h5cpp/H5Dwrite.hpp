/*
 * Copyright (c) 2018 vargaconsulting, Toronto,ON Canada
 * Author: Varga, Steven <steven@vargaconsulting.ca>
 */

#ifndef H5CPP_DWRITE_HPP
#define H5CPP_DWRITE_HPP

namespace h5
{

  /** \func_write_hdr
   *  TODO: write doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T>
  void write( const h5::ds_t& ds, const h5::sp_t& mem_space, const h5::sp_t& file_space, const h5::dxpl_t& dxpl, const T* ptr  )
  {
    H5CPP_CHECK_PROP( dxpl, h5::error::io::dataset::write, "invalid data transfer property" );
    using element_t = typename h5::impl::decay<T>::type;
    h5::dt_t<element_t> type;
    H5CPP_CHECK_NZ(H5Dwrite( static_cast<hid_t>( ds ), type, mem_space, file_space, static_cast<hid_t>(dxpl), ptr), h5::error::io::dataset::write, h5::error::msg::write_dataset);
  }
  
  /** \func_write_hdr
   *  TODO: write doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t >
  h5::ds_t write( h5::ds_t& ds, const T* ptr,
                  const count_t& count,
                  const current_dims_t& data_dims,
                  const dxpl_t& dxpl,
                  args_t&&... args)
    try {

      auto args_tuple = std::forward_as_tuple(args...);
      
      h5::sp_t file_space{H5Dget_space( static_cast<::hid_t>(ds) )};

      int rank = h5::get_simple_extent_ndims( file_space );
      const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, default_offset);
      const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, default_stride);
      const h5::block_t& block = get<const h5::block_t&>(args_tuple, default_block);

      h5::set_extent(ds, data_dims);

      hsize_t size = 1;
      for(int i=0; i<rank; i++)
        size *= count[i] * block[i];
      
      h5::sp_t mem_space = h5::create_simple( size );
      h5::select_all( mem_space );
      h5::select_hyperslab( file_space, offset, stride, count, block );
                
      // this can throw exception
      h5::write<T>(ds, mem_space, file_space, dxpl, ptr);
      
      return ds;
    } catch ( const std::exception& err ){
      throw h5::error::io::dataset::write( err.what() );
    }


  /** \func_write_hdr
   *  TODO: write doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t >
  h5::ds_t write( const h5::fd_t& fd, const std::string& dataset_path,
                  const T* ptr, const count_t& count, args_t&&... args)
  {

    auto args_tuple = std::forward_as_tuple(args...);

    h5::current_dims_t def_data_dims;
    int rank = def_data_dims.rank = count.rank;

    const h5::dcpl_t& default_dcpl{ H5Pcreate(H5P_DATASET_CREATE) };

    const h5::dcpl_t& dcpl = get<const dcpl_t&>(args_tuple, default_dcpl);
    const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);
    const h5::dxpl_t& dxpl = get<const dxpl_t&>(args_tuple, default_dxpl);
    
    if (!has_type<const h5::current_dims_t&, args_t...>::value)
      {
        for(int i =0; i < rank; i++ )
          def_data_dims[i] = count[i];
        if ( has_type<const h5::stride_t&, args_t...>::value )
          { //STRIDE
            const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, default_stride);
            for(int i=0; i < rank; i++)
              def_data_dims[i] *= stride[i];
          }
        if ( has_type<const h5::offset_t&, args_t...>::value )
          { //OFFSET
            const h5::offset_t& offset = get<const h5::offset_t&>(args_tuple, default_offset);
            for(int i=0; i < rank; i++)
              def_data_dims[i] += offset[i];
          }
      }

    const h5::current_dims_t& data_dims = get<const current_dims_t&>(args_tuple,
                                                                     def_data_dims);

    h5::ds_t ds = (H5Lexists_path(fd, dataset_path.c_str() ) > 0) ? // will throw error
      h5::open( fd, dataset_path, dapl) : h5::create<T>(fd, dataset_path, data_dims, dcpl, args...);

    h5::write<T>(ds, ptr, count, data_dims, dxpl, args...);
          
    return ds;
  }

  /** \func_write_hdr
   *  TODO: write doxy for here  
   *  \par_file_path \par_dataset_path \par_ref \par_offset \par_count \par_dxpl \tpar_T \returns_herr 
   */ 
  template <class T, class... args_t>
  h5::ds_t write( const h5::fd_t& fd, const std::string& dataset_path, const T& ref,
                  const count_t& count, args_t&&... args)
  {
    auto args_tuple = std::forward_as_tuple(args...);
    
    h5::current_dims_t def_data_dims;
    
    int rank = def_data_dims.rank = count.rank;

    const h5::dcpl_t& default_dcpl{ H5Pcreate(H5P_DATASET_CREATE) };

    const h5::dcpl_t& dcpl = get<const dcpl_t&>(args_tuple, default_dcpl);
    const h5::dapl_t& dapl = get<const dapl_t&>(args_tuple, default_dapl);
    const h5::dxpl_t& dxpl = get<const dxpl_t&>(args_tuple, default_dxpl);

    if (!has_type<const h5::current_dims_t&, args_t...>::value)
      {
        for(int i =0; i < rank; i++ )
          def_data_dims[i] = count[i];
        if ( has_type<const h5::stride_t&, args_t...>::value )
          { //STRIDE
            const h5::stride_t& stride = get<const h5::stride_t&>(args_tuple, default_stride);
            for(int i=0; i < rank; i++)
              def_data_dims[i] *= stride[i];
          }
        if ( has_type<const h5::offset_t&, args_t...>::value )
          { //OFFSET
            const h5::offset_t& offset =  get<const h5::offset_t&>(args_tuple, default_offset);
            for(int i=0; i < rank; i++)
              def_data_dims[i] += offset[i];
          }
      }

    const h5::current_dims_t& data_dims = get<const current_dims_t&>(args_tuple, def_data_dims);
    
    h5::ds_t ds = (H5Lexists_path(fd, dataset_path.c_str() ) > 0) ? // will throw error
      h5::open( fd, dataset_path, dapl) : h5::create<T>(fd, dataset_path, data_dims, dcpl, dapl, args...);
          
    h5::write<T>(ds, &ref, count, data_dims, dxpl, args...);
    
    return ds;
  }

   /** \func_write_hdr
    *  \func_write_desc \par_file_path \par_dataset_path 
    *  \par_offset \par_stride \par_count \par_block  \par_dxpl \tpar_T \returns_herr 
    */ 
  template <class T, class... args_t >
  h5::ds_t write( const std::string& file_path, const std::string& dataset_path,
                  const T& ref, const count_t& count, args_t&&... args )
  {
    //TODO: check if exists create if doesn't
    h5::fd_t fd = h5::open( file_path, H5F_ACC_RDWR, h5::default_fapl );
    return h5::write( fd, dataset_path, ref, args...);
    
  }
}
#endif
