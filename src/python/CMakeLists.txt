
add_library(iomodule SHARED ${NEUROH5_PYTHON_SRCS})

set_target_properties(
    iomodule
    PROPERTIES
        PREFIX ""
        OUTPUT_NAME "io"
        LINKER_LANGUAGE C
    )

target_include_directories(iomodule PUBLIC
    ${PYTHON_INCLUDE_DIRS}
    ${NUMPY_INCLUDE_DIR}
    ${PROJECT_SOURCE_DIR}/include
    )

target_link_libraries(iomodule
    ${PYTHON_LIBRARIES}
)
